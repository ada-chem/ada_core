import re
from setuptools import find_packages, setup

NAME = 'ada_core'
DESCRIPTION = 'Python API for North Robotics controllers'
AUTHOR = 'Lars Yunker'
CLASSIFIERS = [
    'Programming Language :: Python :: 3',
    'Operating System :: Windows',
]
INSTALL_REQUIRES = [
    'slackclient>=1.2.0',
    'numpy>=1.14.0',
    'imageio>=2.2.0',
    'opencv_python>=3.4.0.12',
    'matplotlib>=2.1.2',
    'Pillow>=5.0.0',
    'pylibftdi>=0.16.1.2',
    'openpyxl>=2.5.2',
    'scikit-learn>=0.19.1',
    'pyzmq>=17.0.0',
    'pandas>=0.23.3',
]
EXCLUDE_PACKAGES = [
]

with open('LICENSE') as f:
    LICENSE = f.read()

with open('VERSION') as f:
    VERSION = re.sub('^v', '', f.read())

# find packages and prefix them with the main package name
PACKAGES = [f'{NAME}.{package}' for package in find_packages(exclude=EXCLUDE_PACKAGES)]

setup(
    name=NAME,
    version=VERSION,
    description=DESCRIPTION,
    author=AUTHOR,
    install_requires=INSTALL_REQUIRES,
    packages=PACKAGES,
    # look for modules inside our package namespace in the root directory (normally this is a subdirectory)
    package_dir={NAME: ''},
    license=LICENSE,
    classifiers=CLASSIFIERS
)
