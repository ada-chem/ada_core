from tkinter import *
from PIL import ImageTk  # needed to work with images that aren't GIF's
import os
import ada_core
import pickle


def loadimage(name, test=123.):
    """
    Loads an image in another folder because it won't read otherwise
    :param name: image name
    testing
    :return: PhotoImage instance
    """
    return ImageTk.PhotoImage(
        file=(imagedir + name)
        # Image.open(imagedir + name)
    )
import tkinter as tk
import tkinter.ttk as ttk

class Tooltip:
    '''
    It creates a tooltip for a given widget as the mouse goes on it.

    see:

    http://stackoverflow.com/questions/3221956/
           what-is-the-simplest-way-to-make-tooltips-
           in-tkinter/36221216#36221216

    http://www.daniweb.com/programming/software-development/
           code/484591/a-tooltip-class-for-tkinter

    - Originally written by vegaseat on 2014.09.09.

    - Modified to include a delay time by Victor Zaccardo on 2016.03.25.

    - Modified
        - to correct extreme right and extreme bottom behavior,
        - to stay inside the screen whenever the tooltip might go out on
          the top but still the screen is higher than the tooltip,
        - to use the more flexible mouse positioning,
        - to add customizable background color, padding, waittime and
          wraplength on creation
      by Alberto Vassena on 2016.11.05.

      Tested on Ubuntu 16.04/16.10, running Python 3.5.2

    TODO: themes styles support
    '''

    def __init__(self, widget,
                 *,
                 bg='#FFFFEA',
                 pad=(5, 3, 5, 3),
                 text='widget info',
                 waittime=400,
                 wraplength=250):

        self.waittime = waittime  # in miliseconds, originally 500
        self.wraplength = wraplength  # in pixels, originally 180
        self.widget = widget
        self.text = text
        self.widget.bind("<Enter>", self.onEnter)
        self.widget.bind("<Leave>", self.onLeave)
        self.widget.bind("<ButtonPress>", self.onLeave)
        self.bg = bg
        self.pad = pad
        self.id = None
        self.tw = None

    def onEnter(self, event=None):
        self.schedule()

    def onLeave(self, event=None):
        self.unschedule()
        self.hide()

    def schedule(self):
        self.unschedule()
        self.id = self.widget.after(self.waittime, self.show)

    def unschedule(self):
        id_ = self.id
        self.id = None
        if id_:
            self.widget.after_cancel(id_)

    def show(self):
        def tip_pos_calculator(widget, label,
                               *,
                               tip_delta=(10, 5), pad=(5, 3, 5, 3)):

            w = widget

            s_width, s_height = w.winfo_screenwidth(), w.winfo_screenheight()

            width, height = (pad[0] + label.winfo_reqwidth() + pad[2],
                             pad[1] + label.winfo_reqheight() + pad[3])

            mouse_x, mouse_y = w.winfo_pointerxy()

            x1, y1 = mouse_x + tip_delta[0], mouse_y + tip_delta[1]
            x2, y2 = x1 + width, y1 + height

            x_delta = x2 - s_width
            if x_delta < 0:
                x_delta = 0
            y_delta = y2 - s_height
            if y_delta < 0:
                y_delta = 0

            offscreen = (x_delta, y_delta) != (0, 0)

            if offscreen:

                if x_delta:
                    x1 = mouse_x - tip_delta[0] - width

                if y_delta:
                    y1 = mouse_y - tip_delta[1] - height

            offscreen_again = y1 < 0  # out on the top

            if offscreen_again:
                # No further checks will be done.

                # TIP:
                # A further mod might automagically augment the
                # wraplength when the tooltip is too high to be
                # kept inside the screen.
                y1 = 0

            return x1, y1

        bg = self.bg
        pad = self.pad
        widget = self.widget

        # creates a toplevel window
        self.tw = tk.Toplevel(widget)

        # Leaves only the label and removes the app window
        self.tw.wm_overrideredirect(True)

        win = tk.Frame(self.tw,
                       background=bg,
                       borderwidth=0)
        label = tk.Label(win,
                          text=self.text,
                          justify=tk.LEFT,
                          background=bg,
                          relief=tk.SOLID,
                          borderwidth=0,
                          wraplength=self.wraplength)

        label.grid(padx=(pad[0], pad[2]),
                   pady=(pad[1], pad[3]),
                   sticky=tk.NSEW)
        win.grid()

        x, y = tip_pos_calculator(widget, label)

        self.tw.wm_geometry("+%d+%d" % (x, y))

    def hide(self):
        tw = self.tw
        if tw:
            tw.destroy()
        self.tw = None

def buttonPress1(event):  # events associated with a button click

    number1 = int(entryF.get()) + int(entryG.get()) + int(entryH.get()) + int(entryI.get()) + int(entryJ.get()) + int(
        entryK.get())

    textpad.insert(END, "\n")
    textpad.insert(END, number1)

# def save(event):
#     """
#     Saves the parameters
#     :param event:
#     :return:
#     """


imagedir = os.path.join(
    os.path.dirname(ada_core.__file__) + '\\images\\'
)

font = {  # font properties for buttons
    'font':("Helvetica", 15),
    'width': 12,
}
yspacing = 20  # y spacing for buttons

args = [  # required arguments for function execution
    'syrdraw',
    ''
]

root = Tk()  # program starts here
root.iconbitmap(default=(imagedir + 'icon.ico'))  # change window icon
root.title("North Robotics Direct Inject 0.1")  # window title
root.geometry("850x500")  # size of window to open

bkg = loadimage('Background.png')
label0 = Label(root, image=bkg)
label0.place(x=0, y=0)

button1 = Button(root)  # DATA INPUT BUTTON
im1 = loadimage("data input icon.png")
button1.config(image=im1, relief=FLAT)
button1.place(x=10, y=375)
button1.bind("<Button-1>", buttonPress1)

# label1 = Label(root, text='Syringe pump draw volume (mL)')
# label1.place(x=10, y=10)
# syrdraw = Entry(root, **font)  # syringe draw volume



# ----------------------------

label1 = Label(root, text="M6 Pump Speed")
label1.place(x=10, y=10)

entryF = Entry(root, font=("Helvetica", 15), width=12)
entryF.place(x=10, y=30)
entryF.insert(0, 0)
Tooltip(entryF,text='testing 123\n456')

label2 = Label(root, text="M6 Volume")
label2.place(x=10, y=70)

entryG = Entry(root, font=("Helvetica", 15), width=12)
entryG.place(x=10, y=90)
entryG.insert(0, 0)

label3 = Label(root, text="Syringe Speed")
label3.place(x=10, y=130)

entryH = Entry(root, font=("Helvetica", 15), width=12)
entryH.place(x=10, y=150)
entryH.insert(0, 0)

label4 = Label(root, text="Syringe Volume")
label4.place(x=10, y=190)

entryI = Entry(root, font=("Helvetica", 15), width=12)
entryI.place(x=10, y=210)
entryI.insert(0, 0)

label5 = Label(root, text="Wait Time 1")
label5.place(x=10, y=250)

entryJ = Entry(root, font=("Helvetica", 15), width=12)
entryJ.place(x=10, y=270)
entryJ.insert(0, 0)

label6 = Label(root, text="Wait Time 2")
label6.place(x=10, y=310)

entryK = Entry(root, font=("Helvetica", 15), width=12)
entryK.place(x=10, y=330)
entryK.insert(0, 0)

textpad = Text(root, height=20, width=20)
textpad.place(x=160, y=30)

root.mainloop()
