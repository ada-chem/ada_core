import tkinter as tk
import tkinter.ttk as ttk

class Tooltip:
    '''
    It creates a tooltip for a given widget as the mouse goes on it.

    see:

    http://stackoverflow.com/questions/3221956/
           what-is-the-simplest-way-to-make-tooltips-
           in-tkinter/36221216#36221216

    http://www.daniweb.com/programming/software-development/
           code/484591/a-tooltip-class-for-tkinter

    - Originally written by vegaseat on 2014.09.09.

    - Modified to include a delay time by Victor Zaccardo on 2016.03.25.

    - Modified
        - to correct extreme right and extreme bottom behavior,
        - to stay inside the screen whenever the tooltip might go out on
          the top but still the screen is higher than the tooltip,
        - to use the more flexible mouse positioning,
        - to add customizable background color, padding, waittime and
          wraplength on creation
      by Alberto Vassena on 2016.11.05.

      Tested on Ubuntu 16.04/16.10, running Python 3.5.2

    TODO: themes styles support
    '''

    def __init__(self, widget,
                 *,
                 bg='#FFFFEA',
                 pad=(5, 3, 5, 3),
                 text='widget info',
                 waittime=400,
                 wraplength=250):

        self.waittime = waittime  # in miliseconds, originally 500
        self.wraplength = wraplength  # in pixels, originally 180
        self.widget = widget
        self.text = text
        self.widget.bind("<Enter>", self.onEnter)
        self.widget.bind("<Leave>", self.onLeave)
        self.widget.bind("<ButtonPress>", self.onLeave)
        self.bg = bg
        self.pad = pad
        self.id = None
        self.tw = None

    def onEnter(self, event=None):
        self.schedule()

    def onLeave(self, event=None):
        self.unschedule()
        self.hide()

    def schedule(self):
        self.unschedule()
        self.id = self.widget.after(self.waittime, self.show)

    def unschedule(self):
        id_ = self.id
        self.id = None
        if id_:
            self.widget.after_cancel(id_)

    def show(self):
        def tip_pos_calculator(widget, label,
                               *,
                               tip_delta=(10, 5), pad=(5, 3, 5, 3)):

            w = widget

            s_width, s_height = w.winfo_screenwidth(), w.winfo_screenheight()

            width, height = (pad[0] + label.winfo_reqwidth() + pad[2],
                             pad[1] + label.winfo_reqheight() + pad[3])

            mouse_x, mouse_y = w.winfo_pointerxy()

            x1, y1 = mouse_x + tip_delta[0], mouse_y + tip_delta[1]
            x2, y2 = x1 + width, y1 + height

            x_delta = x2 - s_width
            if x_delta < 0:
                x_delta = 0
            y_delta = y2 - s_height
            if y_delta < 0:
                y_delta = 0

            offscreen = (x_delta, y_delta) != (0, 0)

            if offscreen:

                if x_delta:
                    x1 = mouse_x - tip_delta[0] - width

                if y_delta:
                    y1 = mouse_y - tip_delta[1] - height

            offscreen_again = y1 < 0  # out on the top

            if offscreen_again:
                # No further checks will be done.

                # TIP:
                # A further mod might automagically augment the
                # wraplength when the tooltip is too high to be
                # kept inside the screen.
                y1 = 0

            return x1, y1

        bg = self.bg
        pad = self.pad
        widget = self.widget

        # creates a toplevel window
        self.tw = tk.Toplevel(widget)

        # Leaves only the label and removes the app window
        self.tw.wm_overrideredirect(True)

        win = tk.Frame(self.tw,
                       background=bg,
                       borderwidth=0)
        label = tk.Label(win,
                          text=self.text,
                          justify=tk.LEFT,
                          background=bg,
                          relief=tk.SOLID,
                          borderwidth=0,
                          wraplength=self.wraplength)

        label.grid(padx=(pad[0], pad[2]),
                   pady=(pad[1], pad[3]),
                   sticky=tk.NSEW)
        win.grid()

        x, y = tip_pos_calculator(widget, label)

        self.tw.wm_geometry("+%d+%d" % (x, y))

    def hide(self):
        tw = self.tw
        if tw:
            tw.destroy()
        self.tw = None

def make_gui(function):
    """
    Creates and runs a GUI for the provided function
    :param function:
    :return:
    """
    # import tkinter as tk
    from PIL import Image, ImageTk  # needed to work with images that aren't GIF's
    import os
    import inspect
    import nrobotics
    import pickle

    def loadimage(imname):
        """
        Loads an image in another folder because Tkinter is finicky
        :param imname: image name
        :return: PhotoImage instance
        """
        return ImageTk.PhotoImage(
            # file=(imagedir + imname)
            Image.open(imagedir + imname)
        )

    def variable_documentation(docstring, variable):
        """
        Pulls the variable docstring from the function docstring
        :param docstring: full docstring
        :param variable: variable name
        :return: variable's docstring
        """
        if type(docstring) != list:
            docstring = docstring.split(':')  # split by : to allow for multi-line doc values
        for ind, line in enumerate(docstring):
            if line == ('param ' + variable):  # if the param matches the docstring entry
                return docstring[ind + 1].replace('\n', ' ')[1:-1]  # return the formatted following entry
        raise ValueError('No documentation was found for the variable "%s"' % variable)

    def interpret_value(val):
        """
        Attempts to interpret a value from the Entry box as list, float, or string
        :param val: retrieved value
        :return: interpreted value
        """
        if ',' in val:  # list of values
            return [interpret_value(i) for i in val.split(',')]  # interpret each separately
        try:  # try to convert to float
            return float(val)
        except ValueError:  # return original string
            if val[0] == ' ':  # remove leading space if merited
                return val[1:]
            return val

    def format_value(val):
        """formats a value for placement in a entry box"""
        if type(val) == list:  # if the value is a list, format appropriately
            out = ''
            for ind, item in enumerate(val):
                out += str(item)
                if ind + 1 != len(val):
                    out += ', '
            return out
        return str(val)  # otherwise, return the value

    def execute(event=None):
        """
        executes the function with the provided variables
        :param event:
        :return:
        """
        nonlocal output  # use parent function's output
        print('executing function')
        update_values()  # update the values
        output = function(  # execute function with the specified arguments
            *[boxes[name]['value'] for name in args]
        )
        print('fin.')

    def update_values(event=None):
        """updates the values from the entry boxes"""
        for name in boxes:
            # boxes[name]['previousvalue'] = boxes[name]['value']
            boxes[name]['value'] = interpret_value(
                boxes[name]['entrybox'].get()
            )

    def save_values(event=None):
        """saves the values in the pickel file"""
        update_values()  # update first
        with open(os.path.dirname(nrobotics.__file__) + '\\GUI\\' + function.__name__ + '_savedvalues.pkl', 'wb') as saved:
            pickle.dump(
                {name: boxes[name]['value'] for name in boxes},
                saved,
            )

    if not callable(function):  # check that a function was handed
        raise ValueError('The provided function "%s" is not a callable function' % str(function))

    output = None  # output storage

    insp = inspect.getfullargspec(function)
    args = insp.args  # arguments
    defaults = insp.defaults  # default values (as per function)
    fndocstring = inspect.getdoc(function)

    try:  # try to find saved values
        with open(os.path.dirname(nrobotics.__file__) + '\\GUI\\' + function.__name__ + '_savedvalues.pkl', 'rb') as saved:
            savedvalues = pickle.load(saved)
    except FileNotFoundError:  # if no saved values
        savedvalues = {}

    font = {  # font properties for buttons
        'font': ("Helvetica", 15),
        'width': 12,
    }

    imagedir = os.path.join(  # path for GUI images
        os.path.dirname(nrobotics.__file__) + '\\images\\'
    )
    root = tk.Tk()  # create Tkinter instance
    root.iconbitmap(default=(imagedir + 'icon.ico'))  # change window icon
    root.title("North Robotics - %s GUI" % function.__name__)  # window title
    root.geometry("850x500")  # size of window to open
    bkg = loadimage('Background.png')
    label0 = tk.Label(root, image=bkg)
    label0.place(x=0, y=0)


    xval = 10
    yval = 10
    boxes = {}  # dictionary of labels
    for ind, name in enumerate(args):
        boxes[name] = {
            'label': tk.Label(  # create label instance
                root,
                text=name
            ),
            'entrybox': tk.Entry(  # create entry box instance
                root,
                **font,
            ),
            'value': savedvalues[name] if name in savedvalues else None,  # saved value otherwise None
        }
        if boxes[name]['value'] is not None:
            boxes[name]['entrybox'].insert(0, format_value(boxes[name]['value']))
        elif defaults is not None and ind + 1 > len(args) - len(defaults):  # if there is a default value
            boxes[name]['entrybox'].insert(0, format_value(defaults[
                ind + len(defaults) - len(args)
            ]))
        else:
            boxes[name]['entrybox'].insert(0, 0)
        boxes[name]['label'].place(x=xval, y=yval)
        boxes[name]['entrybox'].place(x=xval, y=(yval + 20))
        boxes[name]['entrybox'].bind('<Return>', update_values)  # enter commits
        txt = variable_documentation(fndocstring, name)  # retrieve variable string
        if defaults is not None and ind + 1 > len(args) - len(defaults):  # if there is a default value
            txt += '\nDefault value: ' + str(defaults[
                ind + len(defaults) - len(args)
                ])
        Tooltip(  # create tooltip for variable entry
            boxes[name]['entrybox'],
            text=txt
        )
        yval += 60

    gobutton = tk.Button(  # function execution button
        root,
        text='Execute'
    )
    # gobutton.config(height=10, width=30)
    gobutton.place(x=10, y=375)
    gobutton.bind("<Button-1>", execute)

    menubar = tk.Menu(root)  # create Menu bar
    filemenu = tk.Menu(menubar, tearoff=0)  # create the file menu
    menubar.add_cascade(label="File", menu=filemenu)
    filemenu.add_command(  # add Save button
        label='Save',
        command=save_values
    )
    filemenu.add_command(
        label='Exit',
        command=root.quit
    )
    root.config(menu=menubar)

    tk.mainloop()  # open GUI
    return output

# from PyNR.dependencies.processing import pull_hplc_data_from_folder
# retrieved = make_gui(pull_hplc_data_from_folder)
