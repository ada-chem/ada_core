"""
A collection of calculations and methods to determine offsets and zeros for an N9 robot.
"""

from ada_core.dependencies.interface import controller
from ada_core.dependencies.arm import robotarm as n9
from ada_core.dependencies.coordinates import deck, calculate_offset
from ada_core.dependencies.general import intround
from ada_core.items.basic import Item


def x_offset(value):
    """sets the x offset to the specified value"""
    n9._x_offset = value
    n9.update_module_locations()


def y_offset(value):
    """sets the y offset to the specified value"""
    n9._y_offset = value
    n9.update_module_locations()


def el_straight_sh_left():
    """moves the elbow straight and the shoulder left"""
    n9.safeheight()
    n9.move_axes({
        'shoulder': sh_zero.middle - n9._shoulder.cpr / 4 - 1000,
        'elbow': el_zero.middle,
    })
    n9.move_to_location(height_2)
    n9.keyboard()
    n9.safeheight()


def el_straight_sh_right():
    """moves the elbow straight and the shoulder right"""
    n9.safeheight()
    n9.move_axes({
        'shoulder': sh_zero.middle + n9._shoulder.cpr / 4 + 1000,
        'elbow': el_zero.middle,
    })
    n9.move_to_location(height_2)
    n9.keyboard()
    n9.safeheight()


def sh_straight_el_left():
    """moves the shoulder straight and the elbow left"""
    n9.safeheight()
    n9.move_axes({
        'shoulder': sh_zero.middle,
        'elbow': el_zero.middle + n9._elbow.cpr / 4 - 0000,
    })
    n9.move_to_location(height_2)
    n9.keyboard()
    n9.safeheight()


def sh_straight_el_right():
    """moves the shoulder straight and the elbow right"""
    n9.safeheight()
    n9.move_axes({
        'shoulder': sh_zero.middle,
        'elbow': el_zero.middle - n9._elbow.cpr / 4 + 0000,
    })
    n9.move_to_location(height_2)
    n9.keyboard()
    n9.safeheight()


def pin_demo(gridref, z=63.5, wait=0.5):
    """pin placement demo with the shoulder inversion"""
    loc = deck[gridref]
    loc.offset(z=z)
    n9.safeheight(100.)
    # if n9.gripper.ingrip is None:
    #     input('Ready for gripper?')
    #     n9.gripper_on(pin)
    # controller.zzz(wait)
    n9.move_to_location(loc)
    while True:
        for i in range(4):
            n9.move_axes({'gripper': 1000 * i})
            controller.zzz(wait)
        n9.invert_elbow()


def update_elbow():
    """updates the elbow with the new zero"""
    n9._elbow.zero = el_zero.middle
    n9.update_module_locations()


def update_shoulder():
    """updates the shoulder with the new zero"""
    n9._shoulder.zero = sh_zero.middle
    n9.update_module_locations()


def straight_out():
    n9.safeheight()
    n9.move_axes({
        'shoulder': sh_zero.middle,
        'elbow': el_zero.middle,
    })


class Zero(object):
    def __init__(self, lower, upper):
        """
        A class for conveniently managing upper and lower bounds of a zeroing calculation

        :param lower: lower counts
        :param upper: upper counts
        """
        self._lower = None
        self._upper = None
        self.middle = None
        self.lower = lower  # lower bound
        self.upper = upper  # upper bound

    def __repr__(self):
        return f'{self.__class__.__name__}({self.lower}, {self.upper})'

    def __str__(self):
        return str(self.middle)

    @property
    def lower(self):
        return self._lower

    @lower.setter
    def lower(self, value):
        self._lower = intround(value)
        self._midpoint()

    @property
    def upper(self):
        return self._upper

    @upper.setter
    def upper(self, value):
        self._upper = intround(value)
        self._midpoint()

    def _midpoint(self):
        try:
            prev = self.middle
            self.middle = intround((self.lower + self.upper) / 2)
            if prev is not None:
                print(f'Previous: {prev}, New: {self.middle}, delta: {self.middle - prev}')
        except TypeError:
            self.middle = None


n9.armbias = 'elbow left'
# set offsets to zero
x_offset(0.28)
y_offset(0.11)
n9.modules.gripper.voffset = -20.
n9._vert.zero = intround(31050)
n9._gripper._initial = 0
n9._vert.axis_range = [-250, 26400]
n9._shoulder.axis_range = [-170, 67000]
n9._elbow.axis_range = [-65, 42220]

edge_finder = Item(length=55.)  # item describing the edge finder

pin = Item(length=6.)  # item describing the pin thingy

el_zero = Zero(7742, 34324)  # calculation for the zero of the elbow
sh_zero = Zero(8286, 58489)  # calculation for the zero of the shoulder
update_elbow()
update_shoulder()
height_2 = {'z': -6.}  # height for edge finder engagement

n9.safe_height = 35.  # set safe height

# 170 mm for both
