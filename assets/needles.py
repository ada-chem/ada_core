"""
Store asset definitions for any needles here.

Asset definitions must define a `type` or a `file` key with a location to a model file. Models are loaded inside of an
asset container can be positioned, rotated and scaled independently to resize and offset the model origin as needed.
The material and color of the model can also be changed with the `material` and `color` keys. Currently the only materials
are `default` and `glass`. The `color` can be an HTML-style hex string or name. Child assets can also be added to the
`children` key to created nested assets. Joints can be added to assets with the `joint` key.

Asset types:
    - empty
    - cube
    - cylinder

Detailed example:

    spin_coater = {
        # model locations are relative to the assets directory. STL and FBX files seem to work best.
        'file': './models/spin_coater.stl',
        # the model origin can be moved with `model_location` and `model_rotation`
        'model_location': {'x': -38.2, 'y': 37.3},
        # rotation can be a single number for a rotation about Y, or a dict with 'x' 'y' and 'z' keys
        'model_rotation': -90,
        # children is an array of asset definitions
        'children': [{
            'type': 'empty',
            # The name is used to generate IDs that can reference assets in the simulator. The first asset with a name
            # will have an ID matching that name, while subsequent assets with have a number appended (:0, :1, etc).
            'name': 'spin_coater_joint',
            # location can be used instead of `model_location` to position the model container (this is really only useful
            # for child assets)
            'location': {'x': -38.2, 'y': 78.55, 'z': 80},
            # optional joints can be added
            'joint': {
                # revolute is a spinning joint
                'type': 'revolute'
            }
        }]
    }
"""

# this is the base definition used for dynamic needles
needle = {
    'type': 'needle',
}


def build_needle(
        sleeve_height=16.7,
        diameter=4.35,
        material='Default',
        color=None,
        tipdiameter=5.,
        needle_length=5.,
        tipmaterial='Default',
        tipcolor='green',
        **kwargs):
    """
    Builds a needle asset description

    :param sleeve_height: height for the probe-needle sleeve
    :param diameter: inner diameter of the sleeve
    :param material: material for the needle in the simulator
    :param color:
    :param tipdiameter: diameter of the needle
    :param needle_length: length of the needle
    :param tipmaterial: material for the piercing part of the needle in the simulator
    :param tipcolor: colour for the needle tip
    :param kwargs:
    :return:
    """
    needle_diameter = max(diameter or 0, 1)
    return {
        'type': 'cylinder',  # needle
        'name': f'needle_{int(sleeve_height)}x{int(diameter or 0) or "N"}',
        'model_location': {'z': needle_length},
        'model_scale': {'x': needle_diameter, 'y': needle_diameter, 'z': sleeve_height},
        'material': material,
        'color': color,
        'children': [{
            'type': 'cylinder',  # tip
            # 'model_location': {'z': 0.},
            'model_scale': {'x': tipdiameter, 'y': tipdiameter, 'z': needle_length},
            'material': tipmaterial,
            'color': tipcolor,
        }]
    }