"""
Store asset definitions for any vials here.

Asset definitions must define a `type` or a `file` key with a location to a model file. Models are loaded inside of an
asset container can be positioned, rotated and scaled independently to resize and offset the model origin as needed.
The material and color of the model can also be changed with the `material` and `color` keys. Currently the only materials
are `default` and `glass`. The `color` can be an HTML-style hex string or name. Child assets can also be added to the
`children` key to created nested assets. Joints can be added to assets with the `joint` key.

Asset types:
    - empty
    - cube
    - cylinder

Detailed example:

    spin_coater = {
        # model locations are relative to the assets directory. STL and FBX files seem to work best.
        'file': './models/spin_coater.stl',
        # the model origin can be moved with `model_location` and `model_rotation`
        'model_location': {'x': -38.2, 'y': 37.3},
        # rotation can be a single number for a rotation about Y, or a dict with 'x' 'y' and 'z' keys
        'model_rotation': -90,
        # children is an array of asset definitions
        'children': [{
            'type': 'empty',
            # The name is used to generate IDs that can reference assets in the simulator. The first asset with a name
            # will have an ID matching that name, while subsequent assets with have a number appended (:0, :1, etc).
            'name': 'spin_coater_joint',
            # location can be used instead of `model_location` to position the model container (this is really only useful
            # for child assets)
            'location': {'x': -38.2, 'y': 78.55, 'z': 80},
            # optional joints can be added
            'joint': {
                # revolute is a spinning joint
                'type': 'revolute'
            }
        }]
    }
"""

# this is the base definition used for dynamic vials
vial = {
    'type': 'vial',
}

vial_hplc_2ml = {
    'type': 'vial_hplc_2ml',
}


def build_vial(
        diameter,
        height,
        material='Glass',
        color=None,
        capdiameter=None,
        capheight=5,
        capmaterial='Default',
        capcolor='blue',
        gapdiameter=None,
        gapheight=2,
        **kwargs):
    return {
        'type': 'cylinder',  # body
        'name': f'vial_{int(height)}x{int(diameter)}',
        'model_scale': {'x': diameter, 'y': diameter, 'z': height - capheight - gapheight},
        'material': material,
        'color': color,
        'children': [{
            'type': 'cylinder',  # gap
            'location': {'z': height - capheight - gapheight},
            'model_scale': {'x': gapdiameter or (diameter - 2), 'y': gapdiameter or (diameter - 2), 'z': gapheight},
            'material': material,
            'color': color,
        }, {
            'type': 'cylinder',  # cap
            'location': {'z': height - capheight},
            'model_scale': {'x': capdiameter or diameter, 'y': capdiameter or diameter, 'z': capheight},
            'material': capmaterial,
            'color': capcolor,
        }]
    }