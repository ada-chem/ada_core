import os
import threading
import queue
import time
import signal
import asyncio
import zmq
from zmq.utils.win32 import allow_interrupt

# set an environment variable so the timeline controller will try and connect to the simulator on a different port to
# allow the timeline controller and ada_core scripts to connect to the same simulator
os.environ['N9_SIMULATOR_ADDRESS'] = 'tcp://127.0.0.1:8887'
from ada_core.dependencies.arm import robotarm as n9
from ada_core.dependencies.exceptions import UnreachablePosition

# disable command waiting
n9.blocking = False

class TimelineController(object):
    def __init__(self, address="127.0.0.1", topic="timeline", port=8889, home_duration=0.5, verbose=False):
        """
        This class is the bridge between the N9 IDE timeline and ada_core that runs and simulates timelines.

        :param address: The address to bind to
        :param port: The base port to bind to
        :param topic: The name of the topic to use when publishing timeline state updates
        :param home_duration: The duration of time to wait while the robot is homing
        :param verbose: Print extra debugging information
        """
        self.address = address
        self.topic = topic
        self.server_port = port
        self.publisher_port = port + 1
        self.home_duration = home_duration
        self.verbose = verbose

        self.enabled = True
        self.running = True
        self.playing = False
        self.playing_start_time = None

        self.timeline_state = {
            'time': 0, 'duration': 0, 'command': ''
        }

        # the server thread responds to requests
        self.server_thread = threading.Thread(target=self.server_thread_handler)
        # the publisher thread publishes timeline state updates
        self.publisher_thread = threading.Thread(target=self.publisher_thread_handler)

        # a queue of messages to publish from the publisher thread
        self.publisher_queue = queue.Queue()

        # setup signal handlers to stop threads on application close
        signal.signal(signal.SIGINT, self.stop)
        signal.signal(signal.SIGTERM, self.stop)

    def start(self):
        """
        Start the server and publisher threads.
        """
        self.server_thread.start()
        self.publisher_thread.start()

    def stop(self):
        """
        Stop the timeline controller and kill the application. ZeroMQ connections seem to hang threads in Python,
        so we need to use an OS-level kill to exit our application (uggh).
        """
        if not self.running:
            return

        # stop the threads and wait 1 second for them to gracefully exit before killing the current process
        self.running = False
        self.server_thread.join(timeout=1)
        self.publisher_thread.join(timeout=1)
        os.popen(f'TASKKILL /PID {os.getpid()} /F')

    def server_thread_handler(self):
        """
        This is the server thread that handles receiving requests and sending responses.
        """

        # create a ZeroMQ context and socket, then bind it to the server address and port
        self.server_context = zmq.Context()
        self.server_socket = self.server_context.socket(zmq.REP)
        self.server_socket.bind(f'tcp://{self.address}:{self.server_port}')

        def quit():
            """
            Intercept ctrl-c style kill events from ZeroMQ and kill the application. Supposedly this hack is needed
            due to Python's GIL and ZeroMQ's blocking operations. Unfortunately, non-blocking operations didn't seem
            to fix this issue.
            """
            self.stop()

        # start the thread main loop, and keep looping until our running flag is False
        with allow_interrupt(quit):
            while self.running:
                # wait for a JSON message from the socket
                message = self.server_socket.recv_json()
                # handle the message and send the response back
                response = self.on_server_message(message)
                self.server_socket.send_json(response or { 'error': False, 'data': {} })

                # sleep for a bit to help limit CPU usage
                time.sleep(0.001)

        # close our sockets at this point, since the thread is exiting
        if not self.server_socket.closed:
            self.server_socket.close()
        if not self.server_context.closed:
            self.server_context.term()

    def publisher_thread_handler(self):
        """
        This is the publisher thread that publishes timeline state updates to the IDE while the timeline is running.
        """

        # start the publisher thread main loop and exit when running is False
        while self.running:
            # send out a timeline state message if we are playing
            if self.playing:
                self.timeline_state['playing'] = True
                self.timeline_state['time'] = time.time() - self.playing_start_time
                n9.robot_simulator.send_message(self.timeline_state, topic="timeline")

            # sleep a bit so we don't spam too many state messages (or kill too much CPU)
            time.sleep(0.05)

    def run_timeline(self, timeline):
        event_loop = asyncio.new_event_loop()
        asyncio.set_event_loop(event_loop)
        try:
            event_loop.run_until_complete(self.run_timeline_async(event_loop, timeline))
        finally:
            event_loop.close()

    async def run_timeline_async(self, event_loop, timeline):
        # loop through modules and commands (this currently only works with a single module)
        for module, commands in timeline['commands'].items():
            for command in commands:
                event_loop.call_later(command['time'], self.run_timeline_command, module, command)

        await asyncio.sleep(timeline['duration'])

    def run_timeline_command(self, module, command):
        try:
            # try dispatching this command to a 'on_MODULE_COMMAND_command' method
            command_handler = getattr(self, f'on_{module.lower()}_{command["name"]}_command')
            command_handler(command['parameters'])
        except AttributeError:
            pass
        
    def on_server_message(self, message):
        """
        Dispatches server request messages to the correct request methods.
        :param message: The message to dispatch
        """

        try:
            # try calling a 'on_MESSAGENAME_request' method with the request data
            method = message['method']
            handler = getattr(self, f'on_{method}_request')
            # return the response if we didn't run into errors
            return handler(message['data'])
        except AttributeError:
            # respond with an error if we couldn't find the method
            return {
                'error': f'Unknown method'
            }

    def on_set_module_parameter_request(self, param):
        """
        Request handler that sets module parameters.
        :param param: A dict of parameters to set
        :return: None or an error response
        """
        try:
            if param['module'] == 'Arm':
                if param['name'] == 'velocity':
                    n9.velocity = param['value']

                if param['name'] == 'acceleration':
                    n9.acceleration = param['value']

            n9.robot_simulator.set_module_parameter(param['module'], param['name'], param['value'])

        except Exception as error:
            print(error)
            return { 'error': True, 'data': {} }

    def on_simulate_timeline_request(self, data):
        """
        Request handler that simulates timelines and responds with command durations.
        :param data: Timeline data
        :return: Response with updated timeline durations, or an error reponse
        """
        # start the simulation
        n9.start_simulation()
        # loop through the timeline modules and commands (this only works for a single module right now)
        for module, commands in data['commands'].items():
            # track the previously run command so we can check for overlaps with the current command
            last_command = None
            for command in commands:
                command['error'] = False
                # check to see if this command overlaps with the previous one, and set the command error flag if it does
                if last_command and 'time' in last_command:
                    end_time = last_command['time'] + last_command.get('duration', 0)
                    if end_time > command['time']:
                        command['error'] = True

                last_command = command

                try:
                    # try dispatching this command to a 'on_MODULE_COMMAND_command' method that returns a duration with
                    # the simulate flag set
                    command_handler = getattr(self, f'on_{module.lower()}_{command["name"]}_command')
                    try:
                        command['duration'] = command_handler(command['parameters'])
                    except Exception as error:
                        print(error)
                        command['error'] = True
                except AttributeError:
                    continue

        n9.stop_simulation()

        # return a response with the updated timeline data
        return { 'error': False, 'data': data }

    def on_run_timeline_request(self, timeline):
        """
        Request handler that runs a timeline.
        :param timeline: Timeline data
        :return: Response with the timeline.
        """

        # setup an asyncio event_loop to
        # home the simulator and robot and wait for them to home
        n9.home()
        n9.robot_simulator.send_command('Gripper', 'Open')
        time.sleep(0.5)

        # store the current time as the playing start time so we can calculate the timeline time
        self.playing_start_time = start_time = time.time()
        # toggle the playing flag and update the timeline state wit the duration of the timeline
        self.playing = True
        self.timeline_state['duration'] = timeline['duration']

        self.run_timeline(timeline)

        # turn off the playing flag
        self.playing = False

        # send a timeline complete response
        return { 'error': False, 'timeline': 'complete' }

    def on_static_set_parameter_request(self, data):
        n9.robot_simulator.set_static_module_parameter(data['module'], data['name'], data['value'])
        return { 'error': False }

    def on_arm_home_command(self, params):
        """
        Command handler that homes the N9.
        :param params: Command parameters (not used in this case)
        :return: Command response
        """
        try:
            n9.home()
            return { 'duration': self.home_duration, 'error': False }
        except:
            return { 'duration': 0.0, 'error': True }

    def on_arm_move_command(self, location):
        """
        Command handler that moves the N9 arm horizontally.
        :param location: A dict with 'x' and 'y' coordinates in mm
        :return: Command response
        """
        return n9.move_to_location({
            'x': location['x'],
            'y': location['y'],
        })

    def on_arm_move_vert_command(self, location):
        """
        Command handler that moves the N9 arm vertically.
        :param location: A dict with a 'z' coordinate in mm
        :return: Command response
        """
        return n9.move_to_location({
            'z': location['z']
        })


    def on_gripper_spin_cw_command(self, params):
        return n9.spin_gripper(params['time'], params['rpm'], -1)

    def on_gripper_spin_ccw_command(self, params):
        return n9.spin_gripper(params['time'], params['rpm'], 1)

    def on_gripper_close_command(self, params):
        if not n9.simulate:
            n9.robot_simulator.send_command('Gripper', 'Close')

        return 0

    def on_gripper_open_command(self, params):
        if not n9.simulate:
            n9.robot_simulator.send_command('Gripper', 'Open')

        return 0

# create an instance of the TimelineController and start running
timeline_controller = TimelineController()
timeline_controller.start()