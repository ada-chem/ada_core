"""
Direct-inject sampling sequences which use the robot arm and a BD needle to draw from a location
"""
import warnings
import time
from ..dependencies.arm import robotarm
from ..dependencies.interface import controller
from ..dependencies.general import UnitFloat
from ..components.pumps import ValveSyringePump, ContinuousPump
from ..components.basic import OutputComponent, Plumbing
from ..components.valves import ActuatorValve, SerialValve


def dispense_volume(pump, volume, input_state='input', output_state='output', delay=0.5):
    """
    Dispenses the target volume using the provided pump instance. If a syringe pump was provided and the volume exceeds
    the maximum volume of the syringe, the pump will draw and dispense until the target volume is achieved.

    :param ContinuousPump or ValveSyringePump pump: pump instance
    :param float volume: volume to dispense (mL)
    :param str input_state: input state for pump (applies to valve-syringe pumps only)
    :param str output_state: output state for pump (applies to valve-syringe pumps only)
    :param float delay: additional delay after moving the pump and before performing the next move
    """
    if isinstance(pump, ContinuousPump):  # simple command for continuous pumps
        pump.start(
            volume,
            direction='push',
            wait=delay,
        )
    elif isinstance(pump, ValveSyringePump):
        maxvol = pump.maximum_volume * 0.98  # just a smidge less than maximum volume to avoid volume issues
        # perform max fill cycles
        for i in range(int(volume // maxvol)):
            pump.state_and_start(
                maxvol,
                input_state,
                'pull',
                delay,
            )
            pump.state_and_start(
                maxvol,
                output_state,
                'push',
                delay,
            )
        # fill remainder
        pump.state_and_start(
            volume % maxvol,
            input_state,
            'pull',
            delay,
        )
        pump.state_and_start(
            volume % maxvol,
            output_state,
            'push',
            delay,
        )
    else:
        raise TypeError(f'The pump type {type(pump)} is not recognized by the dispense volume method. ')


def condition_lines(*pumps, drawscalar=0.3, wastelocation=None):
    """
    Conditions a series of pumps in a backplug-type setup

    :arg pumps: a list of [pump instance, safepushflow, number of times to condition] lists
    :param drawscalar: scalar to use for determining draw flow rate
    :keyword wastelocation: location of the waste container
    """
    # todo make this function general to both syringe and continuous pumps
    def draw(pumpname):
        """tell the specified pump to draw"""
        print('Drawing pump %s #%d/%d' % (
            pumpname,
            pumps[pumpname]['execs'] + 1,
            pumps[pumpname]['n'],
        ))
        # change flowrate, set valve to input, and start the pump
        pumps[pumpname]['pump'].flowrate = pumps[pumpname]['pull']
        pumps[pumpname]['pump'].set_state('input')
        pumps[pumpname]['pump'].start(
            pumps[pumpname]['pump'].maximum_volume * 0.95,
            'pull',
            wait=False,
        )
        pumps[pumpname]['finisht'] = pumps[pumpname]['pump'].finish_time
        pumps[pumpname]['state'] = 0

    def push(pumpname):
        """Tell the specified pump to push"""
        pumps[pumpname]['execs'] += 1
        print('Pushing pump %s #%d/%d' % (
            pumpname,
            pumps[pumpname]['execs'],
            pumps[pumpname]['n'],
        ))
        # change flowrate, set valve to input, and start the pump
        pumps[pumpname]['pump'].flowrate = pumps[pumpname]['push']
        pumps[pumpname]['pump'].set_state('output')
        pumps[pumpname]['pump'].start(
            pumps[pumpname]['pump'].maximum_volume * 0.95,
            'push',
            wait=False,
        )
        pumps[pumpname]['finisht'] = pumps[pumpname]['pump'].finish_time
        pumps[pumpname]['state'] = 1

    def choose(pumpname):
        """Choose push or pull based on current state of pump"""
        if pumps[pumpname]['state'] == 1:
            draw(pumpname)
        else:
            push(pumpname)

    pumps = {
        p[0]: {
            'execs': 0,
            'n': p[2],
            'push': p[1],
            'pump': p[0],
            'pull': p[0].maximum_volume / drawscalar,
            'state': None,
        } for p in pumps
    }

    if wastelocation is not None:
        robotarm.safeheight()
        robotarm.move_to_location(
            wastelocation,
            target='probe',
        )

    print('Conditioning pumps %s' % pumps.keys())
    for pump in pumps:  # start draw on all pumps
        draw(pump)

    # loop while the number of executions is below target
    while any([pumps[pump]['execs'] < pumps[pump]['n'] for pump in pumps]):
        # retrieve finish times for any pumps below the target number
        times = {pump: pumps[pump]['finisht'] for pump in pumps if pumps[pump]['execs'] < pumps[pump]['n']}
        nextup = min(times, key=times.get)  # find next pump up
        controller.zzz(  # wait for that pump to finish
            pumps[nextup]['finisht'] - time.time(),
            'Waiting for pump %s' % nextup,
        )
        choose(nextup)  # perform the appropriate action

    controller.zzz(
        pumps[nextup]['finisht'] - time.time(),
        'Waiting for final pump movement %s' % nextup,
    )
    if wastelocation is not None:
        robotarm.safeheight()
    print('Done conditioning')


def generic_condition_lines(*pumps, n=5, drawtime=30, emptytime=60, contpumpvol=2., wastelocation=None):
    """
    Conditions the provided pumps.

    :param ValveSyringePump or ContinuousPump pumps: pump instances to condition
    :param n: number of times to condition cycle
    :param drawtime: draw time (s)
    :param emptytime: empty time (s)
    :param contpumpvol: volume to push through continuous pumps (mL)
    :param wastelocation: dictionary xyz location of waste to expell to (optional)
    """
    cont_pumps = [pump for pump in pumps if isinstance(pump, ContinuousPump)]
    osp_pumps = [pump for pump in pumps if isinstance(pump, ValveSyringePump)]

    if wastelocation is not None:
        robotarm.safeheight()
        robotarm.move_to_location(
            wastelocation,
            target='probe',
        )

    # condition continuous pumps
    for pump in cont_pumps:
        pump.flowrate = (
            contpumpvol
            / (n * emptytime / 60.)
        )
        pump.start(
            contpumpvol,
            'push'
        )

    # condition output syringe pumps
    for i in range(n):
        print(f'Conditioning pumps {i+1}/{n}')
        for pump in osp_pumps:
            pump.flowrate = (
                pump.maximum_volume
                * 0.95
                / (drawtime / 60.)
            )
            pump.state_and_start(
                pump.maximum_volume * 0.95,
                'input',
                'pull',
            )
        pump.wait_for()  # wait for last pump
        for pump in osp_pumps:
            pump.flowrate = (
                pump.maximum_volume
                * 0.95
                / (emptytime / 60.)
            )
            pump.state_and_start(
                pump.maximum_volume * 0.95,
                'output',
                'push',
            )
        pump.wait_for()  # wait for last pump

    print('Done conditioning')


class BackplugSequence(object):
    def __init__(self, pushpump, samplepump, samplevalve, hplcvalve, hplctrigger, waste_location=None,
                 sample_draw_volume=None,
                 push_volume_to_HPLC=None,
                 sample_draw_rate=2.5,
                 sample_empty_rate=2.5,
                 push_propell_rate=1.25,
                 solvent_draw_rate=2.,
                 airgap=0.05,
                 sample_loop_rinse_volume=0.2,
                 push_line_rinse_volume=0.5,
                 wait=6.,
                 sample_valve_inline=2,
                 sample_valve_bypass=1,
                 hplc_valve_inline=1,
                 hplc_valve_bypass=2,
                 safeheight=None,
                 reinject=False,
                 tubing=None,
                 **kwargs,
                 ):
        """
        A backplug driven sampling sequence.
        This sequence involves drawning a backing solvent into the sample syringe from a reservoir and this
        solvent is used as an incompessible backer when drawing liquid through the needle. The sample loop rinse is
        accomplished by expelling the backplug solvent through the needle and into a waste container located on the
        robot bed.

        :param pushpump: a pump instance that pushes the sample plug to the HPLC valve
        :param samplepump: a pump instance that draws sample into the sample loop
        :param tubing: a tubing instance
        :param waste_location: xyz dict location of a waste container
        :param safeheight: safeheight for the arm
        :param sample_draw_rate: sample draw flow rate (mL/min)
        :param sample_empty_rate: sample clear flow rate (mL/min)
        :param push_propell_rate: push pump propelling flow rate (mL/min)
        :param airgap: air gap when refilling the sample container (mL)
        :param sample_loop_rinse_volume: loop rinse volume (mL)
        :param push_line_rinse_volume: line rinse volume (mL)
        :param solvent_draw_rate: A safe draw rate for drawing from the solvent bottle into the syringes (mL/min)
        :param sample_draw_volume: The volume required to draw sample from the sample container and completely fill the
            sample loop in the arm valve. (mL)
        :param push_volume_to_HPLC: The volume required to push the sample plug from the sample loop in the arm valve
            to the sample loop in the HPLC valve. (mL)

        :param sample_valve_inline: position for sample valve where the sample is in-line with the line going to the HPLC
        :param sample_valve_bypass: position for the sample valve where the sample is in line with the needle and sample pump
        :param hplc_valve_inline: position for the hplc valve where the sample loop is in line with the HPLC
        :param hplc_valve_bypass: position for the hplc valve where the sample loop is bypassing the HPLC
        """
        # todo separate into DirectInject and ArmDirectInject classes
        # todo write else catches
        if isinstance(pushpump, ValveSyringePump): # or isinstance(pushpump, ContinuousPump):
            self.pushpump = pushpump

        if isinstance(samplepump, ValveSyringePump) or isinstance(samplepump, ContinuousPump):
            self.samplepump = samplepump

        if isinstance(tubing, Plumbing):
            self.tubing = tubing

        if isinstance(samplevalve, ActuatorValve) or isinstance(samplevalve, SerialValve):
            self.samplevalve = samplevalve

        if isinstance(hplcvalve, ActuatorValve) or isinstance(hplcvalve, SerialValve):
            self.hplcvalve = hplcvalve

        if isinstance(hplctrigger, OutputComponent):
            self.hplctrigger = hplctrigger

        if tubing is not None:  # legacy support
            warnings.warn('Plumbing-style volume specification is being depreciated. Set sample_draw_volume and '
                          'push_volume_to_HPLC directly.', DeprecationWarning)
            self.sample_draw_volume = self.tubing.volume(
                'arm loop',
                'needle loop'
            )
            self.push_volume_to_HPLC = self.tubing.volume(
                'arm HPLC valve'
            )
        else:
            self.sample_draw_volume = sample_draw_volume
            self.push_volume_to_HPLC = push_volume_to_HPLC

        # legacy catches
        if waste_location is None:
            if 'wastelocation' in kwargs:
                waste_location = kwargs['wastelocation']
        if 'sampledraw' in kwargs:
            sample_draw_rate = kwargs['sampledraw']
        if 'sampleclear' in kwargs:
            sample_empty_rate = kwargs['sampleclear']
        if 'pushdraw' in kwargs:
            solvent_draw_rate = kwargs['pushdraw']
        if 'pushpropell' in kwargs:
            push_propell_rate = kwargs['pushpropell']
        if 'safedraw' in kwargs:
            pass  # duplicate of solvent draw rate
        if 'looprinse' in kwargs:
            sample_loop_rinse_volume = kwargs['looprinse']
        if 'linerinse' in kwargs:
            push_line_rinse_volume = kwargs['linerinse']
        if 'svinline' in kwargs:
            sample_valve_inline = kwargs['svinline']
        if 'svbypass' in kwargs:
            sample_valve_bypass = kwargs['svbypass']
        if 'hplcinline' in kwargs:
            hplc_valve_inline = kwargs['hplcinline']
        if 'hplcbypass' in kwargs:
            hplc_valve_bypass = kwargs['hplcbypass']
        if 'wastelocation' in kwargs:
            waste_location = kwargs['wastelocation']

        self.waste_location = waste_location  # location to expel waste to
        self.safe_height = safeheight  # safe height to use for movements
        self.solvent_draw_rate = solvent_draw_rate  # draw rate from pumps
        self.sample_draw_rate = sample_draw_rate  # draw rate from sample location
        self.sample_empty_rate = sample_empty_rate  # expel rate for sample pump (through probe)
        self.push_propell_rate = push_propell_rate  # rate for push pump
        self.airgap = airgap  # airgap to use when drawing sample
        self.sample_loop_rinse_volume = sample_loop_rinse_volume  # rinse volume for sample line
        self.push_line_rinse_volume = push_line_rinse_volume  # rinse volume for push line
        self.wait = wait  # wait time after drawing sample
        self.sample_reinject = reinject  # whether to reinject air into the sample container to equalize pressure

        self.sample_valve_inline = sample_valve_inline
        self.sample_valve_bypass = sample_valve_bypass
        self.hplc_valve_inline = hplc_valve_inline
        self.hplc_valve_bypass = hplc_valve_bypass

        self.home_valves()  # home the valves

    def __repr__(self):
        return f'{self.__class__.__name__}()'

    def __str__(self):
        return 'Backplug arm direct-inject sampling sequence'

    def __call__(self, *args, **kwargs):
        return self.standard_sequence(*args, **kwargs)

    # LEGACY ATTRIBUTES AND METHODS ===================

    # todo implement warnings for deprecation
    @property
    def svinline(self):
        warnings.warn(f'The svinline property has been renamed to sample_valve_inline ', DeprecationWarning)
        return self.sample_valve_inline

    @property
    def looprise(self):
        warnings.warn('The looprise property has been renamed to sample_loop_rinse_volume', DeprecationWarning)
        return self.sample_loop_rinse_volume

    @property
    def linerinse(self):
        warnings.warn('The linerinse property has been renamed to push_line_rinse_volume', DeprecationWarning)
        return self.push_line_rinse_volume

    def prime_push(self, *args, **kwargs):
        warnings.warn('The prime_push method has been renamed to prime_push_pump', DeprecationWarning)
        return self.prime_push_pump(*args, **kwargs)

    def withdraw_sample(self, *args, **kwargs):
        warnings.warn('The withdraw_sample method has been renamed to withdraw_sample_from_location',
                      DeprecationWarning)
        return self.withdraw_sample_from_location(*args, **kwargs)

    def push_to_hplc(self, *args, **kwargs):
        warnings.warn('The push_to_hplc method has been renamed to push_volume_to_hplc', DeprecationWarning)
        return self.push_volume_to_hplc(*args, **kwargs)

    def prime_sample(self, *args, **kwargs):
        warnings.warn('The prime_sample method has been renamed to prime_sample_pump', DeprecationWarning)
        return self.prime_sample_pump(*args, **kwargs)

    def extra_rinse(self, *args, **kwargs):
        warnings.warn('The extra_rinse method has been renamed to extra_probe_rinse', DeprecationWarning)
        return self.extra_probe_rinse(*args, **kwargs)

    # END LEGACY ATTRIBUTE/METHOD RETRIEVAL ====================

    def condition_lines(self, push=4, sample=10):
        """
        Conditions the lines with the backing solvent

        :param push: number of times to cycle the push pump
        :param sample: number of times to cycle the sample pump
        :return:
        """
        # condition lines
        condition_lines(
            [self.pushpump, 1.5, push],
            [self.samplepump, 1., sample],
            wastelocation=self.waste_location,
        )

    def home_syringes(self):
        """Homes the syringes"""
        self.samplepump.home()
        controller.zzz(0.5)
        self.pushpump.home()

    def home_valves(self):
        """Sets the valves to their home state (the state that they need to be in prior to a sampling sequence)"""
        self.samplevalve.valve_position(self.sample_valve_bypass)
        self.hplcvalve.valve_position(self.hplc_valve_bypass)

    def trigger(self):
        """
        Executes the HPLC trigger sequence

        * switch HPLC valve (inline with HPLC)
        * send HPLC trigger signal

        :return: HPLC trigger time (relative to epoch)
        """
        self.hplcvalve.valve_position(self.hplc_valve_inline)
        self.hplctrigger.pulse(1)
        return time.time()

    def prime_push_pump(self, volume=None, wait=False):
        """
        Primes the push pump with the volume specified of the backing solvent. If no volume is specified,
        `push_volume_to_HPLC` will be used.

        :param float volume: volume to fill the syringe with
        :param bool wait: whether to wait for completion
        :return: duration for the pump motion
        :rtype: float
        """
        if volume is None:
            volume = self.push_volume_to_HPLC
        print(f'Priming push pump with {volume} mL.')
        return self.pushpump.state_and_start(
            volume,
            'input',
            'pull',
            wait,
            flowrate=self.solvent_draw_rate,
        )

    def prime_sample_pump(self, volume=None, wait=False):
        """
        Primes the sample pump with the volume specified of the backing solvent. If no volume is provided,
        `sample_loop_rinse_volume` will be used.

        :param float volume: volume to fill the syringe with
        :param bool wait: wait for completion
        :return: duration for the pump motion
        :rtype: float
        """
        if volume is None:
            volume = self.sample_loop_rinse_volume
        print(f'Priming sample pump with {volume} mL.')
        return self.samplepump.state_and_start(
            volume,
            'input',
            'pull',
            wait,
            flowrate=self.solvent_draw_rate
        )

    def withdraw_sample_from_location(self, location, volume=None, wait=None):
        """
        Withdraws the provided volume of sample from the specified location.

        :param dict or Location location: xyz location dictionary
        :param float volume: volume to withdraw. If no volume is specified, `sample_draw_volume` will be used.
        :param float wait: extra wait time after withdrawing sample. If not specified, uses `wait` default value.
        """
        robotarm.move_to_location(  # move to location
            location,
            target='gripper',
        )
        if volume is None:  # determine volume
            volume = self.sample_draw_volume
        if wait is None:
            wait = self.wait
        print(f'Withdrawing {volume} sample at {self.sample_draw_rate} mL/min')
        if type(self.samplepump) is ValveSyringePump:
            self.samplepump.state_and_start(  # withdraw
                volume,
                'output',
                'pull',
                wait,
                flowrate=self.sample_draw_rate,
            )
        elif type(self.samplepump) is ContinuousPump:
            self.samplepump.start(
                volume=volume,
                direction='pull',
                wait=True,
                flowrate=self.sample_draw_rate,
            )

    def empty_sample_to_waste(self, waste_location=None, watch_for_trigger=False, watchfortrigger=False):
        """
        Empties the sample to waste. This method may also be instructed to watch for a trigger time to improve
        efficiency of movements. When a value is provided to `watch_for_trigger`, the sample will be emptied to waste
        while also watching the current time to see whether the provided time has been reached. If the time has been
        reached, the `trigger` method will be called during this method.

        :param waste_location: location to empty waste to. If this is not specified, the default `waste_location` will
            be used.
        :param False or float watch_for_trigger: whether to watch for a HPLC trigger time. If this functionality is
            desired, specify a float defining the arrival time of the sample plug in the HPLC sample loop.

        :return: trigger time (if the `trigger` method was called)
        :rtype: None or float
        """
        if watchfortrigger is not False:
            warnings.warn('The watchfortrigger kwarg has been renamed to watch_for_trigger', DeprecationWarning)
            watch_for_trigger = watchfortrigger
        print(f'Emptying sample to waste. ')
        triggert = None
        robotarm.safeheight(self.safe_height, target='probe')
        if waste_location is None:
            waste_location = self.waste_location
        robotarm.move_to_location(
            waste_location,
            target='probe',
        )

        if all([
            watch_for_trigger is not False,  # if the method is watching for a trigger time
            watch_for_trigger - time.time() <= 0.,  # and the trigger time has passed
            triggert is None,  # and the HPLC has not been triggered
        ]):
            triggert = self.trigger()

        self.samplepump.set_state('output')  # set syringe to output position
        sft = self.samplepump.empty(  # start sample pump empty
            flowrate=self.sample_empty_rate,  # change flow rate
        ) + time.time()

        # if watching for a trigger time, check whether this will occur prior to the sample pump finishing
        if all([
            watch_for_trigger is not False,  # if the method is watching for a trigger time
            triggert is None,  # and the HPLC has not been triggered
            watch_for_trigger < sft,  # and the trigger time will occurr before the pump finished
        ]):
            controller.zzz(  # wait for the plug to reach the HPLC and trigger
                watch_for_trigger - time.time(),
                'Wait for sample to reach the HPLC'
            )
            triggert = self.trigger()

        # wait for the sample to finish emptying
        controller.zzz(
            sft - time.time(),
            'Wait for sample syringe to finish emptying to waste'
        )
        robotarm.safeheight(self.safe_height)
        return triggert

    def refill_container(self, location, volume=None, watch_for_trigger=False):
        """
        Pumps an air plug into the specified location using the following sequence:

        * Prime the sample pump with a rinse plug
        * Draws airgap plus refill volume
        * Injects it into location
        * Empties the remainder of the syringe to waste.

        During execution of this method, it may also be instructed to watch for a trigger time using the
        `watch_for_trigger` keyword argument to improve efficiency of the sequence. To enable this functionality,
        provide a `float` value corresponding to the the time since the epoch when the sample plug will arrive at the
        HPLC. If this time occurs during the execution of this method, the `trigger` method will be called and the
        trigger time will be passed through as the return of this method.

        :param location: xyz location dictionary
        :param volume: volume to refill. If this is not specified, `sample_draw_volume` will be used.
        :param False or Float watch_for_trigger: whether to watch for a HPLC trigger time. If this functionality is
            desired, specify a float defining the arrival time of the sample plug at the HPLC sample loop.

        :return: trigger time if `trigger` was called during execution, otherwise returns None
        :rtype: None or float
        """
        triggert = None
        if volume is None:
            volume = self.sample_draw_volume
        print(f'Reinjecting {volume} mL air.')
        robotarm.safeheight(self.safe_height)
        # draw refill volume and airgap
        self.samplepump.state_and_start(
            volume + self.airgap,
            'output',
            'pull',
            True,
            flowrate=self.sample_draw_rate,
        )

        # if the function is watching for a trigger time and the time arrives
        if watch_for_trigger is not False and watch_for_trigger - time.time() <= 0. and triggert is None:
            triggert = self.trigger()

        # move to location and reinject volume
        robotarm.move_to_location(
            location,
            target='probe',
        )
        # if the function is watching for a trigger time and the time arrives
        if watch_for_trigger is not False and watch_for_trigger - time.time() <= 0. and triggert is None:
            triggert = self.trigger()

        self.samplepump.state_and_start(
            volume,
            'output',
            'push',
            wait=True,
            flowrate=self.sample_draw_rate,
        )
        # if the function is watching for a trigger time and the time arrives
        if watch_for_trigger is not False and watch_for_trigger - time.time() <= 0. and triggert is None:
            triggert = self.trigger()

        # empty to waste, then rinse
        if watch_for_trigger is not False and triggert is None:
            triggert = self.empty_sample_to_waste(watch_for_trigger=watch_for_trigger)
        else:
            self.empty_sample_to_waste()
        # prime with rinse plug, then empty plug plus airgap to waste
        sft = self.prime_sample_pump()
        if watch_for_trigger is not False and triggert is None and watch_for_trigger < sft:
            controller.zzz(  # wait for the plug to reach the HPLC and trigger
                watch_for_trigger - time.time(),
                'Wait for sample to reach the HPLC'
            )
            triggert = self.trigger()
        # wait for the sample to finish emptying
        controller.zzz(
            sft - time.time(),
            'Wait for sample syringe to finish emptying to waste'
        )
        if watch_for_trigger is not False and triggert is None:
            triggert = self.empty_sample_to_waste(watch_for_trigger=watch_for_trigger)
        else:
            self.empty_sample_to_waste()
        return triggert

    def push_volume_to_hplc(self, volume=None, wait=False):
        """
        Pushes the provided volume towards the HPLC using the push pump.

        :param float volume: volume to push toward the HPLC. If this is not specified, `push_volume_to_HPLC` will be
            used.
        :param bool wait: whether to wait for the pump to finish
        :return: time to complete the pump motion if `wait` was `False`, otherwise returns `None`
        :rtype: float or None
        """
        if volume is None:
            volume = self.push_volume_to_HPLC
        print(f'Pushing {volume} towards HPLC.')
        return self.pushpump.state_and_start(
            volume,
            'output',
            'push',
            wait,
            flowrate=self.push_propell_rate,
        )

    def extra_probe_rinse(self, n=3):
        """
        Performs an extra rinse sequence of the dispenser probe

        :param n: number of extra times to rinse
        """
        robotarm.safeheight(self.safe_height)  # go to safe height
        robotarm.move_to_location(  # move probe to location
            self.waste_location,
            target='probe',
        )
        print(f'Performing {n} extra probe rinses')
        for i in range(n):  # fill and empty
            self.prime_sample_pump(
                self.samplepump.maximum_volume * 0.9,
                True,
            )
            self.samplepump.set_state('output')
            self.samplepump.empty(
                wait=0.5,
                flowrate=self.sample_empty_rate,
            )

    def extra_probe_rinse_continuous(self, volume=2., flowrate=4.):
        """
        Performs an extra rinse sequence of the dispenser probe using a continuous pump

        :param volume: rinse solvent volume in mL
        :param flowrate: rinsing flowrate in mL/min
        """
        robotarm.safeheight(self.safe_height)  # go to safe height
        robotarm.move_to_location(  # move probe to location
            self.waste_location,
            target='probe',
        )
        print(f'Rinsing probe with {volume}mL of solvent at {flowrate}mL/min...')
        self.samplepump.start(volume=volume, direction='push', wait=True, flowrate=flowrate)

    def standard_sequence(self,
                          location,
                          samplevol=None,
                          pushvol=None,
                          refill=False,
                          extrarinse=3,
                          wait=None,
                          ):
        """
        Executes a standard sampling sequence:

        * Sets the valves to the required position for sampling
        * Primes the push pump with the required volume
        * Withdraws sample from the specified location
        * Switches the sample valve to be in line towards the HPLC valve
        * Pushes the sample plug towards the HPLC valve
        * Empties the remaining sample to waste
        * Waits for the sample plug to reach the HPLC valve
        * Switches the HPLC valve to be in line with the HPLC column
        * Triggers the acquisition
        * Rinses the push line
        * Rinses the sample/probe line

        :param Location, dict location: location to withdraw from
        :param float samplevol: sample volume to withdraw. If this is not specified, `sample_draw_volume`
            will be used.
        :param bool refill: whether to refill the vial with a volume of air corresponding to the removed liquid
        :param int extrarinse: number of extra probe rinses to perform (calls extra_rinse)
        :param float wait: extra wait time after withdrawing sample. If not specified, uses `wait` default instance
            value.
        :return: hplc trigger time since the epoch
        :rtype: float
        """
        self.home_valves()  # set initial valve positions
        pft = self.prime_push_pump(volume=pushvol) + time.time()  # start priming push pump
        self.withdraw_sample_from_location(  # withdraw the sample
            location=location,
            volume=samplevol,
            wait=wait,
        )
        controller.zzz(
            pft - time.time(),
            'Wait for push pump to finish filling',
        )
        self.samplevalve.valve_position(self.sample_valve_inline)  # switch sample loop to be in line to HPLC
        pft = self.push_volume_to_hplc(volume=pushvol) + time.time()  # push to HPLC
        triggertime = self.empty_sample_to_waste(watch_for_trigger=pft)
        if pft - time.time() <= 0.:
            triggertime = self.trigger()

        if refill is True:  # if refill is called for
            if triggertime is None:
                triggertime = self.refill_container(location, samplevol, watch_for_trigger=pft)
            else:
                self.refill_container(location, samplevol)

        if triggertime is None:
            controller.zzz(
                pft - time.time(),
                'Wait for sample plug to reach HPLC'
            )
            triggertime = self.trigger()

        pft = self.prime_push_pump(self.push_line_rinse_volume) + time.time()
        sft = self.prime_sample_pump(self.sample_loop_rinse_volume) + time.time()
        controller.zzz(
            max([pft, sft]) - time.time(),
            'Wait for syringes to fill prior to rinsing'
        )
        self.push_volume_to_hplc(self.push_line_rinse_volume)
        self.empty_sample_to_waste()
        self.extra_probe_rinse(extrarinse)  # perform extra rinses
        robotarm.safeheight()
        return triggertime

    def one_thing_at_a_time(self, location, samplevol=None, triggerwait=0.):
        """
        Executes a sampling that sequence that prioritizes sample movement before rinsing.

        :param location: location to withdraw from
        :param samplevol: sample volume (optional)
        :param triggerwait: wait time after pushing and prior to triggering (allows push line to equilibrate pressure)
        :return: elapsed time, hplc trigger time
        """
        start = time.time()
        self.prime_push_pump(wait=True)
        self.withdraw_sample_from_location(
            location,
            samplevol,
        )
        controller.zzz(
            self.wait,
            'Wait for sample syringe to equalize'
        )
        self.samplevalve.valve_position(self.sample_valve_inline)  # switch valve position
        self.push_volume_to_hplc(wait=True)
        controller.zzz(
            triggerwait,
            'Wait for push line to equilibrate'
        )
        triggertime = self.trigger()
        self.samplevalve.valve_position(self.sample_valve_bypass)

        self.empty_sample_to_waste()
        self.refill_container(location, samplevol)

        pft = self.prime_push_pump(self.push_line_rinse_volume) + time.time()
        sft = self.prime_sample_pump(self.sample_loop_rinse_volume) + time.time()
        controller.zzz(
            max([pft, sft]) - time.time(),
            'Wait for syringes to fill prior to rinsing'
        )
        self.push_volume_to_hplc(self.push_line_rinse_volume)
        self.empty_sample_to_waste()
        robotarm.safeheight()
        return time.time() - start, triggertime

    def back_to_basics(self, location, samplevol=None, triggerwait=0.):
        """
        Sampling sequence with no vial refill and leaving the HPLC valve in line with HPLC until the start of the next
        sampling sequence.

        :param location: location to withdraw from
        :param samplevol: sample volume (optional)
        :param triggerwait: wait time after pushing and prior to triggering (allows push line to equilibrate pressure)
        :return: elapsed time, hplc trigger time
        """
        start = time.time()
        self.prime_push_pump(wait=True)
        self.withdraw_sample_from_location(
            location,
            samplevol,
        )
        controller.zzz(
            self.wait,
            'Wait for sample syringe to equalize'
        )
        self.samplevalve.valve_position(self.sample_valve_inline)  # switch valve position
        self.push_volume_to_hplc(wait=True)
        controller.zzz(
            triggerwait,
            'Wait for push line to equilibrate'
        )
        triggertime = self.trigger()
        self.samplevalve.valve_position(self.sample_valve_bypass)

        self.empty_sample_to_waste()

        pft = self.prime_push_pump(self.push_line_rinse_volume) + time.time()
        sft = self.prime_sample_pump(self.sample_loop_rinse_volume) + time.time()
        controller.zzz(
            max([pft, sft]) - time.time(),
            'Wait for syringes to fill prior to rinsing'
        )
        self.push_volume_to_hplc(self.push_line_rinse_volume)
        self.empty_sample_to_waste()
        robotarm.safeheight()
        return time.time() - start, triggertime

    def one_way_only(self, samplevol=None, triggerwait=0.):
        """
        Sequence that is nothing but pull sample and send to HPLC

        :param samplevol: sample volume (optional)
        :param triggerwait: wait time after pushing and prior to triggering (allows push line to equilibrate pressure)
        :return: elapsed time, hplc trigger time
        """
        # self.hplcvalve.valve_position(self.hplcbypass)  # set valve position
        start = time.time()
        self.prime_push_pump(wait=True)  # prime push
        if samplevol is None:  # determine volume
            samplevol = self.sample_draw_volume
        self.samplepump.state_and_start(  # draw sample
            samplevol,
            'output',
            'pull',
            wait=True,
            flowrate=self.sample_draw_rate,
        )
        controller.zzz(
            self.wait,
            'Wait for sample syringe to equalize'
        )
        self.samplevalve.valve_position(self.sample_valve_inline)  # switch valve position
        self.push_volume_to_hplc(wait=True)  # push to HPLC
        # controller.zzz(
        #     triggerwait,
        #     'Wait for push line to equilibrate'
        # )
        triggertime = self.trigger()  # trigger HPLC
        self.samplevalve.valve_position(self.sample_valve_bypass)  # switch arm valve back

        self.samplepump.state_and_start(  # empty sample
            samplevol,
            'input',
            'push',
            flowrate=self.solvent_draw_rate,
        )
        self.prime_push_pump(  # prime line rinse plug
            self.push_line_rinse_volume,
            wait=True,
        )
        self.push_volume_to_hplc(self.push_line_rinse_volume)  # empty line rinse
        return time.time() - start, triggertime
