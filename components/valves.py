"""
Class definitions for defining valve components.
"""
from ..components.basic import SerialComponent, OutputComponent
from ..components.joints import RevoluteJoint
from ..dependencies.interface import controller
from ..dependencies.motion import trajectory_duration


class SerialValve(SerialComponent):
    def __init__(self, comport, commandset, home=1):
        """
        Interacts with a serially controlled valve (e.g. Vici valve)

        :param int comport: com port (see SerialComponent)
        :param commandset: command set to use (see SerialComponent)
        """
        SerialComponent.__init__(
            self,
            comport,
            commandset,
        )
        self.home_position = home
        self.valve_home()

    def valve_position(self, position):
        """
        Changes the position of the valve to that specified. See SerialComponent for more details

        :param position: position key
        :return: read serial value
        """
        return self.send(position)

    def valve_home(self, position=None):
        """
        Sends the valve to the home position

        :param position: if None, retrieves default value
        :return: read serial value
        """
        if position is None:
            position = self.home_position
        return self.send(
            position
        )

    def __repr__(self):
        return f'{self.__class__.__name__}({self.comport})'

    def __str__(self):
        return f'{self.__class__.__name__} on comport {self.comport}'


class ActuatorValve(RevoluteJoint):
    def __init__(self, axis, **kwargs):
        """
        Interacts with a valve controlled by an actuator

        :param Communicator instance comminstance: Communicator instance
        :param int axis: axis number
        :param kwargs:  RevoluteJoint kwargs
        """
        RevoluteJoint.__init__(self, **kwargs)

        # todo allow for user specification of positions (or profile specification
        self.positions = {
            1: 70,
            2: -10,
        }
        self.axis = axis

    def __repr__(self):
        return f'{self.__class__.__name__}({self.axis}, {self.degrees}\u00b0, {self.position()})'

    def __str__(self):
        return f'{self.__class__.__name__} on axis {self.axis} at {self.degrees}\u00b0, {self.position()} counts'

    def valve_position(self, position):
        mopy = self.goto_degrees(
            self.positions[position]
        )
        # todo make a better catch for no movement (check before calculating mopy)
        if len(mopy) == 0:  # if there is no movement
            return
        controller.execute(
            'move',
            self.axis,
            *mopy,
        )
        controller.start_axes(self.axis)
        controller.zzz(trajectory_duration(mopy))

    def valve_home(self, position=-80):
        """
        Sends the valve to the home position and resets the position

        :param position: position (degrees) to go to
        """
        mopy = self.goto_degrees(position)
        controller.execute(
            'move',
            self.axis,
            *mopy,
        )
        controller.start_axes(self.axis)
        controller.zzz(trajectory_duration(mopy))
        self.home()  # sets position to zero


class OutputValve(OutputComponent):
    def __init__(self,
                 output,
                 positions='valve',
                 home_position=0,
                 **kwargs,
                 ):

        OutputComponent.__init__(
            self,
            output,
            positions=positions,
            **kwargs
        )
        self.home_position = home_position

    def toggle(self, delay=None):
        """not implemented for the OutputSyringePump class"""
        pass

    def pulse(self, delay=None):
        """not implemented for the OutputSyringePump class"""
        pass

    def valve_position(self, position):
        """Move the valve to the specified position"""
        self.set_state(position)

    def valve_home(self):
        """
        Sends the valve to the home position
        """
        self.set_state(self.home_position)

