"""
Basic functionality components. These are usually incorporated into other components (further abstracted) prior to use.
"""
import importlib
from ..dependencies.interface import controller
from ..dependencies.general import UnitFloat
from ..items.basic import Cylinder
from ..profiles import outputs


class Plumbing(object):
    def __init__(self, *incoming):
        """
        A class that describes the array of tubing used in a sampling setup.
        The class can then be used to caculate volumes associated with a tubing run
        or time to travel the length of tubing based on a given flow rate.

        :param sections: optional input of sections
        """
        self.tubingprofiles = getattr(
            importlib.import_module('profiles.tubing'),
            'diameters',
        )

        self.sections = {}
        if len(incoming) > 0:  # if an incoming series of sections was provided
            for section in incoming:  # add the sections
                if type(section) == list:  # if an ordered list of arguments
                    self.add_section(*section)
                elif type(section) == dict:  # if a dictionary
                    self.add_section(**section)

    def __repr__(self):
        return f'{self.__class__.__name__}({self.length()}, {self.volume()})'

    def __str__(self):
        return f'{self.__class__.__name__} instance: {self.length()} contained volume, {self.volume()} of tubing'

    def __getitem__(self, item):
        return self.sections[item]

    def __getattr__(self, item):
        if item in self.__dict__:
            return self.__dict__[item]
        elif item in self.sections:
            return self.sections[item]
        else:
            raise AttributeError(f'The {self.__class__.__name__} instance does not have an {item} attribute.')

    def add_section(self, name, length=0., tubingname=None, volume=None, innerdiameter=0.):
        """
        Adds a tubing section to the storage dictionary

        :param name: unique name for the section
        :param length: length of the tubing section (mm)
        :param tubingname: unique name of the tubing (see profiles.tubing)
        :param volume: optional volume if already known (convenient for injection loops of known volume)
        :param innerdiameter: optional can be speicifed instead of using a tubing profile
        """
        if name in self.sections or name in self.__dict__:
            raise KeyError(f'The provided name "{name}" is already contained in the Plumbing instance or shadows a '
                           f'built-in name, please specify a different/unique name.')

        if tubingname is not None:
            if tubingname not in self.tubingprofiles:
                raise KeyError(
                    f'The specified tubing profile "{tubingname}" was not found in the tubing profiles dictionary, '
                    'please check the name or create a profile in profiles.tubing and try again.'
                    )
            innerdiameter = self.tubingprofiles[tubingname]

        self.sections[name] = Cylinder(
            inner_diameter=innerdiameter,
            length=length,
            maximum_volume=volume,
        )

    def travel_time(self, flowrate, *segments):
        """
        Estimates the travel time of a section or sections of tubing based on the provided flowrate.
        Multiple tubing segments may be specified for convenience. The sum of the travel times will be returned
        in this case.

        :param float flowrate: flowrate in mL/min
        :param str segments: name of the section(s)
        :return: time in seconds
        :rtype: UnitFloat
        """
        return UnitFloat(self.volume(*segments) / flowrate * 60, 's')

    def volume(self, *segments):
        """
        Calculates the volume of the specified segment(s).

        :param segments: name of the section(s)
        :return: volume in mL
        :rtype: UnitFloat
        """
        if len(segments) == 0:  # if no segments specified, return total volume
            return sum([self.sections[seg].maximum_volume for seg in self.sections])  # total contained volume in mL
        if any([section not in self.sections for section in segments]):
            raise KeyError(f'The specified section(s) {", ".join(x for x in set(segments) - self.sections.keys())} '
                           f'are not defined in the Plumbing instance.')
        return sum([self.sections[section].maximum_volume for section in segments])

    def length(self, *segments):
        """
        Calculates the length of the specified segment(s).

        :param segments: name of the section(s)
        :return: length in mm
        :rtype: UnitFloat
        """
        if len(segments) == 0:  # if no segments specified, return total volume
            return sum([self.sections[seg].length for seg in self.sections])  # total contained volume in mL
        if any([section not in self.sections for section in segments]):
            raise KeyError(f'The specified section(s) {", ".join(x for x in set(segments) - self.sections.keys())} '
                           f'are not defined in the Plumbing instance.')
        return sum([self.sections[section].length for section in segments])


class SerialComponent(object):
    def __init__(self,
                 comport,
                 commandset,
                 protocol=232,
                 address=None,
                 waittime=0.1,
                 ):
        """
        A component that is installed and controlled by comport communication

        :param comport: the robot's comport to which this component is connected
        :param commandset: specify a profile from profiles.com_components.py or provide a similarly formatted
        :param protocol: the protocol this serial component uses (232 or 485)
        :param waittime: generic wait time after executing a command
        """
        # TODO make toggle for 232 vs 485 (485 will require specification of address
        if type(comport) != int:
            raise TypeError('The comport value must be an integer. Provided: "%s", type: %s' %
                            (str(comport), type(comport)))
        self.comport = comport
        if protocol == 485 and address is None:
            raise ValueError('An address must be specified for a RS485 component.')
        self.protocol = protocol
        self.address = address

        if type(commandset) == str:  # if a commandset name was provided
            self.commandset = getattr(  # update from preset profile
                importlib.import_module('ada_core.profiles.com_components'),
                '%s' % commandset,
            )
        elif type(commandset) == dict:  # if a full commandset was provided
            self.commandset = commandset

        if waittime is not None:
            try:
                self.waittime = float(waittime)
            except ValueError:
                raise ValueError('The waittime keyword must be a number. Provided: %s' % str(waittime))
        else:  # if no value provided, set wait time to 0
            self.waittime = 0.

    def __repr__(self):
        return f'{self.__class__.__name__}({self.comport})'

    def __str__(self):
        return f'{self.__class__.__name__} on comport {self.comport}'

    def pkg(self, string):
        """
        Packages a string with a prefix and affix if these values are specified in the commandset dictionary.
        'global prefix' and 'global affix' keys respectively. This function will return the original string if
        there are no global prefixes and affixes in the commandset dictionary.

        :param string: string to package
        :return: packaged string
        """
        out = ''
        if self.address is not None:  # add address if specified
            out += self.address
        if 'global prefix' in self.commandset and self.commandset['global prefix'] is not None:
            out += str(self.commandset['global prefix'])  # add prefix
        out += string  # add string
        if 'global affix' in self.commandset and self.commandset['global affix'] is not None:
            out += str(self.commandset['global affix'])  # add affix
        return out

    def send(self, command, append=None, waittime=None, globalpackage=True):
        """
        sends the specified command to the comport

        :param command: command defined in the commandset['commands'] dictionary
        :param append: if specified, the value will be appended after the command
        :param waittime: if specified, overrides the wait time variable of the instance (the script will wait this amount of time before continuing)
        :param globalpackage: if there are global prefixes and affixes to apply, specifies whether to apply them
        :return: output from run command
        """
        if 'commands' not in self.commandset:
            raise KeyError("There is no set of commands in the instance's commandset. \n"
                           'Define a dictionary with key "commands" in the commandset and retry.')
        if command not in self.commandset['commands']:
            raise KeyError('The command "%s" is not defined in the command set. \nValid commands: %s' %
                           (command, ', '.join(self.commandset['commands'].keys())))
        send = self.commandset['commands'][command]

        if append is not None:  # add the append value as string
            send += str(append)

        if globalpackage is True:  # if global packaging is specified, do so
            send = self.pkg(send)

        return controller.serial_command(  # execute command
            self.comport,  # instance's comport
            send,  # command to send
            waittime if waittime is not None else self.waittime,  # specified wait time
        )

    def read(self):
        """Reads from the serial port"""
        return controller.execute('readcom', self.comport)


    def send_package(self, packagecommand, package, waittime=None, globalpackage=True):
        """
        Sends the provided package within the package command

        :param packagecommand: prefix and affix to enclose the package in
        :param package: the variable to be packaged
        :param waittime: if specified, overrides the wait time variable of the instance (the script will wait this amount of time before continuing)
        :param globalpackage: if there are global prefixes and affixes to apply, specifies whether to apply them
        :return: output from run command
        """
        if 'packaged commands' not in self.commandset:
            raise KeyError('There is no set of package commands in the instances commandset. \n'
                           'Define a dictionary with key "packaged commands" in the commandset and retry.')
        if packagecommand not in self.commandset['packaged commands']:
            raise KeyError('The command "%s" is not defined in the command set. \nValid commands: %s' %
                           (packagecommand, ', '.join(self.commandset['packaged commands'].keys())))

        send = ''
        curr = self.commandset['packaged commands'][packagecommand]

        if 'prefix' in curr and curr['prefix'] is not None:  # if there is a prefix
            send += curr['prefix']
        send += str(package)  # add package
        if 'affix' in curr and curr['affix'] is not None:  # if there is an affix
            send += curr['affix']

        if globalpackage is True:  # globally package
            send = self.pkg(send)

        return controller.serial_command(  # execute command
            self.comport,  # instance's comport
            send,  # command to send
            waittime if waittime is not None else self.waittime,  # specified wait time
        )

    def send_raw_command(self, command, waittime=None):
        """
        Sends the specified command with no modification on the comport

        :param command: command to send
        :param waittime: if specified, overrides the wait time variable of the instance (the script will wait this amount of time before continuing)
        :return: output from run command
        """
        return controller.serial_command(  # execute command
            self.comport,  # instance's comport
            str(command),  # string version of the command
            waittime if waittime is not None else self.waittime,  # specified wait time
        )


class OutputComponent(object):
    def __init__(self, output, positions=None, delay=0.2, **kwargs):
        """
        A general object for an object connected to a 24V relay output on the robot. Examples include valves for
        syringe pump flow control, HPLC pulse signals, etc.
        Effectively this is a convenience wrapper for the Communicator.output method.

        :param output: output number associated with the valve
        :param positions: dictionary associating positions with convenience keys
            This may also be a key name for a predefined dictionary in profiles.outputs

        :param delay: delay time after executing a command (default 0.2 s)
        """
        self.output = int(output)
        self._state = 0
        self.delay = delay
        self.on_component = None  # container variable for intalled items on the module (e.g. needle on tip)
        if positions is not None:
            if type(positions) == str:
                self.positions = getattr(
                    outputs,
                    f'{positions}',
                )
            elif type(positions) == dict:
                self.positions = positions
            else:
                raise TypeError('The positions argument must be a dictionary relating a convenience key to a state')

    def __repr__(self):
        return f'{self.__class__.__name__}({self.output}, {self.state})'

    def __str__(self):
        return f"Output Valve connected to output {self.output}, state {self.state}"

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, newstate):
        """Enables direct modification of the state attribute"""
        self.set_state(newstate)

    def set_state(self, state, delay=None):
        """
        Sets the state of the output

        :param state: state to set
        :param delay: delay to wait after setting state (optional override for default delay)
        :return: state
        """
        if state not in [1, 0]:
            if 'positions' in self.__dict__:
                try:
                    state = self.positions[state]
                except KeyError:
                    raise KeyError(f'The state "{state}" is not defined in the positions dictionary of this instance.'
                                   f'Choose from {self.positions.keys()}')
            else:
                raise ValueError(f'Only a 1, 0, or a key defined in the positions dictionary may be specified for '
                                 f'the output state (provided: {state})')
        if delay is None:
            delay = self.delay
        controller.output(
            self.output,
            state,
            delay,
        )
        self._state = state
        return state

    def toggle(self, delay=None):
        """
        Toggles the output with the specified delay

        :param delay: delay to wait after setting state (optional override for default delay)
        :return: state
        """
        state = 1 - self._state  # toggle state
        self.set_state(
            state,
            delay
        )
        return state

    def pulse(self, delay=None):
        """
        Pulses the output

        :param delay: delay to wayt between switches (s)
        :return: state
        """
        controller.output(  # toggle one way
            self.output,
            1 - self._state,
            sleep=delay,
        )
        controller.output(  # toggle back
            self.output,
            self._state,
            sleep=delay,
        )
        return self._state


class Relay(OutputComponent):
    def __init__(self, output, delay=0.2, **kwargs):
        """
        Convenience class for operating a relay connected to an output.

        :param output: output number associated with the valve
        :param delay: delay time after executing a command (default 0.2 s)
        """
        OutputComponent.__init__(
            self,
            output=output,
            delay=delay,
            positions='gripper',
        )

    def on(self, delay=None):
        """
        Turns on the relay.

        :param delay: delay after triggering
        """
        self.set_state('on', delay)

    def off(self, delay=None):
        """
        Turns off the relay.

        :param delay: delay after triggering
        """
        self.set_state('off', delay)

