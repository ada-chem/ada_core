"""
Class definitions for liquid-handling pumps.
"""
import warnings
import time
from ..dependencies.general import intround
from ..dependencies.chemistry import UnitFloat
from ..dependencies.interface import controller
from ..components.joints import RevoluteJoint, PrismaticJoint
from ..components.basic import SerialComponent
from ..components.valves import OutputValve
from ..items.basic import Syringe
from ..dependencies.motion import trajectory_duration


class Pump(object):
    DIRECTIONS = ['pull', 'push']

    def __init__(self,
                 flowrate=2.,
                 mode='push',
                 max_flowrate=None,
                 default_wait=0.,
                 backdraw_volume=0.02,
                 ):
        """
        Basic Pump class with common parameters for pumps. This class has no control abilities and is intended to be
        a parent class of functional pump classes. Several of the methods of this class have comments which should
        serve as skeletons for building the appropriate methods.

        :param flowrate: flowrate for the pump
        :param mode: pump mode (push or pull)
        :param max_flowrate: maximum flow rate for the pump
        :param default_wait: default wait time for the pump to use after executing a movement
        :param backdraw_volume: default volume to backdraw after a pump dispensing (if backdraw is True in a start
            method call).
        """
        self._mode = None
        self._direction = None
        self._flowrate = None
        self.running = False
        self.backdrawn = False
        self.change_direction(mode)  # interpreted provided mode and set initial direction
        self._max_flowrate = max_flowrate
        self.flowrate = flowrate
        self.default_wait = default_wait
        self._temporary_wait = 0.
        self._movement_duration = 0.  # duration for a pump's current movement
        self._movement_start = 0.  # current movement start time

        self.active = {  # dictionary of active times of the pump
            'dispense': [],
            'withdraw': [],
        }
        self.backdraw_volume = backdraw_volume

    def __repr__(self):
        return f'{self.__class__.__name__}()'

    def __str__(self):
        return f'{self.__class__.__name__} at {self.flowrate}'

    @property
    def mode(self):
        return self._mode

    @mode.setter
    def mode(self, value):
        if self.running is True:  # if running, stop pump
            self._timepoint('stop')
        if value.lower() not in self.DIRECTIONS:
            raise ValueError(f'The mode value must be one of {", ".join(self.DIRECTIONS)}')
        self._direction = self.DIRECTIONS.index(value.lower())  # set direction value
        self._mode = value.lower()

        if self.running is True:  # if running, restart pump
            self._timepoint('start')

    @property
    def direction(self):
        return self._direction

    @direction.setter
    def direction(self, direction):
        if self.running is True:  # if running, stop pump
            self._timepoint('stop')
        if direction not in [0, 1]:
            raise ValueError('The direction value must be 1 or 0')
        self._direction = direction
        self._mode = self.DIRECTIONS[direction]
        if self.running is True:  # if running, restart pump
            self._timepoint('start')

    @property
    def finish_time(self):
        """Time since the epoch that the pump's current movement will complete at"""
        return (
            self._movement_start  # movement start time (since epoch, seconds)
            + self._movement_duration  # duration of the movement (s)
            + self.temporary_wait  # temporary wait time for the movement
        )

    @property
    def finishtime(self):
        """Legacy retrieval"""
        warnings.warn('Please access the finish_time pump attribute instead of finishtime. ')
        return self.finish_time

    @property
    def temporary_wait(self):
        """one-time use wait addition for the current or next movement"""
        out = self._temporary_wait  # retrieve current value
        self._temporary_wait = 0.  # reset to 0.
        return out  # return value

    @temporary_wait.setter
    def temporary_wait(self, value):
        self._temporary_wait = value

    @property
    def movement_duration(self):
        """Duration for the current movement (seconds)"""
        return self._movement_duration

    @property
    def movement_start(self):
        """Start time since the epoch for the current movement"""
        return self._movement_start

    @property
    def flowrate(self):
        return self._flowrate

    @flowrate.setter
    def flowrate(self, newrate):
        if self._check_flowrate(newrate) is False:
            raise ValueError(f'The specified flow rate ({newrate} mL/min) exceeds the '
                             f'maximum supported by the pump ({self._max_flowrate} mL/min)')
        self._flowrate = UnitFloat(
            newrate,
            'L/min',
            'm',
            'm',
        )
        if self.running is True:  # if pump was running
            self._timepoint('stop')  # stop the previous time course
            self._timepoint('start')  # start it again with the new flowrate

    @property
    def flowrate(self):
        return self._flowrate

    @flowrate.setter
    def flowrate(self, newrate):
        if self._check_flowrate(newrate) is False:
            raise ValueError(f'The specified flow rate ({newrate} mL/min) exceeds the '
                             f'maximum supported by the pump ({self._max_flowrate} mL/min)')
        self._encoder_v = self._flowrate_to_velocity(newrate)  # calculate and update velocity
        self._flowrate = UnitFloat(
            newrate,
            'L/min',
            'm',
            'm',
        )
        if self.running is True:  # if pump was running
            self._timepoint('stop')  # stop the previous time course
            self._timepoint('start')  # start it again with the new flowrate

    def _check_flowrate(self, flowrate):
        """checks whether the specified flowrate is below the maximum"""
        if self._max_flowrate is not None:
            return flowrate <= self._max_flowrate
        return True

    def _timepoint(self, key):
        """
        Stores a timepoint with the specified key in order to track volumes
        :param key: key to associate the timpoint with ('start', 'stop')
        """
        if self.mode == 'push':
            a = self.active['dispense']
        else:
            a = self.active['withdraw']
        # if there is no point, or the last _timepoint has already been stopped
        if len(a) == 0 or a[-1]['stop'] is not None:
            a.append({  # create new _timepoint set
                'start': None,
                'stop': None,
                'rate': self.flowrate,
            })
        a[-1][key] = time.time()

    def change_flowrate(self, newrate):
        """
        Changes the flow rate to the specified value
        :param newrate: new flow rate (mL/min)
        """
        warnings.warn(
            'The change_flowrate method has been depreciated, modify the flowrate attribute directly.',
            DeprecationWarning
        )
        self.flowrate = newrate

    def change_direction(self, direction=None):
        """
        Changes the direction of flow

        :param direction: 1, 0, 'pull', 'push', or None (if None, the direction will be toggled)
        """
        if direction in [0, 1]:  # direction flag
            self.direction = direction
        elif direction in self.DIRECTIONS:  # direction name
            self.mode = direction
        elif direction is None:  # invert direction if not specified
            self.mode = 1 - self.direction
        else:
            raise ValueError(f'The direction {direction} is not recognized. Choose 1, 0, or one of '
                             f'{", ".join(self.DIRECTIONS)}')

    def backdraw(self, volume=None):
        """
        Backdraws the pump (usually called after dispensing)

        :param volume: optional volume to use instead of the pump default
        """
        self.wait_for()  # wait for the pump to finish moving
        if self.backdrawn is True:
            raise ValueError(f'The pump {str(self)} is already backdrawn.')
        if volume is None:
            volume = self.backdraw_volume
        else:
            self.backdraw_volume = volume
        self.start(
            volume,
            'pull',
            wait=True,
        )
        self.backdrawn = True

    def start(self,
              volume,
              direction=None,
              wait=False,
              flowrate=None,
              backdraw=False
              ):
        """
        Starts the pump with the specified volume.

        :param float volume: the volume to move (if None, the pump will continuously run)
        :param direction: (optional) if not specified, the pump will use the last direction
        :param bool or float wait: (optional) if not False, the function will wait for the pump to finish
        A float may be also be specified here, which will interpreted as additional wait time in seconds to be added
        to the estimated finish time of the movement.

        :param float flowrate: optional flowrate change prior to start
        :param backdraw: whether to backdraw after dispensing (withdraws most hanging droplets after dispensing)
        :return: if wait is False, returns the estimated completion time of the movement
        """
        # start methods for the pump
        if backdraw is True and wait is False:
            raise ValueError('Backdraw requires wait to be True. ')
        if self.running is True:  # if the running tracker, wait for pump to finish
            self.wait_for()
        if flowrate is not None:
            self.flowrate = flowrate
        if direction is not None:
            self.change_direction(direction)
        if self.backdrawn is True and self.mode == 'push':
            volume = volume + self.backdraw_volume
            self.backdrawn = False
        self._timepoint('start')  # create a start timepoint
        self.running = True  # set running flag

        self._movement_start = time.time()  # movement start time

        # execute movement and set duration, add default wait period
        self._movement_duration = self._execute_movement(volume) + self.default_wait

        if wait is not False:
            if type(wait) == float or type(wait) == int:  # if an additional wait time was specified, save
                self.temporary_wait = wait
            self.wait_for()  # wait for the pump to finish moving
            if backdraw is True:  # if backdraw was specified, do so
                self.backdraw()
        # todo refactor this return to be the actual finish time
        return self.movement_duration

    def _execute_movement(self, volume):
        """
        Performs the movement execution commands for the pump in question. Subclasses should populate this method
         with pump-specific methods. This method should be called from within the start method, and should return
         the duration of the requisite movemnt.

        :param volume: volume to move (mL)
        :return: duration of movement
        :rtype: float
        """
        return 0.

    def wait_for(self, verb=False):
        """
        Waits for the pump to finish. Useful if the pump was started with wait=False and
        the active scripts requires it to have finished before continuing.
        """
        if controller.verbose is True or verb is True:
            string = f'The {str(self)} pump is already running, wait for the previous movement to finish'
        else:
            string = None
        controller.zzz(
            self.finish_time - time.time(),
            string,
        )
        self._timepoint('stop')
        self.running = False

    def volume_moved(self, key=None):
        """
        Calculates the total volume moved by the pump
        :param key: 'dispense', 'withdraw', or None. These will return the total volume dispensed, the total volume
        withdrawn, or the net movement respectively.
        :return: volume (mL)
        """
        vol = 0.
        a = self.active
        if key is None:
            key = a.keys()
        else:
            key = [key]
        for mode in key:  # for each mode specified
            if mode == 'withdraw':  # set scalar
                scalar = -1.
            elif mode == 'dispense':
                scalar = 1.
            for slot in a[mode]:
                if slot['stop'] is None:  # if pump is running, use current time
                    vol += scalar * (time.time() - slot['start']) * slot['flowrate'] * 60.
                vol += scalar * (slot['stop'] - slot['start']) * slot['flowrate'] * 60.  # otherwise calculate volume
        return vol


class ContinuousPump(Pump, RevoluteJoint):
    def __init__(self,
                 axis,
                 flowrate=2.,
                 mode='push',
                 max_flowrate=None,
                 cpul=None,
                 ulprev=None,
                 default_wait=0.,
                 backdraw_volume=0.02,
                 **kwargs
                 ):
        """
        Describes a continuous flow pump (e.g. M6 pump or peristaltic pump)

        :param axis: controller axis controlling the pump
        :param mode: push or pull
        :param flowrate: flow rate (mL/min)
        :param max_flowrate: maximum flow rate (mL/min)
        :param cpul: counts per uL
        :param ulprev: uL per revolution
        :param default_wait: default wait time for the pump to use
        :param kwargs: keyword arguments for a RevoluteJoint instance
        """
        self.axis = axis

        if cpul is not None:  # if cpul was specified
            self.cpul = cpul
        elif ulprev is not None:  # if ulprev was specified, calculate cpul
            self.cpul = self.cpr / ulprev
        else:
            raise KeyError('Either cpul or ulprev must be specified for a ContinuousPump instnace')

        RevoluteJoint.__init__(self, **kwargs)
        Pump.__init__(
            self,
            flowrate=flowrate,
            mode=mode,
            max_flowrate=max_flowrate,
            default_wait=default_wait,
            backdraw_volume=backdraw_volume,
        )

    def __repr__(self):
        return f'{self.__class__.__name__}({self.axis})'

    def __str__(self):
        return f'{self.__class__.__name__} at {self.flowrate} in {self.mode} mode'

    @property
    def flowrate(self):
        return self._flowrate

    @flowrate.setter
    def flowrate(self, newrate):
        if self._check_flowrate(newrate) is False:
            raise ValueError(f'The specified flow rate ({newrate} mL/min) exceeds the '
                             f'maximum supported by the pump ({self._max_flowrate} mL/min)')
        self._encoder_v = self._flowrate_to_velocity(newrate)  # calculate and update velocity
        self._flowrate = UnitFloat(
            newrate,
            'L/min',
            'm',
            'm',
        )
        if self.running is True:  # if pump was running
            self._timepoint('stop')  # stop the previous time course
            self._timepoint('start')  # start it again with the new flowrate

    def _flowrate_to_velocity(self, flowrate):
        """
        Calculates the velocity required for the pump to perform at the specified flowrate

        :param flowrate: flow rate (mL/min)
        :return: velocity (Hz)
        """
        v = intround(
            self.cpul  # counts per microliter
            * 1000.  # 1000 uL/mL,
            * flowrate  # flowrate (mL/min)
            / 60.  # 60 s/min
        )
        if self._check_velocity(v) is False:
            raise ValueError(f'The specified flowrate of {flowrate} mL/min will result in a velocity ({v}) that '
                             f'exceeds the maximum supported by the pump ({self.max_v}).')
        return v

    def _execute_movement(self, volume):
        """
        Executes a movement for the ContinuousPump class

        :param volume: volume to move
        :return: duration of the movement
        """
        counts = (
            volume
            * 1000  # convert to microliters
            * self.cpul  # convert to counts
        )

        if self.direction == 1 - self._dirflag:  # negative counts if pump is in push mode
            counts *= -1

        mopy = self.change_position_relative(  # determine mopy
            counts
        )
        controller.execute(
            'move',
            self.axis,
            *mopy,
        )
        controller.start_axes(self.axis)
        return trajectory_duration(mopy)

    def wait_for(self, verb=False):
        """
        Waits for the pump to finish. Useful if the pump was started with wait=False and
        the active scripts requires it to have finished before continuing.
        """
        if controller.verbose is True or verb is True:
            string = f'The {str(self)} pump is already running, wait for the previous movement to finish'
        else:
            string = None
        controller.zzz(
            self.finish_time - time.time(),
            string,
        )
        controller.wait_on_axes(self.axis)  # check axes are no longer moving
        self._timepoint('stop')
        self.running = False


class SyringePump(Pump, PrismaticJoint, Syringe):
    def __init__(self,
                 axis,
                 flowrate=2.,
                 mode='push',
                 max_flowrate=None,
                 default_wait=0.,
                 backdraw_volume=0.02,
                 **kwargs):
        """
        A class defining a liquid handling pump controlled by a North Robotics controller axis (without a valve)

        :param axis: axis that the pump is operating on
        :param flowrate: flow rate for the pump (mL/min)
        :param mode: default mode for the pump ('push' or 'pull'; default 'push')
        :param max_flowrate: maximum flow rate (mL/min)
        :param default_wait: default wait time for the pump to use

        :param kwargs: Initialization keywords for PrismaticJoint and Syringe instances
        """
        self.axis = axis
        Syringe.__init__(self, **kwargs)
        PrismaticJoint.__init__(self, **kwargs)
        Pump.__init__(
            self,
            flowrate=flowrate,
            mode=mode,
            max_flowrate=max_flowrate,
            default_wait=default_wait,
            backdraw_volume=backdraw_volume,
        )

    def __repr__(self):
        return f'{self.__class__.__name__}(ax {self.axis}, syr {self.maximum_volume})'

    def __str__(self):
        return f'{self.__class__.__name__} at {self.flowrate} in {self.mode} mode'

    @property
    def flowrate(self):
        return self._flowrate

    @flowrate.setter
    def flowrate(self, value):
        if self._check_flowrate(value) is False:
            raise ValueError(f'The specified flow rate ({value} mL/min) exceeds the '
                             f'maximum supported by the pump ({self._max_flowrate} mL/min)')
        self._flowrate = UnitFloat(
            value,
            'L/min',
            'm',
            'm',
        )
        self.encoder_v = self._flowrate_to_velocity(value)  # update velocity
        if self.running is True:  # if pump was running
            self._timepoint('stop')  # stop the previous time course
            self._timepoint('start')  # start it again with the new flowrate

    def home(self):
        """Performs a homing operation on the Syringe pump and sets the trackers to their homed state"""
        controller.execute(
            'move',
            self.axis,
            1-self._dirflag,
            2000,
            0,
            int(self.axis_range[1] * 1.5),  # move 1.5 times the total stroke
        )
        controller.start_axes(self.axis)
        self.current_volume = 0.
        self.position = int(self._initial)
        controller.wait_on_axes(self.axis)

    def _flowrate_to_velocity(self, flowrate):
        """
        Calculates the velocity required for the pump to perform at the specified flowrate

        :param flowrate: flow rate (mL/min)
        :return: velocity (Hz)
        """
        v = intround(
            self.travelrate_from_flowrate(flowrate)  # change to mm/min
            * self.cpmm  # counts per mm
            / 60.  # 60 s/min
        )
        if self._check_velocity(v) is False:
            raise ValueError(f'The specified flowrate of {flowrate} mL/min will result in a velocity ({v}) that '
                             f'exceeds the maximum supported by the pump ({self.max_v}).')
        return v

    def empty(self, wait=True, flowrate=None, backdraw=False):
        """
        If a syringe is installed in the pump, the pump will empty the volume contained in the syringe.

        :param bool wait: (optional) if True, the function will wait for the pump to finish
        :param float flowrate: optional flowrate change prior to start
        :param backdraw: whether to backdraw after dispensing (withdraws most hanging droplets after dispensing)
        :return: if wait is False, returns the estimated completion time of the movement
        """
        return self.start(
            self.current_volume,
            'push',
            wait=wait,
            flowrate=flowrate,
            backdraw=backdraw,
        )

    def fill(self, wait=True, flowrate=None):
        """
        Fills the syringe to its maximum capacity.

        :param bool wait: (optional) if True, the function will wait for the pump to finish
        :param float flowrate: optional flowrate change prior to start
        :return: if wait is False, returns the estimated completion time of the movement
        """
        return self.start(
            self.maximum_volume - self.current_volume,
            'pull',
            wait=wait,
            flowrate=flowrate,
        )

    def move_absolute(self, volume, wait=True, flowrate=None, backdraw=False):
        """
        Moves the pump to a specified volume between 0 and maximum.

        :param float or UnitFloat volume: volume the pump should be moved to
        :param bool wait: (optional) if True, the function will wait for the pump to finish
        :param float flowrate: optional flowrate change prior to start
        :param backdraw: whether to backdraw after dispensing (withdraws most hanging droplets after dispensing)
        :return: if wait is False, returns the estimated completion time of the movement
        """
        if volume == self.current_volume:
            return 0.
        elif volume > self.current_volume:
            return self.start(
                volume - self.current_volume,
                'pull',
                wait=wait,
                flowrate=flowrate,
                backdraw=backdraw,
            )
        elif volume < self.current_volume:
            return self.start(
                self.current_volume - volume,
                'push',
                wait=wait,
                flowrate=flowrate,
                backdraw=backdraw,
            )

    def move_relative(self, volume, wait=True, flowrate=None, backdraw=False):
        """
        Moves the pump by a specified volume up or down, whereby positive volumes represent pull and negative volumes
        represent push actions.

        :param float or UnitFloat volume: Volume the pump should be moved by
        :param bool wait: (optional) if True, the function will wait for the pump to finish
        :param float flowrate: optional flowrate change prior to start
        :param backdraw: whether to backdraw after dispensing (withdraws most hanging droplets after dispensing)
        :return: if wait is False, returns the estimated completion time of the movement
        """
        if volume == 0:
            return 0.
        elif volume > 0:
            return self.start(
                volume,
                'pull',
                wait=wait,
                flowrate=flowrate,
                backdraw=backdraw,
            )
        elif volume < 0:
            return self.start(
                - volume,
                'push',
                wait=wait,
                flowrate=flowrate,
                backdraw=backdraw,
            )

    def _execute_movement(self, volume):
        """
        Executes the pump movement for a Syringe Pump.

        :param volume: volume to move (mL)
        :return: duration of pump movement (s)
        """
        if self.direction == self._dirflag:  # if direction is positive movement (filling)
            self.current_volume += volume
        elif self.direction == 1 - self._dirflag:  # if direction is negative movement (emptying)
            self.current_volume -= volume

        # determine mopy from travel
        mopy = self.goto_position(  # move actuator position
            self.travel_from_volume(  # determine new position from volume
                self.current_volume
            )
        )
        controller.execute(
            'move',
            self.axis,
            *mopy,
        )
        controller.start_axes(self.axis)
        return trajectory_duration(mopy)

    def wait_for(self, verb=False):
        """
        Waits for the pump to finish. Useful if the pump was started with wait=False and
        the active scripts requires it to have finished before continuing.
        """

        if controller.verbose is True or verb is True:
            string = f'Waiting for the {str(self)} to finish moving'
        else:
            string = None
        controller.zzz(
            self.finish_time - time.time(),
            string,
        )
        controller.wait_on_axes(self.axis)  # check axes are no longer moving
        self._timepoint('stop')
        self.running = False


class ValveSyringePump(SyringePump, OutputValve):
    def __init__(self,
                 output,
                 axis,
                 flowrate=2.,
                 mode='push',
                 max_flowrate=None,
                 default_wait=0.,
                 backdraw_volume=0.02,
                 positions='valve',
                 **kwargs):
        """
        Describes a syringe pump that has a I/O valve selector controlled by an output.
        This class provides additional convenience methods for controlling the valve state prior to syringe movement.

        :param output: output number for valve
        :param positions: default positions to use for OutputComponent instance
        :param kwargs: keyword arguments for a SyringePump instance
        """
        OutputValve.__init__(
            self,
            output,
            positions=positions,
            **kwargs
        )
        SyringePump.__init__(
            self,
            axis=axis,
            flowrate=flowrate,
            mode=mode,
            max_flowrate=max_flowrate,
            default_wait=default_wait,
            backdraw_volume=backdraw_volume,
            **kwargs
        )

    def __repr__(self):
        return f'{self.__class__.__name__}(ax {self.axis}, op {self.output}, syr {self.maximum_volume})'

    def __str__(self):
        return f'{self.__class__.__name__} at {self.flowrate} in {self.mode} mode'

    def home(self):
        """Performs a homing operation on the Syringe pump and sets the trackers to their homed state"""
        self.set_state(0)
        controller.execute(
            'move',
            self.axis,
            1-self._dirflag,
            2000,
            0,
            int(self.axis_range[1] * 1.5),  # move 1.5 times the total stroke,
        )
        controller.start_axes(self.axis)
        self.current_volume = 0.
        self.position = int(self._initial)
        controller.wait_on_axes(self.axis)

    def draw(self, volume, valveinput=0, wait=True):
        """
        A specific method for a syringe pump with a connected valve where the syringe draws through the input from a
        reservoir
        This method is deprecated and should be replaced by .switch_and_move_absolute() or .switch_and_move_relative()

        :param volume: volume to draw
        :param valveinput: input setting for the installed valve instance (the direction the valve should be in to draw
            from the reservoir (see OutputComponent)
        :param wait: whether to wait for the pump (may also be float for extra wait time)
        """
        warnings.warn(
            'Method has been depreciated, access the .switch_and_move_relative() method instead',
            DeprecationWarning
        )
        return self.switch_and_move_relative(
            volume=volume,  # draw equals positive move
            valvestate=valveinput,
            wait=wait,
        )

    def dispense(self, volume, valveoutput=1, backdraw=False, wait=True):
        """
        A specific method for a syringe pump with a connected valve where the syringe pushes through the output of an
        installed valve.
        This method is deprecated and should be replaced by .switch_and_move_absolute() or .switch_and_move_relative()

        :param volume: volume to dispense
        :param valveoutput: output setting for the installed valve instance
        :param backdraw: whether to backdraw the pump after dispensing (reduces droplet spraying from arm movements)
        :param wait: wether to wait for the pump
        """
        warnings.warn(
            'Method has been depreciated, access the .switch_and_move_relative() method instead',
            DeprecationWarning
        )
        return self.switch_and_move_relative(
            volume=-volume,  # dispense equals negative move
            valvestate=valveoutput,
            wait=wait,
            backdraw=backdraw
        )

    def state_and_start(self, volume, state, direction, wait=False, flowrate=None):
        """
        Sets the pump valve to the specified state and starts the pump to move the volume specified.
        This method is deprecated and should be replaced by .switch_and_move_absolute() or .switch_and_move_relative()

        :param volume: volume to move (mL)
        :param state: valve state to set
        :param direction: direction of pump movement
        :param bool or float wait: wait kwarg for start method
        :param float flowrate: optional flowrate change prior to start
        :return: start function output
        """
        warnings.warn(
            'Method has been depreciated, access the .switch_and_move_absolute() or .switch_and_move_relative() '
            'methods instead',
            DeprecationWarning
        )
        self.wait_for(True)  # wait for any previous movement before changing states
        self.set_state(state)  # change state to specified
        return self.start(  # start the pump
            volume,
            direction,
            wait=wait,
            flowrate=flowrate,
        )

    def switch_and_empty(self, valvestate=0, wait=True, flowrate=None, backdraw=False):
        """
        Switches the valve to a specific position and empties the syringe.

        :param valvestate: state for the valve to switch to
        :param bool wait: (optional) if True, the function will wait for the pump to finish
        :param float flowrate: optional flowrate change prior to start
        :param backdraw: whether to backdraw after dispensing (withdraws most hanging droplets after dispensing)
        :return: if wait is False, returns the estimated completion time of the movement
        """
        self.set_state(valvestate)
        return self.empty(wait=wait, flowrate=flowrate, backdraw=backdraw)

    def switch_and_fill(self, valvestate=0, wait=True, flowrate=None):
        """
        Switches the valve to a specific position and fills the syringe.

        :param valvestate: state for the valve to switch to
        :param bool wait: (optional) if True, the function will wait for the pump to finish
        :param float flowrate: optional flowrate change prior to start
        :return: if wait is False, returns the estimated completion time of the movement
        """
        self.set_state(valvestate)
        return self.fill(wait=wait, flowrate=flowrate)

    def switch_and_move_absolute(self, volume, valvestate=0, wait=True, flowrate=None, backdraw=False):
        """
        Switches the valve to a specific position and moves the syringe to a specified volume.

        :param float or UnitFloat volume: volume the pump should be moved to
        :param valvestate: state for the valve to switch to
        :param bool wait: (optional) if True, the function will wait for the pump to finish
        :param float flowrate: optional flowrate change prior to start
        :param backdraw: whether to backdraw after dispensing (withdraws most hanging droplets after dispensing)
        :return: if wait is False, returns the estimated completion time of the movement
        """
        self.set_state(valvestate)
        return self.move_absolute(volume=volume, wait=wait, flowrate=flowrate, backdraw=backdraw)

    def switch_and_move_relative(self, volume, valvestate=0, wait=True, flowrate=None, backdraw=False):
        """
        Switches the valve to a specific position and moves the syringe by a specified volume up or down, whereby
        positive volumes represent pull and negative volumes represent push actions.

        :param float or UnitFloat volume: volume the pump should be moved by
        :param valvestate: state for the valve to switch to
        :param bool wait: (optional) if True, the function will wait for the pump to finish
        :param float flowrate: optional flowrate change prior to start
        :param backdraw: whether to backdraw after dispensing (withdraws most hanging droplets after dispensing)
        :return: if wait is False, returns the estimated completion time of the movement
        """
        self.set_state(valvestate)
        return self.move_relative(volume=volume, wait=wait, flowrate=flowrate, backdraw=backdraw)

    def draw_and_dispense(self, volume, valveinput=0, valveoutput=1, drawflow=None, dispenseflow=None, backdraw=False):
        """
        A convenience function call to first draw, then dispense a specified volume. If the specified volume is larger
        than the currently available syringe volume, multiple strokes are executed. This method always waits for
        completion.

        :param volume: volume to move (mL)
        :param valveinput: input setting for the installed valve instance (the direction the valve should be in to draw
                           from the reservoir (see OutputComponent)
        :param valveoutput: output setting for the installed valve instance
        :param drawflow: flow rate at which liquid should be aspirated
        :param dispenseflow: flow rate at which liquid should be dispensed
        :param backdraw: whether to backdraw the pump after dispensing (reduces droplet spraying from arm movements)
        """
        if self.current_volume == self.maximum_volume:
            raise ValueError('The pump is full and can\'t perform a transfer action!')
        stroke_volume = self.maximum_volume - self.current_volume
        while True:
            if volume > stroke_volume:
                self.switch_and_move_relative(
                    volume=stroke_volume,
                    valvestate=valveinput,
                    flowrate=drawflow,
                )
                self.switch_and_move_relative(
                    volume=-stroke_volume,
                    valvestate=valveoutput,
                    flowrate=dispenseflow,
                )
                volume -= stroke_volume
            else:
                self.switch_and_move_relative(
                    volume=volume,
                    valvestate=valveinput,
                    flowrate=drawflow,
                )
                self.switch_and_move_relative(
                    volume=-volume,
                    valvestate=valveoutput,
                    flowrate=dispenseflow,
                    backdraw=backdraw
                )
                break


class OutputSyringePump(object):
    """Depreciated name for ValveSyringePump"""
    def __new__(cls, *args, **kwargs):
        warnings.warn('The OutputSyringePump class has been renamed to ValveSyringePump. Please import and instantiate '
                      'that class instead. Returning an instance of ValveSyringePump', DeprecationWarning)
        return ValveSyringePump(*args, **kwargs)

    def __init__(self, *args, **kwargs):
        warnings.warn('The OutputSyringePump class has been renamed to ValveSyringePump. Please import and instantiate '
                      'that class instead. Returning an instance of ValveSyringePump', DeprecationWarning)


class CavroPump(ValveSyringePump):
    def __init__(self,
                 comport,
                 address,
                 flowrate=2.,
                 mode='push',
                 max_flowrate=None,
                 default_wait=0.,
                 backdraw_volume=0.02,
                 homestate='zeroleft',
                 waittime=0.5,
                 **kwargs
                 ):
        """
        Defines a Cavro- or Tricontinent-style syringe pump. Any methods defined in the OutputSyringePump class may be
        called here.

        :param int comport: comport to communicate to the pump on
        :param str address: named address (e.g. '/1')
        :param mode: default mode for the pump ('push' or 'pull'; default 'push')
        :param float flowrate: initial flowrate of the pump (mL/min)
        :param homestate: home state of the pump ('zeroright' or 'zeroleft')
        :param float waittime: wait time after sending a command
        :param kwargs: keyword arguments for Syringe initialization
        """
        self.com = SerialComponent(  # initialize serial component
            comport=comport,
            commandset='tricontinent_pump',
            address=address,
            waittime=waittime,
        )
        Syringe.__init__(self, **kwargs)  # initialize Syringe instance
        self._state = 'O'  # default state with zeroright
        self.homestate = homestate

        ValveSyringePump.__init__(
            self,
            flowrate=flowrate,
            mode=mode,
            max_flowrate=max_flowrate,
            default_wait=default_wait,
            backdraw_volume=backdraw_volume,
            positions={
                'I': 'input',
                'O': 'output',
                # added 1/0 to allow OutputSyringePump method calls to function correctly
                1: 'output',
                0: 'input',
            },
            output=9999,  # required for initialization
            axis='n/a',
            cpr=100,
            zero=0,
            cpmm=100.,
            axis_range=[0, 3000],
            **kwargs,
        )
        self.flowrate = flowrate  # perform check of true flowrate setting

        # # initialize Pump instance for pump-related values
        # self.cpmm = 100.  # prerequisite for successful initialization
        # Pump.__init__(
        #     self,
        #     flowrate=flowrate,
        #     mode=mode,
        #     max_flowrate=max_flowrate,
        #     default_wait=default_wait,
        #     backdraw_volume=backdraw_volume,
        # )
        #
        # # initialize PrismaticJoint instance (includes home call)
        # PrismaticJoint.__init__(
        #     self,
        # )


    def __repr__(self):
        return f'{self.__class__.__name__}(COM{self.com.comport}, {self.com.address})'

    def __str__(self):
        return f'{self.__class__.__name__} on COM{self.com.comport}, address {self.com.address}'

    @property
    def flowrate(self):
        return self._flowrate

    @flowrate.setter
    def flowrate(self, newrate):
        self._flowrate = UnitFloat(
            newrate,
            'L/min',
            'm',
            'm',
        )
        self.encoder_v = self._flowrate_to_velocity(newrate)
        self.com.send(
            'velocity',
            self.encoder_v,
        )

    def _flowrate_to_velocity(self, flowrate):
        """
        Calculates the velocity required for the pump to perform at the specified flowrate

        :param flowrate: flow rate (mL/min)
        :return: velocity (Hz)
        """
        v = intround(
            self.travelrate_from_flowrate(flowrate)  # change to mm/min
            * self.cpmm  # counts per mm
            / 60.  # 60 s/min
            * 2  # for whatever reason, velocity needs to be multiplied by 2
        )
        if self._check_velocity(v) is False:
            raise ValueError(f'The specified flowrate of {flowrate} mL/min will result in a velocity ({v}) that '
                             f'exceeds the maximum supported by the pump ({self.max_v}).')
        return v

    def set_state(self, state, delay=None):
        """
        Sets the state of the valve on the Cavro pump.

        :param state: state to set
        :param delay: additional delay to wait after setting state
        :return: state
        """
        if state in self.positions.keys():
            state = self.positions[state]
        elif state in self.positions.values():
            pass
        else:
            raise ValueError(f'The specified state "{state}" is not defined as a valid state. '
                             f'Choose from {", ".join(str(x) for x in self.positions.keys())}, '
                             f'{", ".join(str(x) for x in self.positions.values())}')
        if state == self.state:  # if there is no change from current
            return
        self.com.send(
            state,
            waittime=delay,
        )
        self._state = state
        return state

    def home(self):
        """Performs a homing operation on the Syringe pump and sets the trackers to their homed state"""
        self.com.send(self.homestate)  # home
        time.sleep(7.)
        self.flowrate = self.flowrate  # reset flowrate
        self.position = int(self._initial)
        self.current_volume = 0.

    def _execute_movement(self, volume):
        """
        Executes a movement command for the cavro pump class.

        :param volume: volume to move (mL)
        :return: duration of the movement
        """
        # add or subtract volume as required
        if self.direction == 0:  # if direction is positive movement (filling)
            self.current_volume += volume
        elif self.direction == 1:  # if direction is negative movement (emptying)
            self.current_volume -= volume

        # determine counts to go to
        counts = self.mm_to_counts_relative(
            self.travel_from_volume(
                self.current_volume
            )
        )
        # keep track of counts and determine eta
        self._timepoint('start')  # create a start timepoint

        # send movement command
        self.com.send(
            'goto',
            counts,
        )
        return trajectory_duration(self.change_position(counts, a=1000)) * 2.

    def wait_for(self, verb=False):
        """
        Waits for the pump to finish. Useful if the pump was started with wait=False and
        the active scripts requires it to have finished before continuing.

        :param verb: print wait notification to console
        """
        if controller.verbose is True or verb is True:
            string = f'The {str(self)} pump is already running, wait for the previous movement to finish'
        else:
            string = None
        controller.zzz(
            self.finish_time - time.time(),
            string,
        )
        self._timepoint('stop')
        self.running = False
