"""
Defines components which require the N9 robot arm to function (i.e. they are manipulated by the
robot arm). These are separated from other components due to import requirements (some components are imported
during the robot arm initialization, and will break the import hook if you try to group them together).
"""
import numpy as np
import copy

import time
# import pylab as pl
from ..components.pneumatic import PneumaticHolder
from ..dependencies.exceptions import TrayFull, TrayEmpty, EmptyLocation
from ..dependencies.general import inds_to_cellname, centered_list, intround, cellname_to_inds
from ..dependencies.coordinates import Location, deck, LocationGrid, Rotation
from ..dependencies.motion import trajectory_duration
from ..components.pumps import ContinuousPump, ValveSyringePump
from ..components.joints import RevoluteJoint
from ..dependencies.interface import controller
from ..dependencies.simulator import Asset
from ..sequences.sampling import generic_condition_lines, dispense_volume
from ..dependencies.arm import robotarm
from ..items import manipulated
from ..items import basic


class ManipulatedComponent(object):
    def __init__(self, location, asset=None, asset_visible=True, *args, **kwargs):
        """
        Class defining objects mounted to the robot deck. They can have optional assets attached that will appear in
        the simulator whenever an instance is created.
        :param location: the location of the component
        :param dict asset: an optional dict used to create an a simulator asset (see AssetInfo for parameters)
        :param asset_visible: default visibility of the asset
        """
        self.location = Location(location)
        self.asset = Asset(location, asset, visible=asset_visible)


class Camera(ManipulatedComponent):
    def __init__(self,
                 location,
                 x=-18.75,
                 y=0.,
                 dockheight=130+8.4,
                 length=33.5,
                 clearheight=13.,
                 camerax=1.1,
                 cameray=2.7,
                 cam_number=0,
                 knobheight=20.,
                 **kwargs,
                 ):
        """
        Class defining a grippable USB webcam with a dock location.

        :param location: index location of the docker
        :param x: x offset from index to handle
        :param y: y offset from index to handle
        :param dockheight: height of the docker
        :param length: height of the camera
        :param clearheight: height required to clear the docking pegs
        :param camerax: x offset for camera image alignment
        :param cameray: y offset for camera image alignment
        :param cam_number: webcam number
        :param knobheight: height of the grippable knob
        """
        from ada_peripherals.vision import webcam
        self.cam_number = cam_number
        self.webcam = webcam
        self.camerax = camerax
        self.cameray = cameray
        self.length = length
        self.knobheight = knobheight
        self.dockheight = dockheight
        self.clearheight = clearheight
        self.home = {}

        ManipulatedComponent.__init__(self, location, **kwargs)

        if type(location) == str:  # if handed a Deck grid coordinate
            self.location = deck[location]  # calculate the position
        elif type(location) == dict:  # if handed a position dictionary
            self.location = location
        else:
            raise ValueError('The location of a Camera must be either a grid coordinate or location dictionary')

        self.location.offset(  # offset location
            x=x,
            y=y,
            z=dockheight + length,
        )

        # https://docs.opencv.org/3.1.0/dd/d01/group__videoio__c.html
        self._cv = {  # controlled variables for adjusting camera values
            'frame_width': 3,
            'frame_height': 4,
            'brightness': 10,
            'contrast': 11,
            'saturation': 12,
            'exposure': 15,
        }

        self._cv_defaults = {}
        # change camera values if provided
        if any([key in kwargs for key in self._cv.keys()]):
            video = self.webcam.cv2.VideoCapture(self.cam_number)
            for key in self._cv.keys() & kwargs.keys():
                self._cv_defaults[key] = video.get(self._cv[key])
                video.set(
                    self._cv[key],
                    kwargs[key],
                )
            video.release()
        self.a_avg = None
        self.b_avg = None

    def __repr__(self):
        return f'{self.__class__.__name__}()'

    def __str__(self):
        return f"Grippable {self.__class__.__name__}"

    def pick_up(self, height=None):
        """
        Pick up the camera module in the gripper

        :param height: optional safeheight
        """
        if robotarm.gripper.ingrip is not None:
            raise ValueError(f'There is already something in the gripper! ({robotarm.gripper.ingrip})')
        safeheight = self.dockheight + self.length + self.knobheight  # minimum safe height for pickup
        if height is None or height < safeheight:  # ensures safeheight parameter is sufficient
            height = safeheight
        robotarm.safeheight(height)  # safeheight
        robotarm._gripper.lock_symmetry = True  # lock the symmetry of the gripper
        robotarm.move_to_location(  # go to location
            self.location,
            orient_gripper=0.,
            reference_axis='y',
        )
        robotarm.gripper_on(self)  # turn gripper on and store self in gripper
        self.home = robotarm.axes_location()  # ensures return to exact home
        # self.topdock = dict(self.location)
        # self.topdock['y'] = self.location['y'] + 1.
        # self.topdock['z'] = height
        # robotarm.move_to_location(
        #     self.topdock,
        #     aligngripper='y',
        # )
        robotarm.safeheight(height, v=5000)  # go to clearance height

    def put_down(self, height=None):
        """
        Put the camera module back down on the dock

        :param height: optional safeheight
        """
        safeheight = self.dockheight + self.clearheight
        if height is None or height < safeheight:  # ensures safeheight parameter is sufficient
            height = safeheight
        robotarm.safeheight(height)
        # robotarm.move_to_location(
        #     self.topdock,
        #     aligngripper='y',
        # )
        robotarm.move_axes(
            self.home,
        )
        robotarm.gripper_off()
        robotarm._gripper.lock_symmetry = False
        robotarm.update_module_locations()
        robotarm.safeheight()

    def camera_to_location(self, location, height=None):
        """
        Moves the camera to the specified location

        :param location: location dictionary
        :param height: optional safeheight
        """
        robotarm.safeheight(height)
        location = copy.deepcopy(location)
        location.offset(  # offset location
            x=self.camerax,
            y=self.cameray,
            z=self.length if location.z is not None else None,
        )
        robotarm.move_to_location(
            location,
            orient_gripper='+y',
            reference_axis='y',
        )

    def read_average_rgb(self, location=None, reads=5, section=25, height=None, retrieveframes=False, whitebalance=True):
        """
        Moves the camera to the specified location and reads the RGB value

        :param location: location dictionary
        :param reads: number of reads to average (default 5)
        :param section: the number of pixels of the center section of the image to read (default 25x25 section)
        :param height: optional safeheight
        :param retrieveframes: if True, returns the captured frames
        :return: [RED, GREEN, BLUE]
        """
        frames = self.capture_image(  # retrieve images
            location=location,
            n=reads,
            height=height,
        )
        if whitebalance is True:
            if all(avg is not None for avg in [self.a_avg, self.b_avg]):
                a, b = self.webcam.get_whitebalance(frames[0])
                frames = [self.webcam.apply_whitebalance(frame, a, b) for frame in frames]
                # frames = [self.webcam.apply_whitebalance(frame, self.a_avg, self.b_avg) for frame in frames]
        snippets = [self.webcam.snippet(frame, section) for frame in frames]  # snippet the frames
        avgrgbs = [self.webcam.average_rgb(snippet) for snippet in snippets]  # average the snippets
        avgrgb = [sum([rgb[i] for rgb in avgrgbs]) / reads for i in [0, 1, 2]]
        if retrieveframes is True:
            return avgrgb, frames
        return avgrgb

    def capture_image(self, location=None, n=1, whitebalance=True, height=None):
        """
        Captures image(s) at the current location or at the specified location

        :param location: location dictionary
        :param n: number of frames to capture
        :param whitebalance: whether to white balance the captured image
        :return: frames
        """
        if location is not None:
            self.camera_to_location(
                location,
                height=height,
            )
        frames = self.webcam.capture_image(
            n,
            webcam=self.cam_number,
        )
        if n == 1:
            frames = [frames]
        if whitebalance is True:
            if self.a_avg is not None and self.b_avg is not None:
                frames = [self.webcam.apply_whitebalance(
                    frame,
                    self.a_avg,
                    self.b_avg,
                ) for frame in frames]
        if n != 1:
            return frames
        return frames[0]

    def change_setting(self, setting, value):
        """
        Changes the webcam setting

        :param setting: key defined in self._cv or an integer corresponding to a setting
        :param value: value to set the key to
        """
        video = self.webcam.cv2.VideoCapture(self.cam_number)
        if type(setting) == str:
            setting = self._cv[setting]
        if setting not in self._cv_defaults:
            self._cv_defaults[setting] = video.get(setting)
        video.set(
            setting,
            value
        )

    def get_white_balance(self, location=None, height=None):
        """
        Gets the white balance for the environment

        :param location: optional location to read from
        :param height: optional safeheight
        :return:
        """
        if location is not None:
            self.camera_to_location(
                location,
                height
            )
        self.a_avg, self.b_avg = self.webcam.get_whitebalance()


class Dispenser(basic.Item):
    def __init__(self,
                 location,
                 installed,
                 x_offset=None,
                 y_offset=None,
                 height=None,
                 thickness=None,
                 clearheight=None,
                 dist_to_tubes=None,
                 index_degrees=None,
                 outputs=None,
                 output_depth=None,
                 **kwargs,
                 ):
        """
        A dispenser component held in the gripper of the robot

        :param dict or str location: index location of the docker
        :param dict installed: dictionary of installed pumps associated with each index
        :param x_offset: x offset from index location
        :param y_offset: y offset from index location
        :param height: height of the docker (mm)
        :param thickness: thickness of the dispenser module (mm)
            The gripper will go to height+thickness mm to pick up the dispenser

        :param clearheight: how high the dispenser must go up before it is clear of the docker (mm)
        :param dist_to_tubes: length from the center of the gripper to the tubes (mm, radius of the dispenser)
        :param index_degrees: the difference in angle between the dispenser tubes (degrees)
        :param outputs: how many tubes is the dispenser capable of holding
        :param output_depth: how far the tubes protrude below the dispenser
        """
        # todo implement pick and put down calls in the dispense to location/tube to location calls
        self._thickness = thickness
        self._output_depth = output_depth
        basic.Item.__init__(
            self,
            location=location,
            length=(
                # 47.  # 47 mm offset from gripper to dispenser probe mount
                # robotarm.modules.gripper.length  # gripper teeth length
                + self._thickness  # add dispenser thickness
                # + self._output_depth  # add tube depth
                # + robotarm.modules.probe._base_offset  # plus the base offset of the dispenser from the gripper base
                # - robotarm.modules.probe.length  # minus the dispenser probe length
            ),
            **kwargs,
        )

        self._x_offset = x_offset
        self._y_offset = y_offset
        self._height = height
        self._clearheight = clearheight
        self._dist_to_tubes = dist_to_tubes
        self._index_degrees = index_degrees
        self._outputs = outputs

        # determine locations
        self.offset_location(  # offset location
            x=self._x_offset,
            y=self._y_offset,
            z=self._height,
        )
        self._safeheight = (
            self._height
            + self._clearheight
            + self._thickness
            + self._output_depth
        )  # safe clear height
        self._home = None
        # todo come up with some way to link offsets when something occupies all locations
        self._installed = {}  # dictionary of installed tubes

        self.tube_degrees = centered_list(  # list of degrees for each tube
            self._outputs,
            self._index_degrees,
        )

        # determine the gripper axes locations for the index values
        for ind, val in enumerate(self.tube_degrees):
            if ind + 1 in installed:
                self._installed[ind + 1] = {}
                self._installed[ind + 1]['axdeg'] = intround(val)
                # todo allow convenience naming of indicies
                if isinstance(installed[ind + 1], ContinuousPump):
                    self._installed[ind + 1]['pump'] = installed[ind + 1]
                elif isinstance(installed[ind + 1], ValveSyringePump):
                    self._installed[ind + 1]['pump'] = installed[ind + 1]
                elif type(installed[ind + 1]) == dict:
                    if 'pumptype' not in installed[ind + 1]:
                        raise KeyError('A "pumptype" key is required to direct initialization of the correct '
                                       '*Pump component.')
                    if installed[ind + 1]['pumptype'] in ['continuouspump', 'continuous']:
                        self._installed[ind + 1] = ContinuousPump(**installed[ind + 1])
                    elif installed[ind + 1]['pumptype'] in ['syringepump', 'syringe']:
                        self._installed[ind + 1] = ValveSyringePump(**installed[ind + 1])
                    else:
                        raise ValueError(f'The pumptype "{installed[ind + 1]["pumptype"]} is not recognized as a valid '
                                         f'Pump class signifier.')
                else:
                    raise ValueError('A pump instance or keyword arguments required for a pump instance initialization '
                                     'were not found. ')

    def __repr__(self):
        return "%s(%d tubes)" % (self.__class__.__name__, len(self._installed))

    def __str__(self):
        return "Dispenser controlling %d pumps" % (len(self._installed))

    def _install_tube(self, index, pumpinstance, name=None):
        """
        Installs a tube and pump into the dispenser

        :param index: index of the specified tube
        :param pumpinstance: Pump class instance
        :param name: name for the tube (optional)
        """
        if type(index) != int:
            try:
                index = int(index)
            except ValueError:
                raise TypeError('The provided index is not an integer %s (%s)' % (str(index), type(index)))
        if index in self._installed:
            raise KeyError('The index %d is already installed in the dispenser instance' % index)
        self._installed[index] = {
            'name': name,
            'inst': pumpinstance,
        }

    def get_location(self, loc=None):
        """
        Returns either the bottom or pickup location of the dispenser. Top will return the
        location with vertical offset.

        :param 'bottom', 'pickup' loc: the location to retrieve
        :return: xyz location
        :rtype: Location
        """
        out = Location(self._location)  # stored location is the bottom of the dispenser
        if loc is None or loc == 'bottom':
            pass
        elif loc == 'pickup':
            out.offset(z=self._thickness)
        else:
            raise KeyError(f'The loc value "{loc}" is not recognized. Choose from "pickup" or "bottom"')
        return out

    def pick_up(self, height=None):
        """
        Pick up the dispenser module in the gripper

        :param height: optional safeheight
        :return:
        """
        if height is None or height < self._safeheight:
            height = self._safeheight
        robotarm.safeheight(height)
        robotarm.move_to_location(  # go to location
            self.get_location('pickup'),
            orient_gripper='+y',
            reference_axis='y',
        )
        robotarm.lock_gripper_symmetry = True  # lock symmetry
        self._home = robotarm.axes_location()  # retrieve exact home location
        robotarm.gripper_on(self)  # turn on gripper and store self in gripper module
        # todo figure out a way to reinstate prevention (or locking) of probe use (maybe a pseudodispenser item with the right length)
        robotarm.safeheight(  # go to clearance height
            height,
            # v=5000,
            # target='probe',
        )

    def put_down(self, height=None):
        """
        Put the dispenser module down

        :param height: optional safeheight
        :return:
        """
        if height is None or height < self._safeheight:
            height = self._safeheight
        robotarm.safeheight(height)
        robotarm.move_axes(  # move
            self._home,
            splitxyz='xyz',
        )
        robotarm.gripper_off()  # turn off gripper and remove self from module
        robotarm.lock_gripper_symmetry = False
        robotarm.safeheight()

    def tube_to_location(self, location, tube, height=None):
        """
        Places the specified dispenser tube to the location

        :param location: location to put tube
        :param tube: tube index
        :param height: optional safeheight
        :return:
        """
        # todo implement convenience naming
        # if tube not in self._installed:
        #     if type(tube) == str:  # if handed a name key, determine the output
        #         for key in self._installed:
        #             if tube == self._installed[key]['name']:
        #                 tube = key
        #                 break

        # location = dict(location)
        # location['z'] += self.length  # offset by output depth
        if tube not in self._installed:
            raise KeyError(f'The tube number {tube} is not installed in this {self.__class__.__name__} instance. ')
        robotarm.safeheight(height)
        location = Location(location)  # create immutable
        location.offset(  # offset for tube height
            z=self._output_depth
        )
        robotarm.move_to_location(
            location,
            target='probe',
            orient_gripper=self._installed[tube]['axdeg'],
            reference_axis='forearm',
        )

    def dispense_to_location(self, location, tube, volume, height=None, delay=0.25, jerk=True):
        """
        Moves the dispenser tube to the location and dispenses the specified volume.

        :param location: location to put tube
        :param tube: tube index
        :param volume: volume to dispense (mL)
        :param height: optional safeheight
        :param delay: addition delay for the pump on top of the completed movement (s)
        :param jerk: whether to jerk the arm (dislodges most hanging droplets)
        """
        self.tube_to_location(
            location,
            tube,
            height
        )
        dispense_volume(  # dispense the volume specified
            self._installed[tube]['pump'],
            volume,
            delay=delay,
        )
        if jerk is True:  # jerk the robot arm if specified
            robotarm.jerk()
        self._installed[tube]['pump'].backdraw()  # backdraw the pump
        robotarm.safeheight(height)

    def dispense_set_to_location(self, location, *sets, height=None, delay=0.25, jerk=True):
        """
        Dispenses a set of volumes to the specified location in the most efficient manner possible.

        :param dict location: location to put the liquids
        :param list of lists sets: sets of [tube #, volume (mL)] to dispense
        :param float height: optional safeheight
        :param float delay: addition delay for the pump on top of the completed movement (s)
        :param bool jerk: whether to jerk the arm (dislodges most hanging droplets)
        :return: timepoints when each item was added
        :rtype: list
        """
        if controller.verbose:
            print('Dispensing set: %s' % str(sets))
        if len(sets) == 1:  # if handed only one set, simply perform a single dispense
            self.dispense_to_location(
                location,
                sets[0][0],
                sets[0][1],
                height=height,
                delay=delay,
                jerk=jerk,
            )
            return None

        # separate sets depending on whether they need to be primed
        syringe = [tubevol for tubevol in sets if isinstance(self._installed[tubevol[0]]['pump'], ValveSyringePump)]

        # todo figure out how to use this method if the volume exceeds what the syringe can hold
        if len(syringe) > 0:  # prime all SyringePump type pumps
            for tubevol in syringe:
                tube, vol = tubevol
                self._installed[tube]['pump'].draw(
                    vol,
                    wait=False,
                )
            for tubevol in syringe:  # wait for pumps to finish
                tube, vol = tubevol
                self._installed[tube]['pump'].wait_for()

        # todo change to addition order depending on time finished
        timepoints = []
        # dispense volumes
        for tubevol in sets:
            tube, vol = tubevol
            self.tube_to_location(  # go to location
                location,
                tube,
                height
            )
            timepoints.append(time.time())
            if isinstance(self._installed[tube]['pump'], ValveSyringePump):
                self._installed[tube]['pump'].dispense(
                    vol,
                    wait=True,
                )
            elif isinstance(self._installed[tube]['pump'], ContinuousPump):
                self._installed[tube]['pump'].start(
                    vol,
                    'push',
                    wait=True,
                )
            if jerk is True:
                robotarm.jerk()
            self._installed[tube]['pump'].backdraw()
            robotarm.safeheight(height)
        return timepoints

    def prime_tubes(self, wastelocation, height=None, **kwargs):
        """
        Uses the generic_condition_lines method to condition the pumps controlling the tubes.

        :param wastelocation: xyz dictionary location for emptying waste
        :param height: optional safeheight
        :param kwargs: generic_condition_lines keyword arguments
        """
        if robotarm.gripper.ingrip != self:
            if robotarm.gripper.ingrip is not None:
                raise ValueError(f'There is something else in the gripper ({robotarm.gripper.ingrip})!')
            self.pick_up()
        self.tube_to_location(
            wastelocation,
            int(self._outputs / 2) + self._outputs % 2,  # middle tube
            height=height,
        )
        generic_condition_lines(
            *[self._installed[tube]['pump'] for tube in self._installed],
            **kwargs,
        )
        robotarm.jerk()
        # todo consider putting down after priming


class TipRemover(ManipulatedComponent):
    def __init__(self,
                 location,
                 vdepth,
                 vwidth,
                 height=0.,
                 x_offset=0.,
                 y_offset=0.,
                 horizontal_smidge=0.,
                 vertical_smidge=-0.5,
                 rotate=False,
                 **kwargs):
        """
        A needle or pipette tip remover tower

        :param location: location dictionary or index

        :param height: height of the remover "V" from the deck (mm)
        :param rotate: rotation of the remover (see Location.offset() for details)
        :param vdepth: depth of the "V" (mm)
        :param vwidth: breadth of the "V" (mm)
        :param x_offset: x offset from index (mm)
        :param y_offset: y offset from index (mm)
        :param horizontal_smidge: slight horizontal offset to be applied to the calculated touch point (mm; reduces
            smashing at high velocities)
        :param vertical_smidge: slight vertical offset to be applied to the calculated touch height (mm). Negative
            values will move the probe lower and positive values will move the probe higher.
        :param asset: asset model to use in a connected simulator
        """
        ManipulatedComponent.__init__(self, location, **kwargs)
        self._vdepth = vdepth
        self._vwidth = vwidth
        self._height = height
        self._h_smidge = None
        self._v_smidge = None
        self.horizontal_smidge = horizontal_smidge
        self.vertical_smidge = vertical_smidge
        self._rotate = None
        self.rotate = rotate

        # create a location instance for the offset and rotate it as specified
        offset = Location(
            x_offset,
            y_offset
        )
        offset.rotate(rotate)

        # apply rotation offset
        self.location.offset(offset)
        # apply height
        self.location.offset(z=self._height)

    def __repr__(self):
        return f'{self.__class__.__name__}'

    @property
    def horizontal_smidge(self):
        """horizontal offset to move the probe back from the calculated touch point"""
        return self._h_smidge

    @horizontal_smidge.setter
    def horizontal_smidge(self, value):
        if value is None:
            value = 0.
        self._h_smidge = value

    @horizontal_smidge.deleter
    def horizontal_smidge(self):
        self.horizontal_smidge = 0.

    @property
    def vertical_smidge(self):
        """vertical offset to move the probe up or down from the calculated interaction point"""
        return self._v_smidge

    @vertical_smidge.setter
    def vertical_smidge(self, value):
        if value is None:
            value = 0.
        self._v_smidge = value

    @vertical_smidge.deleter
    def vertical_smidge(self):
        self.horizontal_smidge = 0.

    @property
    def rotate(self):
        return self._rotate

    @rotate.setter
    def rotate(self, rotation):
        self._rotate = Rotation(z=rotation)

    def calculate_touch(self, diameter):
        """
        Calculates the distance from the point of a triangle where a circle of diameter will touch the triangle

        :param diameter: diamter of the circle
        :return: offset
        """
        # radius of the circle
        radius = diameter / 2.

        trialpha = np.math.atan(  # angle of the triangle at the mouth of the V
            self._vdepth / (self._vwidth / 2.)
        )
        # angle between the radius and the chord
        circleangle = np.math.pi / 2. - trialpha

        # distance from the center of the circle to the chord
        circlerise = radius * np.math.sin(circleangle)

        # length of the half-chord
        halfchord = radius * np.math.cos(circleangle)

        # rise from the triangle point to the chord
        chordrise = halfchord * np.math.tan(trialpha)

        return circlerise + chordrise

    def remove_tip(self,
                   height=190.,
                   ignoreempty=False,
                   initloc=None,
                   offset=30.,
                   top_id=None,
                   bot_id=None,
                   interaction_depth=None,
                   armbias='shoulder middle'
                   ):
        """
        Removes a pipette tip or syringe needle from the tip attachment.

        :param height: override for safeheight calls
        :param ignoreempty: ignores empty ontip value and performs tip removal action regardless
        :param initloc: initial location to go to before removing
            This parameter is useful when the robot arm does a large swing to approach the remover, which can sometimes
            result in collisions.
        :param float offset: offset distance for initial approach for tip removal. This will be used if initloc is not
            provided
        :param float top_id: top inner diameter of the item being removed. This will be automatically retrieved from the
            item on the arm probe. If retrieval fails, this value will be used.
        :param float bot_id: bottom inner diameter of the item being removed. This will be automatically retrieved from
            the item on the arm probe. If retrieval fails, this value will be used.
        :param float interaction_depth: The interaction depth of the item (distance from the top to the bottom id
            measurement points). This will be automatically retrieved from the item on the arm probe. If retrieval
            fails, this value will be used.
        :param str armbias: The arm bias for the shoulder movements. 'shoulder middle' is highly recommended here to
            provide the best interaction angle and to avoid accidental contact of other parts of the arm.
        :return: the item that was previously on the tip attachment
        """
        if ignoreempty is False and robotarm.modules.probe.item is None:
            raise ValueError('Python is not aware of anything on the probe.')

        # remove item on tip
        item = removing = robotarm.modules['probe'].remove_item()  # remove item from tip
        if isinstance(item, manipulated.ProbeItem):  # check for attribute requirements
            top_id = item.top_inner_diameter
            bot_id = item.bot_inner_diameter
            interaction_depth = item.interaction_depth
        elif not all([top_id, bot_id, interaction_depth is not None]):
            raise ValueError('Removal parameters could not be extracted from the item on the probe and they were not'
                             'specified in the method call')

        # copy locations and offset top vertically
        toptouch = copy.copy(self.location)
        bottouch = copy.copy(self.location)
        toptouch.offset(z=-interaction_depth + self.vertical_smidge)

        # calculate top/bot offsets, then rotate
        topoffset = Location(
            y=(
                    - self._vdepth
                    + self.calculate_touch(top_id)
                    + self.horizontal_smidge
            )
        )
        topoffset.rotate(self._rotate)
        botoffset = Location(
            y=(
                    - self._vdepth
                    + self.calculate_touch(bot_id)
                    + self.horizontal_smidge
            )
        )
        botoffset.rotate(self.rotate)

        # adjust touch points based on orientation
        toptouch.offset(topoffset)
        bottouch.offset(botoffset)

        if initloc is not None:  # if initial location was supplied, create instance
            initloc = Location(initloc)
            initloc.z = toptouch.z  # enforce height
        else:  # otherwise retrieve toptouch position
            initloc = Location(toptouch)
            offset = Location(y=offset)  # apply default 30mm offset
            offset.rotate(self.rotate)  # rotate according to instance settings
            initloc.offset(offset)  # apply directional offset for initial location
        movement_kwargs = {
            'target': 'probe',
            'armbias': armbias,  # shoulder middle is least likely to result in a bad removal angle
        }
        robotarm.safeheight(
            height,
            target='probe'
        )
        robotarm.move_to_location(  # move to initial location
            initloc,
            splitxyz='xyz',  # move in, then down, in order to work with the sharps bin
            **movement_kwargs,
        )
        robotarm.move_to_location(  # move to top touch point (should be above item)
            toptouch,
            splitxyz='zxy',  # move z, then xy
            **movement_kwargs,
        )
        item.item_asset.hide()
        robotarm.move_to_location(  # move up to scrape off item
            bottouch,
            splitxyz=None,
            **movement_kwargs,
        )
        # move up just a titch more to make sure gripper is clear
        robotarm.safeheight(
            max(bottouch['z'] + 5., height) if height is not None else bottouch['z'] + 5.,
            target='probe'
        )

        removing.location = 'DISPOSED'
        return removing

    def remove_cap(self,
                   height=190.,
                   ignoreempty=False,
                   initloc=None,
                   offset=30.,
                   ):
        """
        Removes the protective cap from a needle.

        :param height: override for safeheight calls
        :param ignoreempty: ignores empty ontip value and performs tip removal action regardless
        :param initloc: initial location to go to before removing
            This parameter is useful when the robot arm does a large swing to approach the remover, which can sometimes
            result in collisions.
        :param offset: offset distance for initial approach for tip removal

        :return: True if a cap has been removed
        """
        try:
            if ignoreempty is False and robotarm.modules.probe.item.cap is False:
                raise ValueError('The needle is already uncapped!')
        except AttributeError:
            raise ValueError('The item on the probe tip is not a needle!')

        # remove cap from object
        robotarm.modules.probe.item.cap = False

        # work out touch location
        captouch = copy.copy(self.location)  # create deep copy of V location

        # the position is measured from the tip of the needle, so:
        captouch.offset(z=
                        - robotarm.modules.probe.item.total_length  # go up the overall needle length
                        + robotarm.modules.probe.item.height  # and down half the distance between cap and cone
                        )

        capoffset = Location(  # create an offset to account for the geometry of the V and the needle
            y=(
                    - self._vdepth
                    + self.calculate_touch(robotarm.modules.probe.item.top_outer_diameter)
                    + self._h_smidge
            )
        )
        capoffset.rotate(self._rotate)  # rotate offset so it fits with the remover orientation

        captouch.offset(capoffset)  # apply offset

        if initloc is not None:  # if initial location was supplied, create instance
            initloc = Location(initloc)
        else:  # otherwise retrieve captouch position
            initloc = Location(captouch.xy)
        offset = Location(y=offset)  # default 30mm offset
        offset.rotate(self._rotate)  # rotate according to instance settings
        initloc.offset(offset)  # apply directional offset for initial location
        initloc.z = captouch.z   # set initial location height
        movement_kwargs = {
            'target': 'probe',
            'armbias': 'shoulder middle',  # this arm bias is least likely to result in a bad removal angle
        }

        robotarm.safeheight(
            height,
            target='probe'
        )
        robotarm.move_to_location(  # move to initial location
            initloc,
            splitxyz='xyz',
            **movement_kwargs,
        )
        robotarm.move_to_location(  # move to top touch point (should be above item)
            captouch,
            **movement_kwargs,
        )
        robotarm.safeheight(  # move up to remove the cap
            max(captouch['z'] + 30., height) if height is not None else captouch['z'] + 30.,
            target='probe'
        )
        robotarm.move_to_location(  # move out horizontally to clear the teeth
            initloc.xy,
            **movement_kwargs,
        )

        return True


class Tray(ManipulatedComponent, LocationGrid):
    def __init__(self,
                 location,
                 population=None,
                 iterorder='x',
                 height=None,
                 itemclass='Item',
                 itemkwargs=None,
                 asset=None,
                 rotate=0.,
                 **locgridkwargs
                 ):
        """
        Defines a general tray (may be used for vial trays, needle trays, pipette trays, etc.).
        The general form defines an array of locations where there are item instances at each of those locations.
        The Tray object may be iterated over or used as a generator

        :param location: A location index or dictionary of the index hole of the needle tray
        :param iterorder: the directional preference to iterate around the tray (x or y axis)
        :param height: height off the deck where the items are place (mm)
        :param itemclass: the name of the class of item that is put in this tray (case sensitive)
        :param itemkwargs: keword arguments to provide to the item class
        :param population: per-item keyword arguments (associated by alphanumeric indicies, e.g. A1, H14)
        :param float rotate: the angle in degrees to rotate the tray by.
            # CW is positive degrees, CCW is negative degrees, 0. for no rotation.
        :param locgridkwargs: LocationGrid keyword arguments

        **LocationGrid keyword arguments**

        The LocationGrid class is used for determining the location of items in trays. Keyword arguments for the
        LocationGrid instance of the tray should be passed directly to the Tray. See dependencies.general.LocationGrid
        for more details.
        """

        # TODO: fix buggy tray generation
        #asset_info = asset or assets.trays.build_tray(height=height, **locgridkwargs)
        asset_info = asset

        ManipulatedComponent.__init__(self, location, asset=asset_info, **locgridkwargs)
        # todo either consolidate or catch and combine height and z_offset
        LocationGrid.__init__(
            self,
            referencelocation=self.location,
            z_offset=height,
            rotate=rotate,
            **locgridkwargs,
        )
        self._iterorder = iterorder
        self._height = height

        self.itemclass = itemclass  # store itemclass from when tray was first initialized
        self.population = population  #  store population from when tray was first initialized
        self.itemkwargs = itemkwargs

        # initialize array to contain item instances
        self.itemarray = np.full(self.shape, None)

        # if there are items to install in the tray
        if itemkwargs is None:
            itemkwargs = {
                'tray_rotation': self._rotate
            }
        else:
            itemkwargs['tray_rotation'] = self._rotate

        if itemclass is not None:
            if hasattr(basic, itemclass):
                item = getattr(basic, itemclass)
            elif hasattr(manipulated, itemclass):
                item = getattr(manipulated, itemclass)
            else:
                raise ValueError(f'The item class "{itemclass}" is not defined in the Items package. '
                                 'Please check your spelling (case sensitive)')

            # if handed a population, populate accordingly
            if population is not None:  # todo put in more descriptive error catching here
                for key in population:
                    row, col = cellname_to_inds(key)
                    self.itemarray[row][col] = item(
                        location=self._array[row][col],
                        name=key,
                        **itemkwargs,
                        **population[key],  # todo create a catch for double-specification of kwargs
                    )
            else:  # otherwise, populate all cells
                for i, row in enumerate(self.itemarray):
                    for j, col in enumerate(row):
                        self.itemarray[i][j] = item(
                            location=self._array[i][j],
                            name=inds_to_cellname(i, j),
                            **itemkwargs,
                        )

        self.generatorarray = self._yielditem()  # create a generator for item instance retrieval

    def __iter__(self):
        """the instructions to iterate over each vial"""
        if self._iterorder == 'x':
            for row in self.itemarray:  # row
                for item in row:  # item in row
                    if item is not None:  # check if slot is populated
                        yield item
        elif self._iterorder == 'y':
            for col, val in enumerate(self.itemarray[0]):  # column
                for row, val1 in enumerate(self.itemarray):  # row
                    item = self.itemarray[row][col]
                    if item is not None:
                        yield item

    def __getitem__(self, item):
        """returns the location of the specified vial

        Can be either a string (e.g. 'A1') or a tuple
        """
        if type(item) == str:
            return self.itemarray[cellname_to_inds(item)]
        elif type(item) == tuple:
            return self.itemarray[item[0]][item[1]]

    def __repr__(self):
        return f'{self.__class__.__name__}({self.location})'

    def __str__(self):
        return f'Tray with reference location {self.location}'

    def __len__(self):
        return sum([1 for item in self if item is not None])

    def get_item(self):
        """retrieves the next needle in the needle tray"""
        try:
            return next(self.generatorarray)  # try to return the next pipette tip
        except StopIteration:
            raise TrayEmpty

    def _yielditem(self):
        """creates a generator for the tray"""
        if self._iterorder == 'x':
            for row in self.itemarray:
                for item in row:
                    if item is not None:
                        yield item
        elif self._iterorder == 'y':
            for col, val in enumerate(self.itemarray[0]):
                for row, val1 in enumerate(self.itemarray):
                    item = self.itemarray[row][col]
                    if item is not None:
                        yield item

    def plot(self, critvolume=0.25):  # todo reimplement
        """plots a representation of the planned pierce array"""
        raise NotImplementedError('This needs to be reimplemented.')
        # pl.clf()
        # pl.close()
        # grx = []
        # gry = []
        # rdx = []
        # rdy = []
        # size = None
        # for item in self:
        #     if item is not None:
        #         if isinstance(item, items.manipulated.Vial):
        #             if size is None:
        #                 size = item.p['diameter']
        #             if item.current_volume <= critvolume:
        #                 rdx.append(item._location['x'])
        #                 rdy.append(item._location['y'])
        #             else:
        #                 grx.append(item._location['x'])
        #                 gry.append(item._location['y'])
        #         elif isinstance(item, items.manipulated.Needle):
        #             if size is None:
        #                 size = item.od
        #             if type(item.location) == dict:
        #                 grx.append(item.location['x'])
        #                 gry.append(item.location['y'])
        #
        # size = (size * 2) ** 2
        #
        # pl.scatter(grx, gry, facecolors='green', s=size)
        # pl.scatter(rdx, rdy, facecolors='red', s=size)
        # pl.gca().invert_xaxis()
        # pl.gca().invert_yaxis()
        # pl.ylabel('y (mm)')
        # pl.xlabel('x (mm)')
        # pl.show()

    def get_empty_location(self):
        """
        Retrieves the next empty location in the tray.

        :return: column, row, location dictionary
        :rtype: int, int, dict
        """
        # depending on iterorder, looks for the first empty spot in the itemarray
        if not self.empty_locations():
            raise TrayFull
        if self._iterorder == 'x':
            for i, row in enumerate(self.itemarray):  # row
                for j, item in enumerate(row):  # item in row
                    if item is None:
                        return i, j, self._array[i][j]
        elif self._iterorder == 'y':
            for j, val in enumerate(self.itemarray[0]):  # column
                for i, val1 in enumerate(self.itemarray):  # row
                    item = self.itemarray[i][j]
                    if item is None:
                        return i, j, self._array[i][j]

    def get_location(self, row, col=None):
        """
        Returns the location of the specified row and column in the Tray.

        :param int or grid reference row: column index or grid reference
        :param int or None col: row index (not required if a grid reference was provided)
        :return: well location
        :rtype: Location
        """
        if type(row) == int and col is None:  # input validity check
            raise ValueError('If specifying indicies, both a column and row must be specified.')

        if type(row) == str:  # if supplied with a grid reference, convert to indicies
            row, col = cellname_to_inds(row)
        return Location(self._array[row][col])

    def empty_locations(self):
        """Checks whether there are any empty item spaces in the tray instance"""
        return any([item is None for col in self.itemarray for item in col])

    def filled_locations(self):
        """Checks whether there are any filled item spaces in the tray instance"""
        return any([item is not None for col in self.itemarray for item in col])

    def item_in_location(self, row, col=None, item=None):
        """
        Places the provided item in the item array. If the item has a location attribute, the item's location will be
        updated. This location will the bottom of the tray well.

        :param int or grid reference col: column index or grid reference
        :param int or None row: row index (not required if a grid reference was provided)
        :param item: item to place in the itemarray
        :return: item grid reference
        :rtype: string
        """
        # input validity check
        if type(row) == int and row is None:
            raise ValueError('If specifying indicies, both a column and row must be specified.')

        if type(row) == str:  # if supplied with a grid reference, convert to indicies
            row, col = cellname_to_inds(row)

        if self.itemarray[row, col] is not None:  # check if the spot is empty
            raise ValueError(f'The itemarray of this {self.__class__.__name__} instance already contains something at '
                             f'indices {row},{col}: {self.itemarray[row, col]}')
        self.itemarray[row, col] = item  # assign item
        return inds_to_cellname(row, col)  # return grid reference

    def remove_item_from_location(self, row, col=None):
        """
        Removes the item at the specified index from the itemarray

        :param int or grid reference row: column index or grid reference
        :param int or None col: row index (not required if a grid reference was provided)
        :return: removed item
        """
        if type(row) == int and col is None:  # input validity check
            raise ValueError('If specifying indicies, both a column and row must be specified.')

        if type(row) == str:  # if supplied with a grid reference, convert to indicies
            row, col = cellname_to_inds(row)

        if self.itemarray[row, col] is None:  # if there is nothing in the location
            raise EmptyLocation(inds_to_cellname(row, col))
        item = self.itemarray[row, col]  # retrieves item
        self.itemarray[row, col] = None  # sets itemarray point to None
        return item  # return the retrieved item

    def reset_iterator(self):
        """Resets the state of the iterator"""
        self.generatorarray = self._yielditem()

    def populate_tray(self,
                      itemclass=None,
                      population=None,
                      itemkwargs=None,
                      ):
        # populate tray with same item and population as when tray was first initialized with, or with the item and
        # population passed into the function

        if itemclass is None:
            itemclass = self.itemclass
        if itemkwargs is None:
            itemkwargs = self.itemkwargs

        if itemclass is not None:
            if hasattr(basic, itemclass):
                item = getattr(basic, itemclass)
            elif hasattr(manipulated, itemclass):
                item = getattr(manipulated, itemclass)
            else:
                raise ValueError(f'The item class "{itemclass}" is not defined in the Items package. '
                                 'Please check your spelling (case sensitive)')

            # if handed a population, populate accordingly
            if population is not None:  # todo put in more descriptive error catching here
                for key in population:
                    row, col = cellname_to_inds(key)
                    self.itemarray[row][col] = item(
                        location=self._array[row][col],
                        name=key,
                        **itemkwargs,
                        **population[key],  # todo create a catch for double-specification of kwargs
                    )
            else:  # otherwise, populate all cells
                for i, row in enumerate(self.itemarray):
                    for j, col in enumerate(row):
                        self.itemarray[i][j] = item(
                            location=self._array[i][j],
                            name=inds_to_cellname(i, j),
                            **itemkwargs,
                        )


class OnDeckPneumaticHolder(ManipulatedComponent, PneumaticHolder):
    def __init__(self, location, height=0., x_offset=0., y_offset=0., approach_offset=0., rotate=False, **kwargs):
        """
        Defines a pneumatic (pressure driven) gripper that is located in a given position on the robot deck (such as a
        vial gripper).

        :param location: index location
        :param height: height of the gripper (where to put the bottom of the vial, mm)
        :param x_offset: x offset from index (mm)
        :param y_offset: y offset from index (mm)
        :param approach_offset: offset form the fixed jaw for the vertical approach. Assumes the moving jaw is at +Y.
        :param rotate: rotation of the gripper (see Location.offset() for details)
        :param kwargs: PneumaticHolder keyword arguments
        """
        ManipulatedComponent.__init__(self, location, **kwargs)
        PneumaticHolder.__init__(
            self,
            **kwargs
        )

        if type(location) == str:  # if handed a Deck grid coordinate
            self.location = deck[location]  # calculate the position
        elif type(location) == dict:  # if handed a position dictionary
            self.location = location
        else:
            raise TypeError(f'The location type is not recognized: {type(location)}')

        # create offset from supplied X and Y offsets and height, rotate appropriately, and apply to location
        offset = Location(
            x=x_offset,
            y=y_offset,
            z=height
        )
        offset.rotate(rotate)
        self.location.offset(offset)

        # create copy of location, and apply offset to obtain initial approach location
        self.initial_approach = copy.deepcopy(self.location)
        approach_offset = Location(y=approach_offset)
        approach_offset.rotate(rotate)
        self.initial_approach.offset(approach_offset)

    def __repr__(self):
        return f'{self.__class__.__name__}({self.output}, {self.state})'

    def __str__(self):
        return f'{self.__class__.__name__} on output {self.output} ({"ON" if self.state == 1 else "OFF"})'

    def place_in_grip(self, letgo=False):
        """
        Moves the item in the gripper to the pneumatic holder's location and engages the gripper. If the instance has an
        approach offset, it will follow an L shaped trajectory whereby it first moves the item down and then sideways
        into the fixed jaw of the gripper. This avoids the item snagging on the fixed jaw.

        :param letgo: have the arm gripper let go of the item
        """
        if robotarm.gripper.ingrip is None:
            raise ValueError('There appears to be nothing in the robot arm gripper.')

        # todo consider installing a catch for items that don't have a length attribute or 'pickup' location key

        # move at safe height to over the gripper with offset
        robotarm.safeheight()
        robotarm.move_to_location(
            self.initial_approach.xy,
            target='gripper'
        )
        robotarm.move_to_location(  # move to location
            self.location,
            target='gripper',
            splitxyz='zxy'
        )
        controller.zzz(0.5)
        self.on(  # turn on gripper
            robotarm.gripper.ingrip
        )
        if self.ingrip.item_asset is not None:
            self.ingrip.item_asset.detach()
        if hasattr(self.ingrip, 'update_location'):  # if item has a location property, set location to current
            self.ingrip.update_location(self.location)
        if letgo is True:  # if requested, turns off the robot gripper
            robotarm.gripper_off()  # remove the item from the gripper

    def remove_from_grip(self, letgo=True):
        """
        Picks up the item currently in the deck gripper in the robot arm gripper

        :param letgo: have the deck gripper let go of the item
        :return: item now in the robot arm gripper
        """
        if self.ingrip is None:
            raise ValueError('There appears to be nothing in the deck gripper.')

        loc = copy.deepcopy(self.location)
        if hasattr(self.ingrip, 'length'):  # if the item in grip has a height attribute
            loc['z'] += self.ingrip.length
        robotarm.move_to_location(loc)  # move to location
        robotarm.gripper_on(self.ingrip)  # turn on the robot gripper
        if robotarm.modules.gripper.item.item_asset is not None:
            robotarm.modules.gripper.item.item_asset.attach('gripper')
        if letgo is True:  # if requested, turns off the deck gripper
            self.off()

        return robotarm.gripper.ingrip


class SpinCoater(PneumaticHolder):
    def __init__(self,
                 axis,
                 location,
                 height=0.,
                 **kwargs,
                 ):
        """
        Defines a spin coater controlled by a C9 controller

        :param axis: axis number for the spin coater
        :param location: location of the spin coater
        :param height: height of the spin coater
        :param kwargs: RevoluteJoint and PneumaticHolder keyword arguments
        """
        ManipulatedComponent.__init__(self, location, **kwargs)
        PneumaticHolder.__init__(self, **kwargs)  # give top level access to output component methods
        self._spinner = RevoluteJoint(**kwargs)  # joint is attribute
        self.axis = axis  # axis
        self.location.offset(z=height)  # offset z
        self.finishtime = 0.  # finish time for movements

    def __repr__(self):
        return f'{self.__class__.__name__}({self.axis}, {self.output})'

    def __str__(self):
        return f'{self.__class__.__name__}(ax {self.axis}, op {self.output})'

    def spin(self, t=60, rpm=750, direction=1, wait=False):
        """
        Spins the spin coater.

        :param float t: duration
        :param float rpm: revolutions per minute
        :param int direction: direction to spin
        :param bool wait: whether to wait for completion of the movement
        """
        mopy = self._spinner.spin(t, rpm, direction)  # calculate movement trajectory
        controller.execute(  # load movement trajectory
            'move',
            self.axis,
            *mopy,
        )
        self.finishtime = time.time() + t
        controller.start_axes(self.axis)  # start the axis
        if wait is True:
            self.wait_for()

    def align(self, alignment):
        """
        Aligns the spin coater.

        :param 'x', 'y', float alignment: selected alignment
            Valid alignments are 'x', 'y', for a float value which will be interpreted as a degree relative to the 'y'
            axis.
        """
        if alignment == 'x':  # if alignment is x
            counts = self._spinner.radians_to_counts_closest(np.pi / 2)
        elif alignment == 'y':  # if alignment is y
            counts = self._spinner.radians_to_counts_closest(0.)
        elif type(alignment) == float or type(alignment) == int:  # degree value
            counts = self._spinner.degrees_to_counts_closest(alignment)
        else:  # unrecognized type
            raise TypeError(f'The alignment {alignment} is not a valid type ({type(alignment)}.')
        mopy = self._spinner.change_position(counts)  # calculate movement
        if len(mopy) > 0:  # if there is a movement
            controller.execute(  # load motion command
                'move',
                self.axis,
                *mopy
            )
            controller.start_axes(self.axis)  # start axis
            controller.zzz(trajectory_duration(mopy))  # wait for trajectory time
            self.wait_for()

    def get_location(self, loc=None):
        """
        Returns the location of the center of the spin coater.

        :param loc: unused (catch to make the signature identical to other get_location methods)
        :return: xyz location
        :rtype: Location
        """
        return Location(self.location)

    def wait_for(self):
        """Waits for the spin coater to finish its motion"""
        controller.zzz(
            self.finishtime - time.time(),
            f'Waiting for {self.__class__.__name__} to finish moving'
        )
        controller.wait_on_axes(self.axis)
