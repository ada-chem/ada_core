"""
Basic items (typically these are inherited by classes for abstracted functionality).
"""
import time
import warnings
import numpy as np
from ..dependencies.chemistry import UnitFloat, SubstanceDaemon
from ..dependencies.exceptions import CriticalVolumeException
from ..dependencies.general import cylinder_travel, cylinder_volume
from ..dependencies.coordinates import Location
from ..dependencies.simulator import Asset


class Item(object):
    def __init__(self,
                 location=None,
                 name=None,
                 length=None,
                 asset=None,
                 asset_visible=True,
                 **kwargs,
                 ):
        """
        A generic item class to be subclassed. Has the generic attributes get_location, name, and length used
        extensively in the project.

        :param Location or dict location: xyz location
        :param str name: convenience name
        :param float length: length (mm)
        :param dict asset: an optional dict used to create an a simulator asset (see AssetInfo for parameters)
        :param bool asset_visible: default asset visibility
        """
        self._location = Location(location)  # stores the location of the bottom of the item
        self.name = name
        self.length = length

        self.item_asset = Asset(self.location_bottom, asset, visible=asset_visible)

    def __repr__(self):
        return f'{self.__class__.__name__}()'

    def __str__(self):
        return f'{self.__class__.__name__}({self.name}, {self.location_bottom}, {self.length})'

    @property
    def location_bottom(self):
        """The xyz location of the bottom of the item"""
        return Location(self._location)

    @location_bottom.setter
    def location_bottom(self, location):
        self.item_asset.update_location(location)
        self._location.update(location)

    @property
    def location_top(self):
        """The xyz location of the top of the item"""
        out = Location(self._location)
        out.offset(z=self.length)
        return out

    @location_top.setter
    def location_top(self, location):
        incoming = Location(location)  # create immutable
        incoming.offset(z=-self.length)  # offset for length of item
        self.location_bottom = incoming

    def get_location(self, loc=None):
        """
        Returns either the top or bottom location of the item. If the Item has a length value, top will return the
        location with vertical offset.

        :param 'bottom' or 'top' loc: the location to retrieve
        :return: xyz location
        :rtype: Location
        """
        warnings.warn(
            'The get_location method of items has been depreciated. Retrieve the location_bottom, _top, etc. '
            'attributes directly.',
            DeprecationWarning
        )
        if loc is None or loc.lower() == 'bottom':
            return self.location_bottom
        elif loc.lower() == 'top':
            return self.location_top
        else:
            raise KeyError(f'The loc value "{loc}" is not recognized. Choose from "top" or "bottom"')

    def update_location(self, location):
        """
        Updates the current location of the Item. This method assumes that the provided location is the location of the
        bottom of the Item.

        :param Location or dict location: xyz location
        """
        warnings.warn(
            'The update_location method has been depreciated. Modify the location_bottom attribute directly. ',
            DeprecationWarning
        )
        self.item_asset.update_location(location)
        self._location.update(location)

    def offset_location(self, **kwargs):
        """
        Applies an offset to the Item's location. See `Location.offset()` for keyword argument details.

        :param kwargs: `Location.offset()` keyword arguments
        """
        self._location.offset(**kwargs)
        self.item_asset.update_location(self._location)


class Container(object):
    def __init__(self,
                 current_volume=0.,
                 maximum_volume=None,
                 minimum_volume=0.,
                 decimal_recovery=10,
                 **kwargs,
                 ):
        """
        Generic class for defining a container (e.g. beaker, erlenmeyer, vial, etc.).

        :param float current_volume: current volume in the container (mL)
        :param float maximum_volume: maximum volume for the container (mL)
        :param float minimum_volume: minimum allowable volume for the container (mL)
            If remove_volume is called and the volume removed will drop below this value, an error will be raised.
        :param int decimal_recovery: nth digit to round volumes to (avoids floating point precision errors when
            performing comparisons). By default the values are rounded to 1 pL, which should be sufficient for most
            applications.
        """
        self._curr_vol = None
        self._min_vol = None
        self._max_vol = None
        self._dec_recovery = decimal_recovery  # decimal recovery tracker

        self.contents = SubstanceDaemon()  # substance tracker
        self.minimum_volume = minimum_volume  # minimum volume for the container
        self.maximum_volume = maximum_volume  # maximum volume for the container
        self.current_volume = current_volume  # current volume in the container

    def __repr__(self):
        return f"{self.__class__.__name__}({self.current_volume})"

    def __str__(self):
        return f"{self.__class__.__name__} containing {self.current_volume} "

    @property
    def current_volume(self):
        return self._curr_vol

    @current_volume.setter
    def current_volume(self, volume):
        if self._volume_in_range(volume) is False:
            raise CriticalVolumeException(
                f'The the volume {volume} is beyond the volume range of the '
                f'{self.__class__.__name__} item: {self.minimum_volume} - {self.maximum_volume}.'
            )
        self._curr_vol = UnitFloat(volume, 'L', 'm', 'm')  # set current volume
        self.contents.add_volume(self.current_volume)  # adjust volume

    @property
    def minimum_volume(self):
        return self._min_vol

    @minimum_volume.setter
    def minimum_volume(self, volume):
        if volume is None:  # minimum is default 0. (enforced)
            self._min_vol = UnitFloat(0., 'L', 'm', 'm')
        else:
            if volume < 0.:
                raise CriticalVolumeException(
                    f'The minimum volume must be greater or equal to 0.'
                )
            self._min_vol = UnitFloat(volume, 'L', 'm', 'm')

    @property
    def maximum_volume(self):
        return self._max_vol

    @maximum_volume.setter
    def maximum_volume(self, volume):
        if volume is None:
            self._max_vol = None
        else:
            if volume < 0.:
                raise CriticalVolumeException(
                    f'The maximum volume must be greater or equal to 0.'
                )
            self._max_vol = UnitFloat(volume, 'L', 'm', 'm')

    def _volume_in_range(self, volume):
        """
        Checks that the provided volume is within the accessible range of the syringe

        :param float, UnitValue volume: volume to verify
        :return: in range
        :rtype: bool
        """
        volume = round(volume, self._dec_recovery)  # round volume
        if volume == 0.:  # catch for -0. which breaks everything
            volume = 0.
        return all([
            # minimum volume check
            volume >= round(self.minimum_volume, self._dec_recovery),
            # maximum volume check if applicable
            volume <= round(self.maximum_volume, self._dec_recovery) if self.maximum_volume is not None else True,
        ])

    def add_substance(self, *args, **kwargs):
        """
        Adds a substance to the container's SubstanceDaemon. See SubstanceDaemon for more details.
        """
        self.contents.add_substance(*args, **kwargs)

    def contained(self):
        """
        Returns the volume currently contained in the syringe

        :return: volume contained in syringe (mL)
        """
        warnings.warn(
            'Method has been depreciated, access the .current_volume attribute instead',
            DeprecationWarning
        )
        return self.current_volume

    def add_volume(self, volume):
        """
        Adds the specified volume to the current volume tracker

        :param float,UnitValue volume: volume to add (mL)
        :return: volume in container
        :rtype: UnitFloat
        """
        warnings.warn(
            'The add_volume method has been depreciated. Modify the .current_volume attribute directly.',
            DeprecationWarning
        )
        self.current_volume += volume
        return self.current_volume

    def remove_volume(self, volume):
        """
        Withdraws the specified volume from the volume tracker of the vial.
        Raises an exception if the volume goes below the vial critical volume.

        :param float,UnitValue volume: volume (mL)
        :return: remaining volume
        :rtype: UnitFloat
        """
        warnings.warn(
            'The remove_volume method has been depreciated. Modify the .current_volume attribute directly.',
            DeprecationWarning
        )
        self.current_volume -= volume
        return self.current_volume


class Cylinder(Container):
    def __init__(self,
                 inner_diameter=0.,
                 length=0.,
                 maximum_volume=None,
                 **kwargs
                 ):
        """
        Defines a cylinder and its methods.

        :param float inner_diameter: diameter (mm)
        :param length: length (mm)
        :param maximum_volume: volume (optional, mm).
            If volume is not specified, it will be calculated from the diameter and length.

        :param kwargs: throwaway catch for additional keyword arguments
        """
        # requirement checks
        typecheck = [
            inner_diameter == 0.,
            length == 0.,
            maximum_volume is None,
        ]
        if all(typecheck):
            raise ValueError('Either volume, or both diameter and length must be specified for the Cylinder class.')
        elif maximum_volume is None and any(typecheck[:2]):
            raise ValueError('If volume is not specified, both diameter and length must be for the Cyliner class.')

        # store values
        self.inner_diameter = UnitFloat(inner_diameter, 'm', 'm', 'm')
        self.length = UnitFloat(length, 'm', 'm', 'm')
        if maximum_volume is None:
            maximum_volume = cylinder_volume(inner_diameter, length)
        Container.__init__(
            self,
            maximum_volume=maximum_volume,
            **kwargs
        )

    def __repr__(self):
        return f'{self.__class__.__name__}({self.current_volume})'

    def __str__(self):
        return f'{self.__class__.__name__}(vol {self.current_volume}, diam {self.inner_diameter}, len {self.length})'

    def volume_from_travel(self, travel):
        """
        Calculates the volume associated with the distance travelled.

        :param travel: distance (mm)
        :return: volume (mL)
        :rtype: UnitFloat
        """
        return cylinder_volume(
            self.inner_diameter,
            travel,
        )

    def travel_from_volume(self, volume):
        """
        Calculates the travel in mm to move the specified volume

        :param volume: desired volume (mL)
        :return: travel in mm
        :rtype: UnitFloat
        """
        return cylinder_travel(
            volume,
            self.inner_diameter
        )


class LocationContainer(Container, Item):
    def __init__(self, *args, **kwargs):
        """
        Defines a container that has the location attributes of the Item class (e.g. a well in a 96-well plate or
        an Erlenmeyer)

        :param args: arguments for Container and Item classes
        :param kwargs: keyword arguments for Container and Item classes
        """
        Item.__init__(self, *args, **kwargs)
        Container.__init__(self, *args, **kwargs)

    def __repr__(self):
        return f"{self.__class__.__name__}({self.name}, {self.location_bottom})"

    def __str__(self):
        return f"{self.__class__.__name__} {self.name}, {self.location_bottom}"


class Syringe(Cylinder):
    def __init__(self, **kwargs):
        """
        Describes a syringe.

        :param kwargs: Container and Cylinder keyword arguments
        """
        Cylinder.__init__(self, **kwargs)

    def __repr__(self):
        return f"{self.__class__.__name__}({self.current_volume})"

    def __str__(self):
        return f"{self.maximum_volume} Syringe containing {self.current_volume}"

    def travelrate_from_flowrate(self, flowrate):
        """
        Calculates the travel rate in mm/{unit time} of the specified flow rate (mL/{unit time})

        :param flowrate: flow rate (mL/{unit time})
        :return: travel rate in mm/{unit time}
        :rtype: UnitFloat
        """
        if isinstance(flowrate, UnitFloat) and '/' in flowrate.unit:
            time_unit = flowrate.unit.split('/')[-1]
        else:
            time_unit = 'min'  # assume minute if not specified
        return UnitFloat(
            cylinder_travel(
                flowrate,
                self.inner_diameter,
            ),
            f'm/{time_unit}', 'm', 'm'  # create new rate unit
        )

    def flowrate_from_travelrate(self, travelrate):
        """
        Calculates the flow rate in mL/{unit time} of the specified travel rate (mm/{unit time})

        :param travelrate: travel rate (mm/{unit time})
        :return: flow rate in mL/{unit time}
        :rtype: UnitFloat
        """
        if isinstance(travelrate, UnitFloat) and '/' in travelrate.unit:
            time_unit = travelrate.unit.split('/')[-1]
        else:
            time_unit = 'min'  # assume minute if not specified
        return UnitFloat(
            cylinder_volume(
                self.inner_diameter,
                travelrate,
            ),
            f'L/{time_unit}', 'm', 'm'  # create new rate unit
        )


class ReactionVessel(Container):
    def __init__(self,
                 *args,
                 sampleevery=None,
                 numberofsamples=1,
                 **kwargs,
                 ):
        """
        Defines a vessel where a reaction is performed.

        :param args: arguments for LocationContainer
        :param float, UnitValue sampleevery: how often to sample the vial (s)
        :param int numberofsamples: how many samples to take in total
        :param kwargs: keyword arguments for LocationContainer
        """
        Container.__init__(self, *args, **kwargs)  # initialize LocationContainer
        self.sampleevery = UnitFloat(sampleevery, 's')  # how often samples are taken
        self.numberofsamples = numberofsamples  # total number of samples to take
        self._initialized = time.time()  # time point for the vial initialization
        self._start = None  # reaction start point
        self.timepoints = {}  # storage for sample times
        self.ntimessampled = 0  # counter for the number of times the vial has been sampled
        # number of zeros for sample number zerofilling
        self.zerofill = int(np.ceil(np.log10(self.numberofsamples + 1)))

    def __repr__(self):
        return f"{self.__class__.__name__}({self.ntimessampled}/{self.numberofsamples})"

    def __str__(self):
        return f"{self.__class__.__name__} sampled {self.ntimessampled}/{self.numberofsamples} times"

    def next_sample_time(self):
        """
        :return: next sampling _timepoint relative to the epoch or None if the specified number of samples
        have been retrieved
        :rtype: float, None
        """
        if self._start is None:
            raise ValueError('The reaction start point was not set. Call reaction_start() to '
                             'set the reaction start time. ')
        elif self.ntimessampled == self.numberofsamples:  # if the speicifed number of samples is reached
            return None
        elif any([key.startswith('sample ') for key in self.timepoints]) is False:  # no sample points yet
            return time.time()  # return current time (i.e. sample now!)
        else:
            return self.timepoints[
                       sorted(  # find the name of the last sample timepoint
                           [key for key in self.timepoints if key.startswith('sample ')],
                       )[-1]  # first item of sample timepoints
                   ] + float(self.sampleevery)  # return next timepoint

    def reaction_start(self):
        """
        Triggers the start timepoint of the reaction
        """
        self._start = time.time()  # set the start time value
        self.timepoint('reaction start')  # create a _timepoint

    def change_sample_frequency(self, every):
        """
        Changes the sampling frequency of the reaction vessel to the specified value.

        :param UnitValue, float every: sampling frequency (seconds)
        """
        # todo create a timepoint for the frequency change
        # todo track the next sample time relative to the previous sampling time
        self.sampleevery.value = every

    def change_number_of_samples(self, n):
        """
        Changes the number of samples to take to the specified value.

        :param int n: number of samples to take
        """
        self.numberofsamples = n
        zerofill = int(np.ceil(np.log10(self.numberofsamples + 1)))  # recalculate zerofill
        if zerofill > self.zerofill:  # if the new zero fill is greater, reset
            self.zerofill = zerofill
            for key in sorted(self.timepoints):  # rename existing sample numbers to fix sorting
                if key.startswith('sample'):  # if key starts with sample
                    num = key.split(' ')[1]  # retrieve sample number
                    self.timepoints[f'sample {int(num):0{self.zerofill}d}'] = self.timepoints[key]  # regenerate name
                    self.timepoints.pop(key)  # remove old entry

    def timepoint(self, name=None):
        """
        Saves a timepoint with the provided name (the name must be unique).
        If no name is specified, the function assumes it is a sample, and will save the timepoint as
        "sample ###" with an appropriate number of leading zeros.

        :param string name: name of the timepoint
        :return: time delta since reaction start or initialization of the Vial instance
        :rtype: float
        """
        if name is None or name.lower() == 'sample':  # assume it's a sample unless otherwise specified
            self.ntimessampled += 1
            name = f'sample {self.ntimessampled:0{self.zerofill}d}'
        if name in self.timepoints:  # if the timepoint has already been saved
            raise ValueError(f'The provided name "{name}" is already in the _timepoint storage')
        self.timepoints[name] = time.time()  # save the timepoint in dictionary
        try:  # try to return time delta
            return self.timepoints[name] - self._start
        except KeyError:
            if name == 'reaction start':  # if this is the reaction start, return 0.
                return 0.
            else:  # otherwise, return time since initialization
                return self.timepoints[name] - self._initialized
