"""A series of functions used for determining which comport a North Robotics controller is on"""
import time

from ftdi_serial import Serial, SerialException, SerialTimeoutException


def port_to_string(comport):
    """
    Ensures that the specified port conforms to the format "COM#".

    :param str or int comport: incomping comport
    :return: formatted comport
    :rtype: str
    """
    if type(comport) == str and comport.startswith('COM'):
        return comport
    elif type(comport) == int or comport.isdigit():
        return f'COM{comport}'
    else:
        raise TypeError(f'An unexpected type was provided to the {port_to_string.__name__} function: {comport}, '
                        f'type: {type(comport)}')


def attempt_connection(comport):
    """
    Attempts to connect to a North Robotics controller. (After connection, it expects to send an echo command and
    receive a specific return).

    :param int or str comport: Comport to connect to
    :return: successful connection
    :rtype: bool
    """
    comport = port_to_string(comport)  # formats comport
    try:
        s = Serial(
            comport,
            baudrate=115200,
            timeout=1.,
            writeTimeout=0.5,
        )
    except SerialException:
        return False
    time.sleep(.1)
    try:
        s.write(  # execute echo
            '<ECHO\r'.encode('utf-8'),
        )
    except SerialTimeoutException:
        return False
    start_t = time.time()
    while s.in_waiting == 0 and time.time() < start_t + 1.:
        time.sleep(0.1)
    try:
        read = s.read(
            s.in_waiting,
        ).decode('ascii')
    except SerialTimeoutException:
        s.close()
        return False
    if read == '<ECHOSUCKA>\r':  # if it's the right return string
        s.close()  # close the open ports
        return True
    return False