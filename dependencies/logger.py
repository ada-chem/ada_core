"""
Basing logging implementation for ada_core
"""
import ada_core
import os
import logging
import logging.config

root_dir = os.path.dirname(os.path.abspath(ada_core.__file__))  # root directory of ada_core
if 'logging' not in os.listdir(root_dir):  # if there is no logging folder, create
    os.makedirs(os.path.join(
        root_dir,
        'logging'
    ))

config = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "simple": {
            "format": "%(asctime)s - %(levelname)s - %(name)s - %(message)s"
        }
    },

    "handlers": {
        "debug_file_handler": {
            "class": "logging.handlers.TimedRotatingFileHandler",
            "level": "DEBUG",
            "formatter": "simple",
            "filename": os.path.join(root_dir, "logging/debug.log"),
            "backupCount": 10,
            "when": "d",
            "encoding": "utf8"
        },

        "info_file_handler": {
            "class": "logging.handlers.TimedRotatingFileHandler",
            "level": "INFO",
            "formatter": "simple",
            "filename": os.path.join(root_dir, "logging/info.log"),
            "backupCount": 10,
            "when": "d",
            "encoding": "utf8"
        },

        "error_file_handler": {
            "class": "logging.handlers.TimedRotatingFileHandler",
            "level": "ERROR",
            "formatter": "simple",
            "filename": os.path.join(root_dir, "logging/error.log"),
            "backupCount": 10,
            "when": "d",
            "encoding": "utf8"
        }

    },

    "loggers": {

        "dependencies.interface.Communicator": {
            "level": "DEBUG",
            "handlers": ["error_file_handler"],
            "propagate": True,
        },

        "dependencies.interface.PseudoCommunicator": {
            "level": "DEBUG",
            "handlers": ["error_file_handler"],
            "propagate": True,
        },

        "solvent_screen": {
            "level": "DEBUG",
            "handlers": ["info_file_handler"],
            "propagate": True,
        },

        "liquid_level": {
            "level": "DEBUG",
            "handlers": ["info_file_handler"],
            "propagate": True,
        },

        "veronica_syringe_loop":{
            "level": "DEBUG",
            "handlers": ["info_file_handler"],
            "propagate": True,
        },

        "find_needle": {
            "level": "DEBUG",
            "handlers": ["info_file_handler"],
            "propagate": True,
        }

    },

    "root": {
        "level": "DEBUG",
        "handlers": ["debug_file_handler"]
    }
}

logging.config.dictConfig(config)  # configure logger
logging.getLogger()  # create logger instance
