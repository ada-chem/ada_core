import copy
import warnings
import numpy as np

from ..dependencies.exceptions import MaxPierces
from ..dependencies.general import cellname_to_inds
# from ada_core.dependencies.arm import robotarm as n9


def coordinate_rotation(x, y, rotation=False):
    """
    Rotates the x and y coordinates about the origin depending on the rotation specified.
    Valid options are 'cw', 'ccw', or '180'.

    :param x: x coordinate (mm)
    :param y: y coordinate (mm)
    :param 'cw', 'ccw', '180', or False rotation: direction specification
    :return: x, y
    :rtype: tuple of floats
    """
    warnings.warn('The coordinate_rotation method has been depreciated, use rotate_offset or rotate_about_index instead',
                  DeprecationWarning)
    rotated = rotate_offset(
        Location(x, y),
        theta=rotation,
    )
    return rotated.x, rotated.y


def calculate_offset(location, nominal, printout=True):
    """
    Calculates the offset of the specified location from the nominal location

    :param Location or dict location: true location
    :param Location or dict nominal: nominal location as a grid reference or location dictionary
    :param printout: whether to print the offset values
    :return: offset dictionary
    """
    # convert to Location instances
    nominal = Location(nominal)
    location = Location(location)

    # calculate offsets
    offset = {key: location[key] - nominal[key] for key in location.keys() & nominal.keys()}

    # print if specified
    if printout is True:
        print('Calculated offsets:')
        for key in offset:
            print(f'\t{key}: {offset[key]}')
    return offset


def locationarray(columns,
                  rows,
                  spacing,
                  reference=None,
                  x_offset=0.,
                  y_offset=0.,
                  z_offset=0.,
                  flipcol=True,
                  fliprow=False,
                  rotate=0.,
                  include_z=True
                  ):
    """
    Creates an array of locations based on an array size, hole-hole spacing, and an offset.

    The location array will have the same axes as the robot (see communicator.inverse_kinematics for details).
    By default, columns will increase to user's right (towards the left of the robot arm); invert this behaviour by
    setting flipx to False. Rows will increase towards user (away from robot arm); invert this behaviour by setting
    flipy to True.

    :param int columns: number of columns in the x axis of the array
    :param int rows: number of rows in the y axis of the array
    :param float spacing: hole to hole (center-center) spacing (mm)
    :param dict or Location or str reference: reference xyz location
    :param x_offset: x offset from the reference location to hole A1
    :param y_offset: y offset from the reference location to hole A1
    :param z_offset: z offset from the reference location to hole A1
    The offsets are specified separately from the reference location to allow for rotation of the array. The offsets
    are applied differently depending on the rotation.

    :param bool flipcol: whether columns increase to user's right (default True)
        Based on a 0 degree rotation while on the bed.
    :param bool fliprow: whether rows increase towards the robot (default False)
        Based on a 0 degree rotation while on the bed.
    :param rotate: rotation angle of the array in degrees (default 0. applies no rotation)
        Legacy support, the user can enter 'CW', 'CCW', 180, or False for the rotation instead of an angle.
        For tray rotation, the following rotations may be accomplished relative to the original state:
        90 degree clockwise: set rotate to 'CW'
        90 degree counter clockwise: set rotate to 'CCW'
        180 degree rotation: set rotate to 180 or 'full'
    :param bool include_z: whether to include a z key in the output dictionary
    :return: list of list of dictionarys with indicies list[row][column] = {'x': loc, 'y': loc, 'z': loc}
    :rtype: list of list of dict
    """
    if type(columns) != int:
        try:
            columns = int(columns)
        except ValueError:
            raise ValueError('The number of columns must be an integer')
    if type(rows) != int:
        try:
            rows = int(rows)
        except ValueError:
            raise ValueError('The number of rows must be an integer')

    if reference is None:  # default reference location if not provided
        reference = [0, 0, 0]
    reference = Location(reference)  # create immutable reference

    # convert bool to scalar
    flipcol = -1 if flipcol is True else 1
    fliprow = -1 if fliprow is True else 1

    return [    # return list of lists of Location instances
        [
            rotate_about_index(
                index=reference,
                x_offset=x_offset + colval,
                y_offset=y_offset + rowval,
                z_offset=z_offset,
                theta=rotate,
                include_z=include_z,
            ) for colval in np.linspace(0., flipcol*(columns - 1) * float(spacing), columns)
        ] for rowval in np.linspace(0., fliprow*(rows - 1) * float(spacing), rows)
    ]


class LocationGrid(object):
    def __init__(self,
                 columns,
                 rows,
                 spacing,
                 referencelocation=None,
                 flipcol=True,
                 fliprow=False,
                 rotate=0.,
                 additional_offsets=None,
                 include_z=True,
                 x_offset=0.,
                 y_offset=0.,
                 z_offset=0.,
                 **kwargs
                 ):
        """
        Defines a grid of locations, accessible by grid reference or pythonic indicies.


        *Required*

        :param int columns: number of columns in the x axis of the array
        :param int rows: number of rows in the y axis of the array
        :param float spacing: point to point spacing (center-center)


        *Location modifiers*

        :param grid reference or dict referencelocation: Reference location to build from
        :param float x_offset: x offset from the reference location to hole A1
        :param float y_offset: y offset from the reference location to hole A1
        :param float z_offset: z offset from the reference location to hole A1
            The offsets are specified separately from the reference location to allow for rotation of the array
            (the offsets are applied differently depending on the rotation).
            If no reference location is provided, the base location [0., 0., 0.] will be used.

        *Rotation parameters*

        :param bool flipcol: whether columns increase to user's right (default True)
        :param bool fliprow: whether rows increase towards the robot (default False)
        :param 'CW', 'CCW', 180, or False rotate: rotation of the array (default False applies no rotation)
        For tray rotation, the following rotations may be accomplished relative to the original state:
        90 degree clockwise: set rotate to 'CW'
        90 degree counter clockwise: set rotate to 'CCW'
        180 degree rotation: set rotate to 180

        *Tweaking array segments*

        :param additional_offsets: list of keyword argument sets for offset_locations calls.
        This is a convenient way to modify segments of the array (for instance, if a set of holes is offset vertically).
        The most efficient way of implementing this is to provide dictionaries specifying the start and stop indicies,
        as well as the key: offset pairings for each axis that is to be modified. For example, supplying the keyword
        argument set:

        >>> **{
            'start': 'D1',
            'stop": 'F16',
            'y': -20.5,
            'z': 24.,
        }

        Will move all indicies encompassed between D1 and F16 -20.5 mm in the y direction and 24 mm in the z direction.

        *Item retrieval*

        Locations may be retrieved by providing a grid reference (row is denoted by the alpha, column by numeric).

        >>> grid = LocationGrid(4, 2, 20.)
        >>> grid['A2']
        {'x': -20.0, 'y': 0.0}

        A tuple index

        >>> grid[0]
        [{'x': 0.0, 'y': 0.0, 'z': 0.0}, ... , {'x': -60.0, 'y': 0.0, 'z': 0.0}]
        >>> grid[0, 1]
        {'x': -20.0, 'y': 0.0, 'z': 0.0}
        >>> grid[0, 1, 'x']
        -20.0

        Slicing

        >>> grid[0:2]
        [[{'x': 0.0, 'y': 0.0}, ...], [{'x': 0.0, 'y': 20.0}, ...]]

        Or list indexing

        >>> grid[0][0]
        {'x': 0.0, 'y': 0.0}
        """
        if referencelocation is None:
            referencelocation = [0, 0, 0]
        self.location = Location(referencelocation)  # reference location for array

        self._spacing = spacing
        self._rows = rows
        self._columns = columns
        self._flipcol = flipcol
        self._fliprow = fliprow
        self._rotate = Rotation(z=rotate)

        # create grid array
        self._array = locationarray(
            columns,
            rows,
            spacing,
            reference=self.location,
            flipcol=flipcol,
            fliprow=fliprow,
            rotate=rotate,
            x_offset=x_offset,
            y_offset=y_offset,
            z_offset=z_offset,
            include_z=include_z,
        )

        # create shape property for convenience
        self.shape = (self._rows, self._columns)

        # if additional offsets are defined for the array
        if additional_offsets is not None:
            if type(additional_offsets) == dict:  # if only one
                additional_offsets = [additional_offsets]
            for offset in additional_offsets:
                values = Location(  # create a Location instance
                    x=offset['x'] if 'x' in offset else 0.,
                    y=offset['y'] if 'y' in offset else 0.,
                    z=offset['z'] if 'z' in offset else 0.,
                )
                values.rotate(self._rotate)  # rotate it appropriately
                self.offset_locations(  # apply offset
                    offset['start'],
                    offset['stop'],
                    values,
                )

    def __getitem__(self, item):
        if type(item) == str:
            row, col = cellname_to_inds(item)
            return self._array[row][col]
        elif type(item) == tuple:
            if len(item) == 2:
                return self._array[item[0]][item[1]]
            elif len(item) == 3:
                return self._array[item[0]][item[1]][item[2]]
            else:
                raise ValueError(
                    f'Unhandled tuple length encountered while getting item from {self.__class__.__name__}')
        elif type(item) == int:
            return self._array[item]
        elif isinstance(item, slice):
            return self._array[item.start:item.stop]
        else:
            raise ValueError(f'Unrecognized getitem argument handed to {self.__class__.__name__}: {item}')

    def __repr__(self):
        return f'{self.__class__.__name__}({len(self._array)}, {len(self._array[0])})'

    def __str__(self):
        return f'LocationGrid instance with {len(self._array)} columns and {len(self._array[0])} rows'

    def __len__(self):
        return self.shape[0] * self.shape[1]

    @property
    def locations(self):
        return self._array

    def dimension(self, arr):
        """
        Returns the dimensions of the array of locations not including the dimension of the location itself

        :param list of lists arr: array of locations
        :return: array shape
        :raises TypeError: If no dictionaries are encountered.
        """
        if type(arr) == dict:  # if a location dictionary is encountered
            return []
        if type(arr) == float or type(arr) == int:  # if the wrong type is encountered
            raise TypeError('The does not appear to contain any location dictionaries.')
        return [len(arr)] + self.dimension(arr[0])

    def expand(self, arr):
        """
        Expands the current array in this object with another locations array or the locations array in another
        LocationGrid instance. May also be handed a set of keyword arguements to initialize another LocationGrid
        instance.

        :param array or LocationArray instance or dict arr: incoming array
        """
        # todo write docstring segment for this function and include it in the class kwargs
        if type(arr) == list:
            shape = self.dimension(arr)  # determine the shape array
            if len(shape) == 1:  # if the array is a single column
                self._array.append(arr)
            elif len(shape) == 2:  # multiple columns
                self._array.extend(arr)
        # elif isinstance(arr, np.ndarray):
        #     pass  # todo code addition of ndarrays
        elif isinstance(arr, LocationGrid):
            self._array.extend(arr._array)
        elif type(arr) == dict:  # assume kwargs for LocationGrid instance
            self._array.extend(
                LocationGrid(**arr)._array
            )
        else:
            raise TypeError(f'The array type {type(arr)} is not recognized as an expandable item.')

        self.shape = self.dimension(self._array)

    def offset_locations(self, start, stop, offset):
        """
        Updates the location key of all locations encompassed between the provided indicies.

        :param grid reference, tuple, or int start: start index for modification
        :param grid reference, tuple, or int stop: stop index for modification
        :param Location offset: offset to apply (mm)
        """
        if type(start) == str:  # if provided with a grid reference, convert to tuple
            start = cellname_to_inds(start)
        elif type(start) == int:  # if a single index, change the range to be inclusive
            start = (start, start + 1)

        if type(stop) == str:
            stop = cellname_to_inds(stop)
        elif type(stop) == int:
            stop = (stop, stop + 1)

        stop = (stop[0] + 1, stop[1] + 1)  # modify stop to work with range arguments
        # print(start, stop, key, value)
        for i in range(start[0], stop[0]):
            for j in range(start[1], stop[1]):
                self._array[i][j].offset(offset)


class ImmutableLocationGrid(LocationGrid):
    def __init__(self, **kwargs):
        """
        A LocationGrid mimic that returns deepcopied items when an itemget is performed.
        (This is useful for grid reference accession)

        :param kwargs: LocationGrid keyword arguments
        """
        LocationGrid.__init__(self, **kwargs)

    def __getitem__(self, item):
        if type(item) == str:
            row, col = cellname_to_inds(item)
            return copy.deepcopy(
                self._array[row][col]
            )
        elif type(item) == tuple:
            if len(item) == 2:
                return copy.deepcopy(
                    self._array[item[0]][item[1]]
                )
            elif len(item) == 3:
                return copy.deepcopy(
                    self._array[item[0]][item[1]][item[2]]
                )
            else:
                raise ValueError(
                    f'Unhandled tuple length encountered while getting item from {self.__class__.__name__}')
        elif type(item) == int:
            return copy.deepcopy(
                self._array[item]
            )
        elif isinstance(item, slice):
            return copy.deepcopy(
                self._array[item.start:item.stop]
            )
        else:
            raise ValueError(f'Unrecognized getitem argument handed to {self.__class__.__name__}: {item}')


class PierceArray(object):
    def __init__(self,
                 piercediameter,
                 needle=None,
                 gauge=None,
                 od=None,
                 minspacing=1.5,
                 reuseholes=True,
                 alloweduses=1,
                 edgebuffer=0.,
                 ):
        """
        Creates an array of pierce locations for a pierceable septum based on the outer diameter of a needle and the
        minimum spacing specified. One of needle, gauge, or od must be specified.

        :param piercediameter: the diameter of the piercable area (mm)
        :param needle: Needle instance
        :param od: outer diameter of the needle
        :param gauge: needle gauge
        :param minspacing: minimum spacing for the holes (in units of needle diameters)
        :param reuseholes: whether to reuse pierce locations
        :param alloweduses: the number of times a hole may be used
        :param edgebuffer: buffer distance between the edge of the pierceable area and the usable pierce area
        """
        from ..items.manipulated import Needle
        if needle is not None and isinstance(needle, Needle):
            self.diam = needle.od
        elif od is not None:
            self.diam = od
        elif gauge is not None:
            from profiles.needles import gauges
            self.diam = gauges[gauge]
        else:
            raise ValueError('A value must be provided for one of the needle, gauge, or od parameters')

        self.piercediameter = piercediameter
        self.locs = [Location(0., 0.)]  # array of locs
        if self.piercediameter > 0. and self.diam > 0.:  # if there is a specified area and diameter is greater than 0
            rlist = np.arange(  # list of radii
                0,
                (
                    (self.piercediameter / 2 - edgebuffer)
                    - float(self.diam) / 2
                ),
                float(self.diam) * minspacing
            )

            for r in rlist:  # for each radius
                circum = 2 * np.pi * r  # circumference at radius
                numpierce = circum / float(self.diam)  # number of possible piercings in that radius
                numpierce = int(numpierce / minspacing)  # space holes out to minimum spacing
                thetas = [(2 * np.pi) / numpierce * i for i in range(numpierce)]  # angles of those piercings
                for theta in thetas:
                    self.locs.append(Location(  # convert spherical polar to cartesian and append to locs list
                        r * np.cos(theta),  # x value
                        r * np.sin(theta),  # y value
                    ))
        self.generatorarray = self.yieldloc()  # creates a generator
        self.ntimesused = 0  # number of times the set of holes has been used
        self.i = 0  # for current index calls
        self.reuseholes = reuseholes
        self.alloweduses = alloweduses

    def __repr__(self):
        return f'{self.__class__.__name__}({len(self)} holes)'

    def __str__(self):
        return f'{self.diam} mm od Needle pierce array: {len(self)} holes, {self.diam} mm od needle'

    def __len__(self):
        return len(self.locs)

    def yieldloc(self):
        """grabs the next location"""
        for ind, loc in enumerate(self.locs):
            self.i = ind
            yield loc

    def getloc(self):
        """retrieves the next location from the generator"""
        try:
            return next(self.generatorarray)  # try to return the next pipette tip
        except StopIteration:
            if self.reuseholes is True:
                # print('The pierce array has been exhausted, now reusing holes. ')
                self.generatorarray = self.yieldloc()  # regenerate array
                return next(self.generatorarray)
            else:
                raise MaxPierces

    def plot(self):
        """plots a representation of the planned pierce array"""
        import pylab as pl
        pl.clf()
        pl.close()
        fig, ax = pl.subplots(figsize=(15, 15))

        circles = [
            pl.Circle((0., 0.), self.piercediameter / 2., color='g', fill=False, clip_on=False, linewidth=2)]

        for ind, val in enumerate(self.locs):
            if ind < self.i:  # if hole has been used
                c = {'color': 'r'}
            elif ind == self.i:  # current hole
                c = {'color': 'y'}
            else:  # unused
                c = {'color': 'g'}
            ax.text(*val, ind + 1, color='w', verticalalignment='center', horizontalalignment='center', size=20)
            circles.append(pl.Circle(val, self.diam / 2., clip_on=False, fill=True, **c))
        for circle in circles:
            ax.add_artist(circle)
        bounds = [-self.piercediameter / 2, self.piercediameter / 2]
        ax.set_xlim(*bounds)
        ax.set_ylim(*bounds)
        pl.show()


class Location(object):
    def __init__(
            self,
            x=None,
            y=None,
            z=None,
            unit='mm',
    ):
        """
        Creates a location dictionary-like instance describing a cartesian location.
        The location has the general behaviour of a dictionary, but also supports attribute retrieval.

        :param float, str, dict, Location x: x location (mm), grid reference (str), or location dictionary (dict)
        :param float y: y location (mm)
        :param float z: z location (mm)

        **Examples:**

        >>> loc = Location(25., 50., 100.)
        >>> loc
        Location({'x': 25.0, 'y': 50.0, 'z': 100.0})

        >>> dict(loc)
        {'x': 25 mm, 'y': 50 mm, 'z': 100 mm}

        >>> for key in loc:
                print(key, loc[key])
        x 25 mm
        y 50 mm
        z 100 mm

        **Attribute retrieval**

        Each instance has an attribute for x, y, z, or any combination thereof for convenient retrieval of segments of
        the location dictionaries.

        >>> loc.x
        25 mm

        >>> loc.xy
        {'x': 25 mm, 'y': 50 mm}

        >>> loc.xyz
        {'x': 25 mm, 'y': 50 mm, 'z': 100 mm}

        **Grid Reference**

        A grid reference may be specified for convenient specification of a location.

        >>> loc2 = Location('K6')
        >>> loc2
        Location({'x': 187.5, 'y': 156.0})

        **Key Omission**

        Any key may be omitted from the initialization. The Location instance will only track supplied keys. Any key
        may be added or deleted after instantiation.

        >>> loc3 = Location()
        >>> loc3
        Location({})

        >>> loc3.x = 25.
        >>> loc3
        Location({'x': 25.0})

        >>> loc3.y = 50.
        >>> loc3
        Location({'x': 25.0, 'y': 50.0})

        >>> del loc3.y
        >>> loc3
        Location({'x': 25.0})

        **Mutation**

        The x, y, and z attributes are only mutable if modified directly. If a dictionary is retrieved from a Location
        instance and its values are subsequently modified, these modifications will not be applied to the originals in
        the Location instance.

        >>> retrieved = dict(loc)  # the returned values prevent mutation of the original instance
        >>> retrieved['z'] += 50.
        >>> retrieved
        {'x': 25 mm, 'y': 50 mm, 'z': 150 mm}
        >>> loc
        Location({'x': 25.0, 'y': 50.0, 'z': 100.0})
        >>> loc.x += 25.
        >>> loc
        Location({'x': 50.0, 'y': 50.0, 'z': 100.0})

        >>> retrieved
        {'x': 25.0, 'y': 50.0, 'z': 150.0}

        The location may be updated or offset as needed.

        >>> loc.update(x=15.)  # updates the values
        >>> loc
        Location({'x': 15.0, 'y': 50.0, 'z': 100.0})

        >>> loc.offset(y=25.)  # offsets the current values
        >>> loc
        Location({'x': 15.0, 'y': 75.0, 'z': 100.0})

        """
        self.unit = unit
        self._keys = {'x', 'y', 'z'}
        self._x = None
        self._y = None
        self._z = None

        if type(x) == str:  # allows specification of grid reference
            self.update(deck[x])

        elif type(x) == dict:  # if handed a dictionary as first argument
            self.update(x)

        # allows providing a list, tuple, or array (will be interpreted as an ordered x, y, z list)
        elif type(x) in [list, tuple] or isinstance(x, np.ndarray):
            self.update(  # convert to dictionary and update
                {key: val for key, val in zip(sorted(self._keys), x)}
            )

        elif isinstance(x, Location):  # if handed another location class
            self.update(x)

        else:  # otherwise, interpret literally and set if not None
            self.x = x
            self.y = y
            self.z = z

    def __repr__(self):
        dct = self.dict(*self._keys)
        dct = {key: dct[key] for key in sorted(dct)}
        return f'{self.__class__.__name__}({dct})'

    def __str__(self):
        locs = self.dict(*self._keys)
        return f'{self.__class__.__name__}({", ".join(f"{key}: {str(locs[key])} {self.unit}" for key in sorted(locs))})'

    def __delitem__(self, key):
        if key in self._keys:  # only permit deletion of xyz values
            delattr(self, key)

    def __getitem__(self, item):
        return getattr(self, item)

    def __getattr__(self, item):
        if item in self.__dict__:
            return float(self.__dict__[item])
        elif item == 'xy':
            return self.dict('x', 'y')
        elif item == 'xz':
            return self.dict('x', 'z')
        elif item == 'yz':
            return self.dict('y', 'z')
        elif item in ['loc', 'xyz']:
            return self.dict('x', 'y', 'z')
        else:
            raise AttributeError(f'The {self.__class__.__name__} instance does not have an {item} attribute.')

    def __iter__(self):
        for key in sorted(self._keys):
            if getattr(self, key) is not None:
                yield key

    def __len__(self):
        return sum([1 for key in self])

    def __setitem__(self, key, value):
        if key not in ['x', 'y', 'z']:
            raise KeyError('The key for changing a Location item must be x, y, or z. ')
        setattr(self, key, value)

    def __copy__(self):
        return Location(
            float(self.x) if self.x is not None else None,
            float(self.y) if self.y is not None else None,
            float(self.z) if self.z is not None else None,
        )

    def __deepcopy__(self, memodict={}):
        return self.__copy__()

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, x):
        if x is not None:
            self._x = float(x)

    @x.deleter
    def x(self):
        self._x = None

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, y):
        if y is not None:
            self._y = float(y)

    @y.deleter
    def y(self):
        self._y = None

    @property
    def z(self):
        return self._z

    @z.setter
    def z(self, z):
        if z is not None:
            self._z = float(z)

    @z.deleter
    def z(self):
        self._z = None

    def _mod_or_set(self, key, value):
        """
        Modifies or sets a value depending on whether it is defined.

        :param x, y, z key: key to offset
        :param value: value to apply
        """
        if key not in self._keys:
            raise KeyError('Only x, y, or z keys of a Location instance may be modified.')
        if value is not None:  # for supplied values
            setattr(
                self,
                key,
                # get and modify it not None, set otherwise
                getattr(self, key) + value if getattr(self, key) is not None else value
            )
        if value is None:  # if none, delete attribute
            delattr(self, key)

    def keys(self):
        """Enables dictionary-like behaviour"""
        return set([key for key in self])

    def dict(self, *keys):
        """
        Returns a dictionary format of the keys specified, provided they are defined in the instance.

        :param keys: 'x', 'y', or 'z'
        :return: dictionary location
        :rtype: dict
        """
        if any([key not in self._keys for key in keys]):  # invalid key check
            raise KeyError(f'Invalid keys were supplied: {", ".join(key for key in keys - self._keys)}. '
                           f'valid keys: {", ".join(key for key in self._keys)}')
        out = {}
        for key in keys:
            if getattr(self, key) is not None:
                out[key] = float(getattr(self, key))
        return out

    def update(self, dct=None, **kwargs):
        """
        Updates the keyed location dictionary.

        :param dct: dictionary to use for updating
        :param kwargs: optional keyword specification of keys
        """
        if dct is None:  # if a dictionary was not provided, use keyword arguments
            dct = kwargs
        elif type(dct) == dict and len(kwargs) != 0:  # if both a dictionary and keyword arguments were provided
            dct = dict(dct)  # prevent mutation
            dct.update(kwargs)  # combine
        elif isinstance(dct, Location):  # if a Location was provided
            dct = dict(dct)

        for key in dct.keys():
            setattr(self, key, dct[key])

    def offset(self, x=None, y=None, z=None):
        """
        Offsets the current values of the Location object by the specified amounts.

        :param float, dict, Location x: x offset (mm)
        :param float y: y offset (mm)
        :param float z: z offset (mm)
        """

        if type(x) == dict:  # if handed a dictionary as first argument
            for key in self._keys & x.keys():
                self._mod_or_set(key, x[key])

        # allows providing a list, tuple, or array (will be interpreted as an ordered x, y, z list)
        elif type(x) in [list, tuple] or isinstance(x, np.ndarray):
            for key, val in zip(sorted(self._keys), x):
                self._mod_or_set(key, val)

        elif isinstance(x, Location):  # if handed another location instance
            for key in x:
                self._mod_or_set(key, x[key])

        else:  # otherwise, interpret literally and set if not None
            if x is not None:
                self._mod_or_set('x', x)
            if y is not None:
                self._mod_or_set('y', y)
            if z is not None:
                self._mod_or_set('z', z)

    def offset_vector(self, r, theta):
        """
        Calculate the location of the vector endpoint starting from location with radius r at angle theta relative to the
        y-axis.

        :param float r: distance (mm)
        :param float theta: degrees relative to the y-axis (degrees)
        """
        rads = np.radians(theta)  # calculate angle in radians
        self.offset(  # angle relative to y=0, therefore sin and cos are flipped from standard
            x=r * np.sin(rads),
            y=r * np.cos(rads),
        )

    def rotate(self, rotation):
        """
        Rotates the location about 0,0 by the specified value.

        :param rotation: an angle to rotate by in degrees
        """
        if self.x is None:
            x = 0.
        else:
            x = self.x
        if self.y is None:
            y = 0.
        else:
            y = self.y

        rotated_index = rotate_about_index(
            index=Location(x=0., y=0., z=0.),
            x_offset=x,
            y_offset=y,
            theta=rotation,
        )
        self.update(x=rotated_index.x, y=rotated_index.y)


def rotate_offset(
        offset,
        axis_of_rotation='z',
        theta=0.,
):
    """
    A generic rotation method that rotates an offset about 0,0,0

    :param dict, Location offset: current offset
    :param str axis_of_rotation: The axis to rotate about when rotating around the index.
        A string either 'x', 'y' or 'z'.
    :param theta: The angle of rotation in degrees. Positive for CW rotation and negative for CCW rotation viewed
        from the positive coordinate of the axis of rotation
    :return: rotated offset
    :rtype: Location
    """
    # legacy support
    if type(theta) == str:
        if theta.lower() == 'ccw':
            theta = -90.

        elif theta.lower() == 'cw':
            theta = 90.

        elif theta in ['180', '180.']:
            theta = 180.
    elif type(theta) == Rotation:  # if passed a Rotation instance
        theta = theta.z
    if theta is False or theta == 0.:
        return offset  # catch for no rotation

    # prevent mutation of supplied offset
    offset = Location(offset)
    # adjusting the computation variables based on the axis of rotation
    if axis_of_rotation is 'z':
        x_prime = offset.x
        y_prime = offset.y
    elif axis_of_rotation is 'x':
        x_prime = offset.y
        y_prime = offset.z
    elif axis_of_rotation is 'y':
        x_prime = offset.z
        y_prime = offset.x
    else:
        raise ValueError("Please enter an axis of rotation. An axis of rotation is a string of either 'x', 'y' or 'z'.")

    # catch zero values from offset (allows the user to not define values in an offset)
    if x_prime is None:
        x_prime = 0.
    if y_prime is None:
        y_prime = 0.

    # taking negative because the convention is CW is negative and CCW is positive but we are doing the opposite
    theta = np.radians(theta)    # converting to radians

    # computing the offsets after rotation. The output of this equation is a numpy array.
    rotated_offsets = np.dot(
        [
            [np.cos(theta), np.sin(theta)],
            [-np.sin(theta), np.cos(theta)]
        ],
        [x_prime, y_prime]
    )

    # Calculating the rotated offset
    if axis_of_rotation is 'z':
        return Location(
            x=rotated_offsets[0],
            y=rotated_offsets[1],
            z=offset.z,
        )
    elif axis_of_rotation is 'x':
        return Location(
            x=offset.x,
            y=rotated_offsets[0],
            z=rotated_offsets[1],
        )
    elif axis_of_rotation is 'y':
        return Location(
            x=rotated_offsets[1],
            y=offset.y,
            z=rotated_offsets[0],
        )


def rotate_about_index(
        index=Location(0., 0., 0.),
        x_offset=None,
        y_offset=None,
        z_offset=None,
        axis_of_rotation='z',
        theta=0.,
        include_z=True,
):
    """
    Determines the new location of a rotated point about an axis. Returns the Location instance of the rotated point.

    :param Location index: The index to rotate on as a Location instance.
    :param x_offset: x offset from the reference location to the point of interest
    :param y_offset: y offset from the reference location to the point of interest
    :param z_offset: z offset from the reference location to the point of interest
    :param str axis_of_rotation: The axis to rotate about when rotating around the index.
        A string either 'x', 'y' or 'z'.
    :param float theta: The angle of rotation in degrees
        Positive for CW rotation and negative for CCW rotation viewed from the positive coordinate of the axis of rotation
    :param include_z: whether or not to include the z location in the Location instance output.
    :return: Location rotated about the index location
    """
    # # catch no rotation and return value
    # if theta in [0., False] or all(val == 0. for val in [x_offset, y_offset, z_offset]):
    #     return index
    # calculate rotated offset
    rotated_offset = rotate_offset(
        Location(  # expects a location instance as offset
            x_offset,
            y_offset,
            z_offset if include_z is True else None,
        ),
        axis_of_rotation=axis_of_rotation,
        theta=theta,
    )
    index = Location(index)  # prevent mutation of supplied index
    index.offset(rotated_offset)  # apply offset to index
    return index


def xyz_to_spherical(x, y, z):
    """
    Converts x, y, z rotation to spherical (rho, theta, phi)

    :param x: rotation about x-axis
    :param y: rotation about y-axis
    :param z: rotation about z-axis
    :return: rho, theta, phi
    :rtype: tuple
    """

    rho = np.sqrt(x ** 2 + y ** 2 + z ** 2)    # rho is the radius of the sphere
    # phi = arccos(z/rho)
    # The angle between the z-axis and the radius
    phi = np.arccos(z/rho)
    # theta = arctan(y/x)
    # The angle between the x-axis and the radius projected onto the xy-plane by the sphere
    theta = np.arctan2(
        y,
        x,
    )
    return rho, theta, phi


def spherical_to_xyz(rho, theta, phi):
    """
    Converts spherical coordinates to x, y, z rotation

    :param rho: radius of the sphere
    :param theta: rotation from the x-axis to the projection of the radius on the xy-plane
    :param phi: rotation from z-axis to the radius created by the origin to the point
    :return: x, y, z rotation
    :rtype: tuple
    """
    x = rho * np.sin(phi) * np.cos(theta)
    y = rho * np.sin(phi) * np.sin(theta)
    z = rho * np.cos(phi)
    return x, y, z


class Rotation(object):
    _x = 0.
    _y = 0.
    _z = 0.

    def __init__(
            self,
            x=0.,
            y=0.,
            z=0.,
    ):
        """
        Defines a rotation about the x, y and z axes. The coordinates are expected to be in degrees
        (not radians).

        :param x: rotation about the x-axis (roll)
        :param y: rotation about the y-axis (pitch)
        :param z: rotation about the z-axis (yaw)
        """

        self._keys = {'x', 'y', 'z'}

        if any([val in ['CW', 'CCW', '180'] for val in [x, y, z]]):  # legacy catches for rotation calls
            warnings.warn('Rotation calls containing CW, CCW, or "180" have been depreciated. Please specify an angle '
                          'in degrees', DeprecationWarning)
            if x == 'CW':
                self.z = 90.
            elif x == 'CCW':
                self.z = -90.
            elif x == '180':
                self.z = 180.

        # allows specification of an axis which will be a rotation about the z axis from +y to the axis specified
        elif type(z) == str:
            self.update(z)

        elif type(x) == str:
            self.update(x)

        elif type(x) == dict:
            self.update(x)

        # allows providing a list, tuple, or array (will be interpreted as an ordered x, y, z list)
        elif type(x) in [list, tuple] or isinstance(x, np.ndarray):
            self.update(  # convert to dictionary and update
                {key: val for key, val in zip(sorted(self._keys), x)}
            )

        # catch handed Rotation instances
        elif isinstance(x, Rotation):
            self.update(x)
        elif isinstance(y, Rotation):
            self.update(y)
        elif isinstance(z, Rotation):
            self.update(z)

        else:
            self.x = x
            self.y = y
            self.z = z

    def __repr__(self):
        dct = self.dict(*self._keys)
        dct = {key: dct[key] for key in sorted(dct)}
        return f'{self.__class__.__name__}({dct})'

    def __str__(self):
        locs = self.dict(*self._keys)
        return f'{self.__class__.__name__}({", ".join(f"{key}: {str(locs[key])} {self.unit}" for key in sorted(locs))})'

    def __delitem__(self, key):
        if key in self._keys:  # only permit deletion of xyz values
            delattr(self, key)

    def __getitem__(self, item):
        return getattr(self, item)

    def __getattr__(self, item):
        if item in self.__dict__:
            return float(self.__dict__[item])
        elif item == 'xy':
            return self.dict('x', 'y')
        elif item == 'xz':
            return self.dict('x', 'z')
        elif item == 'yz':
            return self.dict('y', 'z')
        elif item in ['loc', 'xyz']:
            return self.dict('x', 'y', 'z')
        else:
            raise AttributeError(f'The {self.__class__.__name__} instance does not have an {item} attribute.')

    def __iter__(self):
        for key in sorted(self._keys):
            if getattr(self, key) is not None:
                yield key

    def __len__(self):
        return sum([1 for key in self])

    def __setitem__(self, key, value):
        if key not in ['x', 'y', 'z']:
            raise KeyError('The key for changing a Location item must be x, y, or z. ')
        setattr(self, key, value)

    def __copy__(self):
        return Rotation(
            float(self.x) if self.x is not None else None,
            float(self.y) if self.y is not None else None,
            float(self.z) if self.z is not None else None,
        )

    def __deepcopy__(self, memodict={}):
        return self.__copy__()

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, val):
        self._x = val

    @x.deleter
    def x(self):
        self.x = 0.

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, val):
        self._y = val

    @y.deleter
    def y(self):
        self.y = 0.

    @property
    def z(self):
        return self._z

    @z.setter
    def z(self, val):
        self._z = val

    @z.deleter
    def z(self):
        self.z = 0.

    @property
    def roll(self):
        return self.x

    @property
    def pitch(self):
        return self.y

    @property
    def yaw(self):
        return self.z

    def _mod_or_set(self, key, value):
        """
        Modifies or sets a value depending on whether it is defined.

        :param x, y, z key: key to offset
        :param value: value to apply
        """
        if key not in self._keys:
            raise KeyError('Only x, y, or z keys of a Location instance may be modified.')
        if value is not None:  # for supplied values
            setattr(
                self,
                key,
                # get and modify it not None, set otherwise
                getattr(self, key) + value if getattr(self, key) is not None else value
            )
        if value is None:  # if none, delete attribute
            delattr(self, key)

    def keys(self):
        """Enables dictionary-like behaviour"""
        return set([key for key in self])

    def dict(self, *keys):
        """
        Returns a dictionary format of the keys specified, provided they are defined in the instance.

        :param keys: 'x', 'y', or 'z'
        :return: dictionary location
        :rtype: dict
        """
        if any([key not in self._keys for key in keys]):  # invalid key check
            raise KeyError(f'Invalid keys were supplied: {", ".join(key for key in keys - self._keys)}. '
                           f'valid keys: {", ".join(key for key in self._keys)}')
        out = {}
        for key in keys:
            if getattr(self, key) is not None:
                out[key] = float(getattr(self, key))
        return out

    def update(self, dct=None, **kwargs):
        """
        Updates the keyed rotation dictionary.

        :param dct: dictionary to use for updating
        """
        if dct in ['y', '+y', '-y', 'x', '+x', '-x']:  # if the thing passed was a string
            # CCW is negative CW is positive
            if dct in ['y', '+y']:
                dct = {'z': 0.}
            elif dct == '-y':
                dct = {'z': 180.}
            elif dct in ['x', '+x']:
                dct = {'z': 90.}
            elif dct == '-x':
                dct = {'z': -90.}
        elif type(dct) == dict and len(kwargs) != 0:  # if both a dictionary and keyword arguments were provided
            dct = dict(dct)  # prevent mutation
            dct.update(kwargs)  # combine
        elif isinstance(dct, Rotation):  # if a Rotation was provided
            dct = dict(dct)

        for key in dct.keys():
            setattr(self, key, dct[key])

    def offset(self, x=None, y=None, z=None):
        """
        Offsets the current values of the Rotation object by the specified amounts.

        :param float, dict, Rotation x: x offset (degrees)
        :param float y: y offset (degrees)
        :param float z: z offset (degrees)
        """

        if type(x) == dict:  # if handed a dictionary as first argument
            for key in self._keys & x.keys():
                self._mod_or_set(key, x[key])

        # allows providing a list, tuple, or array (will be interpreted as an ordered x, y, z list)
        elif type(x) in [list, tuple] or isinstance(x, np.ndarray):
            for key, val in zip(sorted(self._keys), x):
                self._mod_or_set(key, val)

        elif isinstance(x, Rotation):  # if handed another rotation instance
            for key in x:
                self._mod_or_set(key, x[key])

        else:  # otherwise, interpret literally and set if not None
            if x is not None:
                self._mod_or_set('x', x)
            if y is not None:
                self._mod_or_set('y', y)
            if z is not None:
                self._mod_or_set('z', z)


# convenience instance that can be used to retrieve Location objects of the holes on the N9 deck
deck = ImmutableLocationGrid(
    columns=21,
    rows=17,
    spacing=37.5,
    include_z=False,
    referencelocation={
        'x': 375.,
        'y': -219.,
    },
)
