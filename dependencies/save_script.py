from ..dependencies.commands import commands
from ..dependencies.interface import InvalidCommand
from ..dependencies.general import intround


class PseudoCommunicator(object):
    def __init__(self, filename):
        """
        Mimics the execution behaviour of the Communicator instance in order to create a permanent script that may be
        saved to a North Robotics microcontroller.
        """
        print(f'Storing script in {filename}')
        self.filename = filename
        self.linenumber = 1
        self.file = open(
            self.filename,
            'wt',
        )

    def execute(self, cmd, *args):
        """
        Executes the specified command by communicating it to the robot

        Arguments are the sequential arguments to pass the function.
        If an insufficient number of arguments are passed, an error will be raised.

        Velocity and acceleration are specified upon initiation of the class,
        but can be passed to this function as keyword arguments 'v' and 'a' respectively.

        """
        try:
            strcmd = commands[cmd]  # retrieve the command
        except KeyError:
            raise InvalidCommand(cmd)

        for i, v in enumerate(args):  # package variables
            if type(v) == float:  # the variable must be an integer
                v = intround(v)
            strcmd += ' V' + str(i + 1) + '[' + str(v) + ']'

        if len(strcmd) == 1024 - 4:
            raise ValueError('The length of the command exceeds 1024 characters '
                             '(the maximum supported by the controller)')
        strcmd = '<' + strcmd + '\r'
        self.file.write(
            f'{self.linenumber:03d} {strcmd}'
        )
        self.linenumber += 1

    def close(self):
        """closes the file"""
        self.file.close()

    def loop_to(self, line):
        """Loop to line number"""
        self.execute(
            'line',
            line
        )

    def start_axes(self, *axes):
        """
        Starts the specified axes

        :param axes: list of axes
        :return:
        """
        self.execute(
            'go',
            *[1 if i in axes else 0 for i in range(10)],
        )

    def zzz(self, t, msg=None):
        """
        Convenience function for waiting and displaying a message. Ignores negative wait times (useful if the wait time
        was calculated, but enough time has elapsed). If a message is specified, the estimated wait time (in seconds)
        is displayed at the end of the provided message.

        :param t: time to wait (non-zero values will be ignored)
        :param msg: message to print (optional)
        """
        if t is not None and t > 0.:  # catch for negative times
            self.execute(
                'wait',
                int(t) + 1,
            )


# todo see if there's a way to inject a kwarg argument using a wrapper (e.g. use real Communicator and set sercon=openfile)

if __name__ == '__main__':
    pc = PseudoCommunicator(
        'test.nrscript'
    )
    pc.execute('home')
    pc.execute('wait', 0.5)
    pc.execute('move', 1, 2, 3)
    pc.close()

