"""
Several classes and methods that make chemistry calculations more convenient
"""
from ..dependencies.temp_molecule import Molecule
from unithandler.base import UnitFloat

si_prefixes = {  # dictionary of SI prefixes and their power
    'E': 18,  # exa
    'P': 15,  # peta
    'T': 12,  # tera
    'G': 9,  # giga
    'M': 6,  # mega
    'k': 3,  # kilo
    'h': 2,  # hecto
    'da': 1,  # deka
    '': 0,  # no prefix
    'd': -1,  # deci
    'c': -2,  # centi
    'm': -3,  # milli
    'u': -6,  # micro
    'n': -9,  # nano
    'p': -12,  # pico
    'f': -15,  # femto
    'a': -18,  # ato
}


def scale_value_by_prefix(value, target_prefix, current_prefix=''):
    """
    Converts a value to a different SI prefix.

    :param float value: value to convert
    :param str target_prefix: target prefix to convert to
    :param str current_prefix: current prefix of the value
    :return: scaled value
    :rtype: float
    """
    if isinstance(value, UnitFloat):  # retrieves current prefix from instance
        current_prefix = value.return_prefix
    if target_prefix not in si_prefixes:
        raise KeyError(f'The specified target prefix "{target_prefix}" is not valid. Choose from '
                       f'{", ".join(x for x in si_prefixes)}')
    if current_prefix not in si_prefixes:
        raise KeyError(f'The current prefix "{target_prefix}" is not valid. Choose from '
                       f'{", ".join(x for x in si_prefixes)}')
    if current_prefix == target_prefix:  # catch for no change
        return value
    return float(
        value
        * 10 ** (
            si_prefixes[current_prefix]   # convert to no prefix
            - si_prefixes[target_prefix]  # scale to target prefix
        )
    )


def prefix_from_power(power):
    """
    Returns the SI prefix associated with the provided power.

    :param int power: power
    :return: SI prefix
    :rtype: str
    """
    if power not in si_prefixes.values():
        raise ValueError(f'The power {power} is not in the SI prefixes dictionary')
    for prefix, p in si_prefixes.items():
        if power == p:
            return prefix


class Dilution(object):
    def __init__(self, ci=None, vi=None, cf=None, vf=None, si_prefix=''):
        """
        Calculates the values of the expression ci*vi=cf*vf

        :param float ci: initial concentration (M)
        :param float vi: initial volume (L)
        :param float cf: final concentration (M)
        :param float vf: final volume (L)
        :param str si_prefix: the SI prefix to use for value storage. If specified, all values will be interpreted as
        having this prefix (i.e. if the volumes are in mL, the concentrations will be in mM).
        """
        # store incoming as attributes
        self._UF_kwargs = {  # keyword arguments for UnitFloat instances
            'si_prefix': si_prefix,
            'stored_prefix': si_prefix,
        }
        if ci is not None:
            self.ci = UnitFloat(ci, 'M', **self._UF_kwargs)
        if vi is not None:
            self.vi = UnitFloat(vi, 'L', **self._UF_kwargs)
        if cf is not None:
            self.cf = UnitFloat(cf, 'M', **self._UF_kwargs)
        if vf is not None:
            self.vf = UnitFloat(vf, 'L', **self._UF_kwargs)

        # perform calculations
        if ci is None:
            self.ci = self.calc_ci(self.cf, self.vf, self.vi)
        elif cf is None:
            self.cf = self.calc_cf(self.ci, self.vi, self.vf)
        elif vi is None:
            self.vi = self.calc_vi(self.cf, self.vf, self.ci)
        elif vf is None:
            self.vf = self.calc_vf(self.ci, self.vi, self.cf)

        # non-solution fill volume to make up concentration
        self.fill = self.calc_fill()

    def __repr__(self):
        return f'{self.__class__.__name__}({self.ci}, {self.vi}, {self.cf}, {self.vf})'

    def __str__(self):
        return f'{self.__class__.__name__}(ci: {self.ci}, vi:{self.vi}, cf: {self.cf}, vf: {self.vf})'

    def different_prefix(self, key, targetunit):
        """
        Changes the units of the desired key to the target si unit

        :param str key: defined value (ci, cf, vi, or vf)
        :param str targetunit: target si prefix
        :return: scaled value
        :rtype: float
        """
        if key not in ['ci', 'cf', 'vi', 'vf']:
            raise KeyError('The specified key must be "ci", "cf", "vi", or "vf"')
        return scale_value_by_prefix(
            self.__dict__[key],
            targetunit,
        )

    def calc_ci(self, cf=None, vf=None, vi=None):
        """
        Calculates the initial concentration.

        :param float vi: initial volume (L)
        :param float cf: final concentration (M)
        :param float vf: final volume (L)
        :return: initial concentration (M)
        :rtype: float
        """
        if cf is None:
            cf = self.cf
        if vf is None:
            vf = self.vf
        if vi is None:
            vi = self.vi
        return UnitFloat(cf * vf / vi, 'M', **self._UF_kwargs)

    def calc_cf(self, ci=None, vi=None, vf=None):
        """
        Calculates the final concentration.

        :param float ci: initial concentration (M)
        :param float vi: initial volume (L)
        :param float vf: final volume (L)
        :return: final concentration (M)
        :rtype: float
        """
        if vf is None:
            vf = self.vf
        if vi is None:
            vi = self.vi
        if ci is None:
            ci = self.ci
        return UnitFloat(ci * vi / vf, 'M', **self._UF_kwargs)

    def calc_vi(self, cf=None, vf=None, ci=None):
        """
        Calculates the initial volume.

        :param float ci: initial concentration (M)
        :param float cf: final concentration (M)
        :param float vf: final volume (L)
        :return: initial volume (L)
        :rtype: float
        """
        if cf is None:
            cf = self.cf
        if vf is None:
            vf = self.vf
        if ci is None:
            ci = self.ci
        if cf > ci:
            raise ValueError(f'The target concentration {cf} cannot be greater than the initial concentration {ci}')
        return UnitFloat(cf * vf / ci, 'L', **self._UF_kwargs)

    def calc_vf(self, ci=None, vi=None, cf=None):
        """
        Calculates the final volume.

        :param float ci: initial concentration (M)
        :param float vi: initial volume (L)
        :param float cf: final concentration (M)
        :return: final volume (L)
        :rtype: float
        """
        if cf is None:
            cf = self.cf
        if vi is None:
            vi = self.vi
        if ci is None:
            ci = self.ci
        if cf > ci:
            raise ValueError(f'The target concentration {cf} cannot be greater than the initial concentration {ci}')
        return UnitFloat(ci * vi / cf, 'L', **self._UF_kwargs)

    def calc_fill(self, vf=None, vi=None):
        """
        Calculates the volume required to fill with solvent to achieve the target concentration.

        :param vf: final volume (L)
        :param vi: initial volume (L)
        :return: volume difference
        :rtype: float
        """
        if vf is None:
            vf = self.vf
        if vi is None:
            vi = self.vi
        if vi > vf:
            raise ValueError(f'The final volume {vf} is less than the intial volume {vi}.')
        return UnitFloat(vf - vi, 'L', **self._UF_kwargs)


class SubstanceDaemon(object):
    def __init__(self,
                 *incoming,
                 current_volume=0.,
                 ):
        """
        A daemon for managing the contents of a container. (Several substances contained sharing the same final volume).

        :param current_volume: The volume that the container will/does contain (mL)
        :param incoming: incoming keyword argument sets for add_substance calls
        """
        self.current_volume = UnitFloat(current_volume, 'L', 'm', 'm')
        self.tracker = {}
        for inc in incoming:
            if type(inc) != dict:
                raise TypeError(
                    f'Incoming kwarg value to {self.__class__.__name__} is not a dictionary. Type: {type(inc)}')
            self.add_substance(**inc)
        # todo store solvent type?
        # todo convert current volume to a property (as in container)

    def __repr__(self):
        return f'{self.__class__.__name__}({len(self)}, {self.current_volume})'

    def __str__(self):
        return f'{self.__class__.__name__}({len(self)} substances in {self.current_volume})'

    def __getitem__(self, item):
        return self.tracker[item]

    # def __getattr__(self, item):
    #     if item in self.tracker:
    #         return self.tracker[item]
    #     return self.__dict__[item]

    def __iter__(self):
        for item in self.tracker:
            yield self.tracker[item]

    def __len__(self):
        return len(self.tracker)

    def add_substance(self,
                      name,
                      dispensed=False,
                      **substance_kwargs,
                      ):
        """
        Stores the amount of substance in the contents tracker.

        :param str name: convenience name for retrieval
        :param dispensed: whether the substance has been dispensed
        :param substance_kwargs: keyword arguments for initialization of a Substance instance
        """
        if name in self.tracker:
            raise KeyError(f'The substance {name} is already defined in the tracker instance.')

        self.tracker[name] = Substance(
            **substance_kwargs,
        )
        self.tracker[name].dispensed = dispensed  # store dispensed bool
        if dispensed is True and self.current_volume != 0.:  # dissolve if there is volume
            self.tracker[name].dissolve(self.current_volume)

    def dispensed(self, name):
        """
        Sets the flag denoting a substance's dispensed status to True.

        :param name: name of the substance
        """
        self.tracker[name].dispensed = True

    def dissolve(self, volume):
        """
        Dissolves the substances stored in the daemon in the volume specified.

        :param float or UnitFloat volume: volume (mL)
        """
        self.current_volume += volume
        for substance in self:
            substance.dissolve(volume)

    def remove_volume(self, volume):
        """
        Removes volume from the volume tracker.

        :param float, UnitValue volume: volume removed (mL)
        :return: current volume
        """
        if self.current_volume - volume < 0.:
            raise ValueError(
                f'The removed volume will be below that stored in the volume tracker ({self.current_volume})')
        self.current_volume -= volume
        for substance in self:
            substance.reduce_volume(self.current_volume)
        return self.current_volume

    def add_volume(self, volume):
        """
        Adds volume to the volume tracker.

        :param float, UnitValue volume: volume added (mL)
        :return: current volume
        """
        self.current_volume += volume
        for substance in self:
            substance.dilute(self.current_volume)

    def iterate_undispensed(self):
        """
        Returns a generator that iterates over the substances in the tracker, returning only substances that have not
        been dispensed.

        :return: undispensed substances
        :rtype: generator
        """
        for substance in self:
            if substance.dispensed is False:
                yield substance


# todo add PythoMS to the PyPI and import molecule here for convenient access to number of moles
class Substance(object):
    def __init__(self,
                 amount,
                 amount_type='mass',
                 mw=None,
                 formula=None,
                 density=None,
                 volume=None,
                 ):
        """
        Data class that stores an amount of substance and its concentration in solution.

        :param float, UnitValue amount: amount of material. If a UnitValue is handed, the unit will be used to
            interpret the type of amount handed to the instance.
        :param 'mass', 'volume', 'moles', amount_type: the type of amount handed to the instance
        :param float mw: molecular weight of the molecule (g/mol)
        :param str formula: molecular formula of the molecule (the molecular weight will be calcuated from this)
        :param float density: density of the material (if a liquid; g/mL)
        :param float volume: volume of liquid that the amount is dissolved in (mL)
        """
        self.conc = None  # concentration (enabled after dissolution)
        self.volume = None  # volume that substance is dissolved in
        self.mol = None
        if isinstance(amount, UnitFloat):
            if amount.unit == 'g':  # if a mass
                amount_type = 'mass'
            elif amount.unit == 'L':  # if a volume
                amount_type = 'volume'
            elif amount.unit == 'mol':  # if a number of moles
                amount_type = 'moles'
            elif amount.unit == 'M':  # concentration
                amount_type = 'conc'
            else:
                raise ValueError(f'Unrecognized unit {amount.unit}.')
            amount = amount.specific_prefix('')


        # determine molecular weight
        if formula is not None:
            self.molecule = Molecule(formula)
            mw = self.molecule.mw
        else:
            self.molecule = None
        self.mw = mw

        if amount_type == 'mass':  # if handed a mass, convert to moles
            self.mol = UnitFloat(
                amount / mw,
                'mol',
            )
        elif amount_type == 'volume':  # if handed a volume, need to convert using density
            if density is None:
                raise ValueError('If a volume is provided for an amount, density must be specified.')
            self.mol = UnitFloat(
                amount * density / mw,
                'mol'
            )
        elif amount_type == 'moles':  # if handed moles
            self.mol = UnitFloat(
                amount,
                'mol',
            )
        elif amount_type == 'conc':  # if handed concentration
            self.conc = UnitFloat(
                amount,
                'M',
            )
        else:
            raise ValueError(f'The amount_type "{amount_type}" is not recongnized.')

        if self.conc is None and volume is not None:  # if there was a volume specified, dissolve
            self.dissolve(volume)

    def __repr__(self):
        return f'{self.__class__.__name__}({self.mol}, {self.conc})'

    def __str__(self):
        out = f'{self.__class__.__name__} {self.mol}'
        if self.molecule is not None:
            out += f' {self.molecule.formula}'
        if self.conc is not None:  # append concentration if present
            out += f' at {self.conc}'
        return out

    def dissolve(self, volume):
        """
        Dissolves the substance in the specified volume, activating the conc class attribute.

        :param volume: volume used to dissolve (mL)
        :return: concentration (M)
        :rtype: UnitFloat
        """
        if isinstance(volume, UnitFloat):  # if UnitValue, scale
            volume = volume.specific_prefix('')
        volume = UnitFloat(
            volume,
            'L',
            'm'
        )
        self.volume = volume  # store volume
        self.conc = UnitFloat(  # store concentration
            self.mol / volume,
            'M',
        )
        return self.conc

    def dilute(self, volume):
        """
        Dilutes the concentration to the specified volume

        :param float, UnitValue volume: new volume (mL)
        :return: new concentration (M)
        """
        if self.volume is None:  # if amount has not been dissolved yet, call instead
            self.dissolve(volume)
            return

        self.conc = Dilution(  # calculate dilution and store new concentration
            vi=self.volume,
            ci=self.conc,
            vf=volume,
        ).cf
        self.volume = volume  # set new volume
        return self.conc

    def reduce_volume(self, volume):
        """
        Reduces the tracked volume to the specified volume.

        :param volume: new volume (mL)
        :return: new volume (mL)
        """
        if isinstance(volume, UnitFloat):  # if UnitValue, scale
            volume = volume.specific_prefix('m')
        volume = UnitFloat(
            volume,
            'L',
            'm'
        )
        self.volume = volume
