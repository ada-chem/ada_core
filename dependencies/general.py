"""
General methods that are used by multiple classes across the package
"""
import numpy as np
from bisect import bisect_left as bl
from ..dependencies.chemistry import UnitFloat
import os
import time
import warnings


def avg_stdev(lst):
    """
    calculates, the average, standard deviation, and relative standard deviation of a list of values

    :param lst: list of values
    :return: average, standard deviation, relative standard deviation expressed as a percent
    """
    if len(lst) == 0:
        return None, None
    avg = sum(lst) / len(lst)
    if len(lst) >= 3:
        stdev = np.sqrt(
            sum([(i - avg) ** 2 for i in lst]) / (len(lst) - 1)
        )
    else:
        stdev = None
    if avg == 0. or stdev is None:
        pstdev = None
    else:
        pstdev = stdev / avg * 100.
    return avg, stdev, pstdev


def scale_value(value, lower, upper):
    """
    Scales values between 0 and 1 to span the upper and lower bound

    :param value: value to scale
    :param lower: lower bound
    :param upper: upper bound
    :return: scaled value
    """
    return float(value * (upper - lower) + lower)


def unscale_value(value, lower, upper):
    """
    Un-scales a value based on the lower and upper value provided

    :param value: value to un-scale
    :param lower: lower bound used to scale up the value
    :param upper: upper bound used
    :return: unscaled value (between 0 and 1)
    """
    return (value - lower) / (upper - lower)


def centered_list(length, spacing=1, center=0, dtype='int'):
    """
    Returns a list centered about zero of specified length

    :param length: length of list
    :param spacing: spacing between points
    :param center: center value
    :param dtype: data type (requires modification if float spacing or center is required)
    :return: centered list
    """
    arr = np.arange(length, dtype=dtype) * spacing
    middleindex = int((length - 1) / 2)
    arr -= arr[middleindex]
    arr += center
    return arr.tolist()


def cellname_to_inds(cellname):
    """
    Converts an alphanumeric grid name to an alpha, numberic pythonic index tuple.

    :param str cellname: alphanumeric grid name

    :returns: tuple of indicies corresponding to the pythonic location (with 0 being the first item)
    :rtype: tuple of int

    **Examples**

    ::

        >>> cellname_to_inds('R57')
        (17, 56)
        >>> cellname_to_inds('AH58793')
        (33, 58792)
        >>> cellname_to_inds('PYTHON1973')
        (201883747, 1972)

    **Notes**

    Based on http://stackoverflow.com/questions/7261936/convert-an-excel-or-spreadsheet-column-letter-to-its-number-in-pythonic-fashion
    """
    if len(cellname) == 0 or type(cellname) != str:
        raise ValueError(f'The provided cellname "{cellname}" is not a valid grid reference.')
    alpha = ''
    numeric = ''
    for x in cellname:  # split into alpha and numeric segments
        if x.isalpha():
            alpha += x
        elif x.isdigit():
            numeric += x
        else:  # ignores special characters (e.g. $)
            continue
    alpha = alpha.upper()  # catch for lower case
    num = 0
    for c in alpha:
        num = num * 26 + (ord(c) - ord('A') + 1)
    return num - 1, int(numeric) - 1


def find_nearest(lst, value, wiggle=None):
    """
    Finds the nearest value in a dictionary or list

    :param lst: sorted list or dictionary with keys that are values
    :param value: value to find
    :param wiggle: the wiggle room that the value needs to be within (the bounds are [value-wiggle, value+wiggle])
    :return: the nearest key in the dictionary to the value
    """
    if len(lst) == 0:  # if there are no values
        return None
    if type(lst) == dict:  # if handed a dictionary
        lst = sorted(lst.keys())
    val = lst[
        np.abs(  # array of absolute differences of each list value to the target
            [val - value for val in lst]
        ).argmin()  # index of the minimum value
    ]
    if wiggle is not None and abs(value - val) > wiggle:  # if it's outside the wiggle area
        return None
    return val


def inds_to_cellname(letter, number, lock=None):
    """
    Takes a pythonic index of row and column and returns the corresponding letter-number combination
    cell name.

    :param int letter: The pythonic index for the alpha coordinate, an index of 0 being the first value.
    :param int number: The pythonic index for the numeric coordinate, an index of 0 being the first value.
    :param 'cell', 'row', 'col lock: If you wish to "lock" a cell (e.g. for use with Excel workbooks), this may be
    specified here


    :return: alphanumeric cell name
    :rtype: str

    **Examples**

    >>> inds_to_cellname(55,14)
    'BD15'
    >>> inds_to_cellname(25,12)
    'Z13'
    >>> inds_to_cellname(25, 12, 'cell')
    '$Z$13'
    >>> inds_to_cellname(558,18268)
    'UM18269'


    **Notes**

    Expanded from http://stackoverflow.com/questions/23861680/convert-spreadsheet-number-to-column-letter
    """
    div = letter + 1
    string = ""
    while div > 0:
        div, modulo = divmod(div - 1, 26)
        string += chr(65 + modulo)

    # string order must be reversed to be accurate
    return f'{"$" if lock in ["col", "cell"] else ""}{string[::-1]}{"$" if lock in ["row", "cell"] else ""}{number+1}'


def mlpermin_to_hz(flow, syringe, cpmm=100):
    """
    Because Tom kept complaining. Converts mL/min to tricontinent speed in Hz

    :param flow: flow rate (mL/min)
    :param syringe: syringe that's being used
    :param cpmm: counts per mm
    :return: speed in Hz
    """
    travelpermin = syringe.travelrate_from_flowrate(flow)  # mm travelled in one minute
    return intround(
        travelpermin
        * cpmm  # convert to counts
        / 60.  # 60 seconds in a minute
    )


def intround(i):
    """
    Rounds the provided value and returns it as an integer

    :param i: value to interpret
    :return: the value rounded and converted to integer
    """
    if type(i) == int:  # if already an integer
        return i
    return int(round(i))  # returns the integer value of i rounded


def locateinlist(lst, value, bias='closest', index=False):
    """
    Finds the closest index of the specified *value* in the supplied list.

    :param lst:  List of values to be searched. This list must be sorted, otherwise the returned index is meaningless.
    :param value: The value to index.
    :param bias: The bias of the searching function. Lesser will locate the index less than the specified value,
        greater will locate the index greater than the specified value, and closest will locate the index
        closest to the specified value.
    :param index: Returns either the index (True) of the nearest value or the nearest value itself (False)
    :return:

    **Notes**

    This function is based on http://stackoverflow.com/questions/12141150/from-list-of-integers-get-number-closest-to-a-given-value
    This modified function was imported from Mass Spec Python Tools
    """
    pos = bl(lst, value)
    if pos == 0:  # if at start of list
        pass
    elif pos == len(lst):  # if insertion is beyond index range
        pos -= 1
    elif lst[pos] == value or bias == 'greater':  # if an exact match is found
        pass
    elif bias == 'greater':  # return value greater than the value (bisect_left has an inherent bias to the right)
        return pos
    elif bias == 'lesser':  # return value lesser than the provided
        pos -= 1
    elif bias == 'closest':  # check differences between index and index-1 and actual value, return closest
        adjval = abs(lst[pos - 1] - value)
        curval = abs(lst[pos] - value)
        if adjval < curval or adjval == curval:  # if the lesser value is closer or values are equidistant
            pos -= 1

    if index is True:  # return index
        return pos
    elif index is False:  # return value
        return lst[pos]


def cylinder_volume(diameter, length):
    """
    Calculates the volume of the cylinder.

    :param float, UnitFloat diameter: diameter (mm)
    :param float, UnitFloat length: length (mm)
    :return: volume (mL)
    :rtype: UnitFloat
    """
    # check for UnitFloat magnitude if specified
    if isinstance(diameter, UnitFloat):
        diameter = diameter.specific_prefix('m')
    if isinstance(length, UnitFloat):
        length = length.specific_prefix('m')
    return UnitFloat(  # V = pi * r**2 * h
        np.pi
        * (diameter / 2.) ** 2
        * length
        / 1000.,
        'L', 'm', 'm'
    )


def cylinder_travel(volume, diameter):
    """
    Calculates the distance along a cylinder corresponding to the specified volume.

    :param float volume: volume (mL)
    :param float diameter: inner diameter of the cylinder
    :return: distance (mm)
    :rtype: UnitFloat
    """
    # check for UnitFloat magnitude if specified
    if isinstance(volume, UnitFloat):
        volume = volume.specific_prefix('m')
    if isinstance(diameter, UnitFloat):
        diameter = diameter.specific_prefix('m')
    return UnitFloat(  # h = V / (pi * r**2)
        volume
        * 1000.
        / (np.pi * (diameter / 2.) ** 2),
        'm', 'm', 'm'
    )


def chunk_list(lst, chunklength=3):
    """
    Splits a list into chunks of specified size.

    :param lst: incoming list
    :param chunklength: length of the list chunks
    :return: list of lists
    :rtype: list
    """
    return [lst[i:i + chunklength] for i in range(0, len(lst), chunklength)]


def calculate_rpm(velocity, cpr):
    """
    Calculates revolutions per minute from velocity and counts per revolution.

    :param int velocity: velocity in counts/second
    :param int cpr: counts per revolution
    :return: revolutions per minute
    :rtype: float
    """
    return (
            (
                    velocity * 60.
            ) / cpr
    )


def calculate_velocity(rpm, cpr):
    """
    Calculates velocity from revolutions per minute and counts per revolution

    :param float rpm: revolutions per minute
    :param int cpr: counts per revolution
    :return: velocity (Hz)
    :rtype: int
    """
    return rpm * cpr / 60.


class Watcher(object):
    def __init__(self,
                 path,
                 watchfor='',
                 includesubfolders=True,
                 subdirectory=None,
                 ):
        """
        Watches a folder for file changes.

        :param path: The folder path to watch for changes
        :param watchfor: Watch for this item. This can be a full filename, or an extension (denoted by *., e.g. "*.ext")
        :param bool includesubfolders: wehther to search subfolders
        :param str subdirectory: specified subdirectory
        """
        self._path = None
        self._subdir = None
        self.path = path
        self.subdirectory = subdirectory
        self.includesubfolders = includesubfolders
        self.watchfor = watchfor

    def __repr__(self):
        return f'{self.__class__.__name__}({len(self.contents)} {self.watchfor})'

    def __str__(self):
        return f'{self.__class__.__name__} with {len(self.contents)} matches of {self.watchfor}'

    def __len__(self):
        return len(self.contents)

    def __iter__(self):
        for file in self.contents:
            yield file

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, newpath):
        if not os.path.isdir(newpath):
            raise ValueError(f'The specified path\n{newpath}\ndoes not exist.')
        self._path = newpath

    @property
    def subdirectory(self):
        return self._subdir

    @subdirectory.setter
    def subdirectory(self, newdir):
        if newdir is None:
            del self.subdirectory
            return
        if not os.path.isdir(
            os.path.join(self.path, newdir)
        ):
            raise ValueError(f'The subdirectory {newdir} does not exist in the path {self.path}.')
        self._subdir = newdir

    @subdirectory.deleter
    def subdirectory(self):
        self._subdir = None

    @property
    def contents(self):
        """Finds all instances of the watchfor item in the path"""
        # TODO exclude subfolders if specified
        if self.subdirectory is not None:
            path = os.path.join(self.path, self.subdirectory)
        else:
            path = self._path
        contents = []
        if self.includesubfolders is True:
            for root, dirs, files in os.walk(path):  # walk through specified path
                for filename in files:  # check each file
                    if filename.lower().endswith(self.watchfor[1:].lower()):  # if extension matches
                        contents.append(os.path.join(root, filename))
                    elif filename.lower() == self.watchfor.lower():  # if filename match
                        contents.append(os.path.join(root, filename))
        else:
            for file in os.listdir(path):
                if file.lower().endswith(self.watchfor[1:].lower()):
                    contents.append(os.path.join(path, file))
                elif file.lower() == self.watchfor.lower():
                    contents.append(os.path.join(path, file))
        return contents

    def check_path_for_files(self):
        """Finds all instances of the watchfor item in the path"""
        warnings.warn('The check_path_for_files method has be depreciated, access .contents directly',
                      DeprecationWarning)
        return self.contents

    def find_subfolder(self):
        """returns the subdirectory path within the full path where the target file is"""
        if self.subdirectory is not None:
            path = os.path.join(self.path, self.subdirectory)
        else:
            path = self.path
        contents = []
        for root, dirs, files in os.walk(path):  # walk through specified path
            for filename in files:  # check each file
                if filename.lower().endswith(self.watchfor[1:].lower()):  # if extension matches
                    contents.append(root)
                elif filename.lower() == self.watchfor.lower():  # if filename match
                    contents.append(root)
        return contents

    def wait_for_presence(self, waittime=1.):
        """waits for a specified match to appear in the watched path"""
        while len(self.contents) == 0:
            time.sleep(waittime)
        return True

    def oldest_instance(self, wait=False, **kwargs):
        """
        Retrieves the first instance of the watched files.

        :param wait: if there are no instances, whether to wait for one to appear
        :return: path to first instance (None if there are no files present)
        """
        if len(self.contents) == 0:  # if there are no files
            if wait is True:  # if waiting is specified
                self.wait_for_presence(**kwargs)
            else:  # if no wait and no files present, return None
                return None
        if len(self.contents) == 1:  # if there is only one file
            return os.path.join(self._path, self.contents[0])
        else:  # if multiple items in list
            return os.path.join(  # return path to oldest (last modified) file in directory
                self._path,
                min(
                    zip(
                        self.contents,  # files in directory
                        [os.path.getmtime(  # last modifiation time for files in directory
                                               os.path.join(self._path, filename)
                                               ) for filename in self.contents]
                    ),
                    key=lambda x: x[1]
                )[0]
            )

    def newest_instance(self):
        """
        Retrieves the newest instance of the watched files.

        :return: path to newest instance
        :rtype: str
        """
        if len(self.contents) == 0:  # if there are no files
            # if wait is True:  # if waiting is specified
            #     self.wait_for_presence(**kwargs)
            # else:  # if no wait and no files present, return None
                return None
        if len(self.contents) == 1:  # if there is only one file
            return os.path.join(self._path, self.contents[0])
        else:  # if multiple items in list
            return os.path.join(  # return path to oldest (last modified) file in directory
                self._path,
                max(
                    zip(
                        self.contents,  # files in directory
                        [os.path.getmtime(  # last modifiation time for files in directory
                            os.path.join(self._path, filename)
                        ) for filename in self.contents]
                    ),
                    key=lambda x: x[1]
                )[0]
            )

    def update_path(self, newpath):
        """
        Updates the path to file of the instance.

        :param str newpath: path to new file
        """
        warnings.warn('The update_path method has been depreciated, modify .path directly', DeprecationWarning)
        self.path = newpath


def front_pad(lst, length, pad=0.):
    """
    Front pads a list to the specified length with the provided value.

    :param lst: list to pad
    :param length: target length
    :return: padded list
    """
    return [pad] * (length - len(lst)) + lst
