"""
Methods for retrieving computer specific variables.

The class EnvironmentVariables retrieves items
"""
import os
import pickle
import warnings


def get_contents(path):
    """
    Retrieves the contents of the specified path
    :param path: path to file
    :return:
    """
    with open(path, 'rt') as file:
        out = file.readlines()
    return [line.strip() for line in out]


def convert_to_dict(lst):
    """
    Converts a retrieved list of values to dictionary format

    :param lst: incoming list of lines read from a file
    :return:
    """
    return {
        key: val for key, val in [line.split() for line in lst]
    }


class EnvironmentVariables(object):
    def __init__(self, variable='nrobotics_private'):
        """
        Convenience retriever of environment variables (does not require assigning environment variable value to a the
        class). The class will attempt to retrieve values first from the file associated with the environment variable
        name provided on initialization, then retrieves from the manual definitions dictionary, then searches for an
        environment variable with the provided name.

        :param variable: name of the environment variable where keys may be found
        """
        warnings.warn('This class is being depreciated, use os.getenv() and .env files instead', DeprecationWarning)
        self.variable = None
        if variable in os.environ:  # if the variable is defined in the environment
            self.variable = variable
        else:  # otherwise warn
            warnings.warn(
                f'The key {variable} is not defined in the operating system envrionment variables. ',
                UserWarning
            )

    def __getitem__(self, item):
        return self.get_key(item)

    def __contains__(self, item):
        try:
            self.get_key(item)
            return True
        except ValueError:
            return False

    def get_key(self, key):
        """
        This method will attempt to retrieve values first from the file associated with the environment variable
        name provided on initialization, then retrieves from the manual definitions dictionary, then searches for an
        environment variable with the provided name.

        :param key: defined variable name
        :return: value associated with the variable
        """
        if self.variable is not None:  # if environment variable is defined
            dct = convert_to_dict(  # convert to dictionary
                get_contents(os.environ[self.variable])  # get the contents
            )
            if key in dct:  # if the value is present, return
                return dct[key]
        if key in manual_definitions:  # look in manual definitions
            if manual_definitions[key] is not None:
                return manual_definitions[key]
        if key in os.environ:  # if a specified environment variable
            return os.environ[key]
        raise ValueError(f'The variable {key} is not defined in either the environment vairable {self.variable} or in '
                         f'the manual definitions. ')

    @staticmethod
    def defined_variables():
        """returns a list of the available environment variables"""
        return os.environ.keys()


def check_n9_status():
    """checks the status of chemos"""
    with open(os.path.join(var_retriever['chemospath'], 'N9_status.pkl'), 'rb') as file:
        return pickle.load(file)


def set_n9_status(status):
    """
    Sets the N9 status to that specified

    :param status: desired status
    :return: previous status
    """
    with open(os.path.join(var_retriever['chemospath'], 'N9_status.pkl'), 'rb') as file:
        previous = pickle.load(file)
    with open(os.path.join(var_retriever['chemospath'], 'N9_status.pkl'), 'wb') as file:
        pickle.dump(
            {'status': status},
            file
        )
    return previous


manual_definitions = {
    'token': None,
    'admin': None,
    'chemospath': None,
    'dbpath': None,
    'hplcfolder': None,
}

# instance of the variable retriever
var_retriever = EnvironmentVariables()
