import time

class AutoSequence(object):
    def __init__(self, tasks=None, firstthing=None):
        """
        Automatically determines the next thing to do based on the supplied time that the things need to be done
        :param things: optional input things (dictionary)
        """
        # storage dictionary for tasks and their properties
        self.tasks = {}
        if tasks is not None:
            for task in tasks:
                self.add_task(task, **tasks[task])
        self.sequence = self.determine_sequence(firstthing)


    def add_task(self, taskname, func, reps, dur, every, funcargs=None, funckwargs=None, prerequisites=None):
        """
        Adds a task set to the sequence queue

        :param taskname: name for the task (this is how the task is accessed in the TaskSequence object)
        :param func: function to be executed
        :param reps: The number of times this function needs to be executed. A counter will be established to track
        the number of times the function has been executed, and an error will be raised if the task is called
        once the number of calls equals the reps value. If this value is set to 0, the task may be called as many times
        as desired.
        :param dur: duration of the function sequence (execution time)
        :param every: execute the function every x seconds
        :param funcargs: arguments to hand to the function
        :param funckwargs: keyword arguments to hand to the function
        :param prerequisites: other task names which need to be executed before this task
        """
        if taskname in self.tasks:
            raise KeyError('The task "%s" already exists in the list of tasks' % taskname)
        self.tasks[taskname] = {
            'func': func,
            'reps': reps,
            'dur': dur,
            'every': every,
            'funcargs': funcargs,
            'funckwargs': funckwargs,
            'prerequisites': list(prerequisites),
            'nexec': 0,  # counter for the number of executions
            'complete': False,  # whether the task (and all its repititions) are complete
        }

    def determine_sequence(self, firsttask=None):
        """
        Determines the sequence of tasks given the currently defined set of tasks

        :param firsttask: (optional) first thing to do
        :return: sequence
        """
        ordered = []
        if firsttask is not None:
            # if there are prerequisites, check that they are complete
            if len(self.tasks[firsttask]['prerequisites']) != 0:
                if any([self.tasks[task]['complete'] is False for task in self.tasks[firsttask]['prerequisites']]):
                    raise ValueError('There are uncompleted prerequisite tasks for the first task: %s' % str(
                        [key for key in self.tasks[firsttask]['prerequisites'] if self.tasks[key]['complete'] is False]
                    ))
            ordered.append(firsttask)

        # todo figure out how to do prerequisites


        # while len(ordered) != len(self.thingstodo):
        #     potentials = [
        #         key for key in self.thingstodo if key in
        #     ]
        #     ordered.append(min(
        #         self.thingstodo,
        #         key=
        #     ))
        #     nextset = []

    def next_thing(self):
        pass

    def execute_task(self, taskname, ignorecomplete=False, *args, **kwargs):
        """
        Executes the specified task

        :param taskname: name of the task
        :param ignorecomplete: manual ignore of complete sequence error
        :arg args: additional arguments to supply to the function
        :keyword kwargs: additional keyword arguments to supply
        :return: function return
        """
        if taskname not in self.tasks:
            raise KeyError('The task "%s" is not defined in the list of tasks' % taskname)
        if self.tasks[taskname]['complete'] is True:
            if ignorecomplete is True:  # manual ignore
                pass
            elif self.tasks[taskname]['reps'] == 0:  # if the number of repititions is 0 (infinite)
                pass
            else:
                raise ValueError('The task "%s" is already listed as complete '
                                 '(number of executions: %d)' % (taskname, self.tasks[taskname]['nexec']))
        # call function with predefined and supplied kw/args
        out = self.tasks[taskname]['func'](
            *self.tasks[taskname]['funcargs'],  # predefined arguments
            *args,
            **self.tasks[taskname]['funckwargs'],  # predefined keyword arguments
            **kwargs
        )
        self.tasks[taskname]['nexec'] += 1
        return out


class Task(object):
    def __init__(self, func, name, duration):
        """
        An instance to store a sequence of code for later execution

        :param func: callable function
        :param name: name for the task
        :param duration: duration of the task
        """
        self.func = func
        self.name = name
        self.duration = duration

    def __call__(self, *args, **kwargs):
        """
        Allows Task class to be callable

        :param args: arguments
        :param kwargs: keyword arguments
        :return: supplied function output
        """
        return self.func(*args, **kwargs)


def test(thing):
    if thing != 'test':
        if thing == 2:
            pass
        else:
            raise ValueError('other')
