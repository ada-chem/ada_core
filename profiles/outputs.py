"""
Convenience keys for outputs
"""

gripper = {
    'on': 1,
    'off': 0,
}

valve = {  # for a syringe pump valve
    'input': 0,
    'output': 1,
}

valve_reverse = {  # for a syringe pump valve with reversed flow
    'input': 1,
    'output': 0,
}


