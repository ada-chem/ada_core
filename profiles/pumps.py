"""
Profiles for instances of ContinuousPump and SyringePump instances. These profiles may also contain keyword arguements
for the parent classes (RevoluteJoint for the ContinuousPump class, and PrismaticJoint, OutputComponent, and Syringe
for the SyringePump class). See the docstrings of these classes for details on the expected keyword arguments.

The named profile entries in this file may be handed to the Pump classes as a "profile" parameter.
"""

VICI_M6_16O = {
    'ulprev': 99.900,
    'cpr': 2000,
    'cpul': 96.01090498,
    'ratio': 4.75,
    # 'cpr': 9500,  # 2000 cpr with a 1:4.75 planetary gear ratio
    'max_rpm': 1350,
    'max_flowrate': 5.,  # maximum flow (mL/min)
}

VICI_M6_15S = {  # specific calibration for VICI M6 with S/N VICI M6 15S-0007P
    'ulprev': 99.690,  # calibrated by gravity
    'cpr': 2000,
    'cpul': 96.29696928,
    'ratio': 4.75,
    # 'cpr': 9500,  # 2000 cpr with a 1:4.75 planetary gear ratio
    'max_rpm': 1350,
    'max_flowrate': 5.,  # maximum flow (mL/min)
}

VICI_M6_18U = {  # specific calibration for VICI M6 with S/N VICI M6 18U-0015P
    'ulprev': 100.035,  # as per VICI certificate
    'cpr': 2000,
    'cpul': 197.5308641975309,  # (cpr / ulprev) * ratio
    'ratio': 9.88,  # as per Henry
    'max_rpm': 1350,
    'max_flowrate': 5.,  # maximum flow (mL/min)
}

nema_11 = {  # Allan syringe pumps
    'cpmm': 1000,
    'cpr': 2000,
    'max_rpm': 1000,
    'axis_range': [0, 30000],
    'a': 500,  # slow acceleration
    # 'max_v': 30000,
}
