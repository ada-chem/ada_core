"""
Tip remover profiles (for removing pipette tips or needles)

UNIQUE_NAME = {
    'height': 0.,  # height of the remover (mm)
    'vdepth': 0.,  # depth of the removing V (mm)
    'vwidth': 0.,  # width of the removing V (mm)
    'xoffset': 0.,  # offset along the x direction to the opening of the v
    'yoffset': 0.,  # offset along the y direction to the opening of the v
}
"""
from ..assets import tip_removers as tip_remover_assets

Gen1 = {
    'height': 231.,  # original 233, now dented
    'vdepth': 7.5,  # depth of the v (along the axis of orientation)
    'vwidth': 15.5,  # breadtdh of the v (perpendicular)
    'x_offset': -18.75,
    'y_offset': 49.35 - 11.,
    # 'y_offset': 49.35 - 10.75,
    'asset': tip_remover_assets.tip_remover_gen1,
}

integrated_gen2 = {
    'height': 150.,
    'x_offset': -37.5/2 + 1.5,
    'y_offset': 37.5 + 26.5,
    'vdepth': 6.,
    'vwidth': 12.5,
    # 'asset': None,  # todo @Sean
}

integrated_gen2_nadder = {
    'height': 148.3,
    'x_offset': -17.68,      # old value -37.5/2 + 1.5
    'y_offset': 58.7 + 5.5,    # old value 37.5 + 26.5
    'vdepth': 5.5,
    'vwidth': 12.5,
    # 'asset': None,
}
