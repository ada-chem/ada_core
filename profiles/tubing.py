"""
Store tubing profiles here

These values are used in the Plumbing class

Add a new tubing profile giving it a unique name and specifying the inner diameter of the tubing.

    'UNIQUE NAME': INNER_DIAMETER,

"""
diameters = {
    'red PEEK': 0.127,
    'yellow PEEK': 0.1778,
    'blue PEEK': 0.254,
    'orange PEEK': 0.508,
    'green PEEK': 0.762,
    'grey PEEK': 1.016,
    'black PEEK': 1.397,
    '0.01"': 0.254,  # 0.01" ID tubing
    '0.02"': 0.508,  # 0.02" ID tubing
    '0.03"': 0.762,  # 0.03" ID tubing
    '0.04"': 1.016,  # 0.04" ID tubing
}
