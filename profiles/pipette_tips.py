"""
Profiles for pipette tips

length: length from the end of the dispenser (probe) attachment to the end of the pipette tip (mm)
volume: volume containable in the pipette tip (mL)

"""

long_2mL = {
    'length': 98. - 22.,
    'maximum_volume': 2.,
}

short_0_5mL = {
    'length': 50. - 9.5,
    'maximum_volume': 0.5
}

long_1_25_mL = {
    'length': 98.23 - 23.,    # old length was 98. - 22.
    'maximum_volume': 1.25,
    'height': 24.07,
    'interaction_depth': 23.,
    'total_length': 98.23
}
